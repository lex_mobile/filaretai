
let defaultState = {
  qr_modal: false
}
export default function (state = defaultState, action) {
    switch (action.type) {
      case "GET_CURRENT_TASK_FROM_QR_SUCCESS":
      case 'GET_CURRENT_TASK_SUCCESS':
        return action.payload;
      case 'GET_CURRENT_TASK_FAIL':
        return defaultState;
      case 'GET_CURRENT_TASK_FROM_QR_FAIL':
        return {qr_modal: true};
      case 'ERROR':
        return action.payload;
      case 'RESET_ERROR':
        return defaultState;
    }
    return state;
}
  