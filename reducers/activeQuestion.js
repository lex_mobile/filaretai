import { SELECT_QUESTION, FETCH_CATEGORIES, FETCH_QUESTION } from '../actions/index';

export default function (state = null, action) {
  switch (action.type) {
    case SELECT_QUESTION:
      return {question: action.payload.questions, id: action.payload.id, nodeid: action.payload.nodeid };
    case FETCH_QUESTION:
      return {question: action.payload.questions, id: action.payload.id, nodeid: action.payload.nodeid };
    case FETCH_CATEGORIES:
      return null;
  }
  return state;
}
