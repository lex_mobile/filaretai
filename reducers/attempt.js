const initState = {attempt: false};
const attemptState = {attempt: true};
export default function (state = initState, action) {
    switch (action.type) {
        case 'CURRENT_TYPE_ATTEMPT':
            return attemptState;
        case 'REGISTER_ATTEMPT':
            return attemptState;
        case 'LOGIN_ATTEMPT':
            return attemptState;
        case 'RESET_ATTEMPT': 
            return initState;
    }
    return state;
}
  