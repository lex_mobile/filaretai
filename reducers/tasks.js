const defaultState = {init: true, data: {data: [], success: false}};
export default function (state = defaultState, action) {
    switch (action.type) {
        case 'GET_TASK_SUCCESS':
            let rejectedStatus = action.payload.filter((item)=> item.Status === 0);
            let doneStatus = action.payload.filter((item)=> item.Status === 1);
            let workingStatus = action.payload.filter((item)=> item.Status === 2);
            let waitingStatus = action.payload.filter((item)=> item.Status === 3);
            let sorted = [ ...workingStatus, ...waitingStatus,...rejectedStatus, ...doneStatus];
            return {data: sorted, count: action.payload.length, new_count: action.payload.filter((item)=> item.Status === 0 || item.Status === 2).length};
        case 'GET_TASK_FAIL': 
            return defaultState;
    }
    return state;
}
  