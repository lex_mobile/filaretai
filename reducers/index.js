import { combineReducers } from 'redux';
import CategoriesReducer from './categories';
import ActiveQuestion from './activeQuestion';
import Questions from './questions';
import allData from './allData';
import nav from './navigation';
import testResults from './testResults';
import User from './user';
import Classes from './classes';
import SocketReducer from './sockets';
import Tasks from './tasks';
import Attempt from './attempt';
import errorScreen from './error';
import CurrentTask from './currentTask';
import CurrentTaskAnswer from './currentTaskAnswer';
import TestsHistory from './testsHistory';
import RoomObjecs from './roomObjects';
import fetchMaps from './fetchMaps';

const rootReducer = combineReducers({
  allData,
  categories: CategoriesReducer,
  question: ActiveQuestion,
  questions: Questions,
  testResults: testResults,
  user: User,
  classes: Classes,
  nav,
  socket: SocketReducer,
  tasks: Tasks,
  attempt: Attempt,
  error: errorScreen,
  currentTask: CurrentTask,
  currentTaskAnswer: CurrentTaskAnswer,
  testsHistory: TestsHistory,
  roomObjects: RoomObjecs,
  fetchMaps
});

export default rootReducer;
