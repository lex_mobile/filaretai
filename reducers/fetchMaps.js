
let defaultState = {
    inProgress: false,
    data: []
  }
  export default function (state = defaultState, action) {
      switch (action.type) {
        case "FETCH_MAPS_SUCCESS":
          return {inProgress: false, data: action.payload};
        case 'ATTTEMPT_FETCH_MAPS':
          return {inProgress: true};
        case 'FETCH_MAPS_FAILURE':
          return defaultState
      }
      return state;
  }
    