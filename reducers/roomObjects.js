import { GET_ROOMOBJECT, FAIL_ROOMOBJECTS } from '../actions/index';

export default function (state = null, action) {
  switch (action.type) {
    case GET_ROOMOBJECT:
      return action.payload;
    case FAIL_ROOMOBJECTS:
        return state;
    default: 
      return state;
  }
}
