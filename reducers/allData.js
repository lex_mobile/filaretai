import { ALL_DATA } from '../actions/index';

export default function (state = null, action) {
  switch (action.type) {
    case ALL_DATA:
      return action.payload;
  }
  return state;
}
