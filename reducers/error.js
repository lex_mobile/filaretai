
export default function (state = {}, action) {
    switch (action.type) {
      case 'ERROR':
        return action.payload;
      case 'RESET_ERROR':
        return {};
    }
    return state;
}
  