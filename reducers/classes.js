export default function (state = null, action) {
    switch (action.type) {
      case 'GET_CLASSES':
        return {...state, classes: action.payload.data.data};
      case 'SET_CLASS':
        return {...state, inClass: action.payload };
    }
    return state;
}
  