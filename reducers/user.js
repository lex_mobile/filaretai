const initState =  {fail: 'Neprisijungęs', RewardPoints: [50,150,300,500]};
export default function (state = initState, action) {
    switch (action.type) {
      case 'REGISTER':
        return action.payload;
      case 'DELETE_USER':
        return initState;
      case 'FAIL': 
        return {fail: action.payload};
      case 'LOGIN':
        return action.payload;
      case  'GET_TASK_SUCCESS':
        return {...state, points: action.points}
      case "USER_UPDATE_POINTS": 
        return  {
          ...state,
          points: action.payload.points,
          complete_successfully: [...state.complete_successfully, {PollCategoryId: action.payload.id}],
      };
    }
    return state;
}
  