export default function (state = {Photos: []}, action) {
    switch (action.type) {
      case 'ANSWER_CURRENT_TASK':
        return {Photos: [...action.payload]};
       case 'ANSWER_TASK_RESET': 
        return {Photos: []};
    }
    return state;
}
  