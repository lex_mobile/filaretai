export default function(state = { answers: [], photos: { photos: [] }, map: 'init' }, action) {
  let map;
  switch (action.type) {
    case "PUSH_BAD_ANSWER":
      if (state.map === "init") {
        map = new Map();
      } else {
        map = new Map(JSON.parse(state.map));
      }
      map.set(action.payload.id, action.payload);
      return {
        ...state,
        answers: [...state.answers, action.payload],
        map: JSON.stringify([...map]),
        objMap: map
      };
    case "PUSH_GOOD_ANSWER":
      if (state.map === "init") {
        map = new Map();
      } else {
        map = new Map(JSON.parse(state.map));
      }
      map.set(action.payload.id, action.payload);
      return {
        ...state,
        answers: [...state.answers, action.payload],
        map: JSON.stringify([...map]),
        objMap: map
      };
    case "RESET_TEST_ANSWERS":
      return { answers: [], photos: { photos: [] }, map: 'init'};
    case "PUSH_PHOTO_ANSWER":
      return {
        ...state,
        photos: [
          ...state.photos,
          {
            id: action.payload.id,
            photos: [...state.photos.photos, ...action.payload.photos]
          }
        ]
      };
  }
  return state;
}
