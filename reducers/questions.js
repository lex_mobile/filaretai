import { FETCH_CATEGORIES, SELECT_QUESTION } from '../actions/index';

export default function (state = null, action) {
  switch (action.type) {
    case SELECT_QUESTION:
      return SELECT_QUESTION;
      break;
    case FETCH_CATEGORIES:
      if (action.payload[0].Children[0].HasChildren === 0) {
        return 'SELECT_QUESTION';
      } else if (action.payload[0].Questions) {
        return 'GET_QUESTIONS';
      }
      break;
    case 'NAV_POLL':
      return 'SELECT_QUESTION';
    case 'RR':
      return null;
    default: 
      return state;
  }
  return state;
}
