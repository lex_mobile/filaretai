
export default function (state = {count: 0, tests: []}, action) {
    switch (action.type) {
        case 'SEND_ANSWERS':
            if(action.payload !== -1) {
                return {count: state.count++, tests: [...state.tests, action.payload]};
            } else {
                return state;
            }
    }
    return state;
}
  