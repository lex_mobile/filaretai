import { NavigationActions } from 'react-navigation';
import {
  RootNavigator,
  NAV_SPLASH,
  NAV_MENU,
  NAV_QUESTIONS,
  NAV_POLL,
  NAV_SETTINGS,
  NAV_STUDENTAREA,
  NAV_REGISTRATION,
  NAV_MAINROOM,
  NAV_CAMERA,
  NAV_GALLERY,
  NAV_NATUREROOM,
  NAV_LIBRARYROOM,
  NAV_LOGIN,
  NAV_TASKS,
  NAV_ERROR,
  NAV_NATURE1,
  NAV_SCANNER,
  NAV_MAPS
} from '../navigation';

const initialAction = { type: NavigationActions.Init };
const initialState = RootNavigator.router.getStateForAction(initialAction);

export default function (state = initialState, action) {
  let nextState;
  switch (action.type) {
    case NAV_SPLASH:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: NAV_SPLASH }),
        state,
      );
      break;
    case NAV_MENU:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: NAV_MENU }),
        state,
      );
      break;
    case NAV_QUESTIONS:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: NAV_QUESTIONS }),
        state,
      );
      break;
    case NAV_POLL:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: NAV_POLL, params: { background: action.payload} }),
        state,
      );
      break;
    case NAV_MAPS:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: NAV_MAPS }),
        state,
      );
      break;
    case NAV_SETTINGS:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: NAV_SETTINGS }),
        state,
      );
      break;
    case NAV_STUDENTAREA:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: NAV_STUDENTAREA }),
        state,
      );
      break;
    case NAV_REGISTRATION:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: NAV_REGISTRATION }),
        state,
      );
      break;
    case NAV_MAINROOM:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: NAV_MAINROOM }),
        state,
      );
      break;
    case NAV_NATUREROOM:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: NAV_NATUREROOM, params: action.payload }),
        state,
      );
      break;
    case NAV_SCANNER:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: NAV_SCANNER }),
        state,
      );
      break;
    case NAV_LIBRARYROOM:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: NAV_LIBRARYROOM, params: action.payload }),
        state,
      );
      break;
    case NAV_CAMERA:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: NAV_CAMERA }),
        state,
      );
      break;
    case "GET_CURRENT_TASK_FROM_QR_SUCCESS":
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: NAV_TASKS, params: {fromQr: true} }),
        state,
      );
      break;
    case NAV_GALLERY:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: NAV_GALLERY }),
        state,
      );
      break;
    case NAV_LOGIN:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: NAV_LOGIN }),
        state,
      );
      break;
    case NAV_TASKS:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: NAV_TASKS }),
        state,
      );
      break;
    case NAV_ERROR:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: NAV_ERROR }),
        state,
      );
      break;
    case NAV_NATURE1:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: NAV_NATURE1, params: action.payload}),
        state,
      );
      break;
    default:
      nextState = RootNavigator.router.getStateForAction(action, state);
      break;
  }

  // Simply return the original `state` if `nextState` is null or undefined.
  return nextState || state;
}
