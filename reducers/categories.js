import { FETCH_CATEGORIES, ALL_DATA } from '../actions/index';

export default function (state = null, action) {
  switch (action.type) {
    case FETCH_CATEGORIES:
      return action.payload[0].Children;
    case ALL_DATA:
      return action.payload.data.data;
  }
  return state;
}
