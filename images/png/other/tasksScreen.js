import React, { Component } from "react";
import {
    View,
    Text,
    FlatList,
    TouchableOpacity,
    Image,
    Dimensions,
    ScrollView,
    KeyboardAvoidingView,
    Platform,
    StyleSheet,
    ImageBackground
} from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
    getTasks,
    getCurrentTask,
    resetCurrentTask,
    submitTask
} from "../actions/index";

import { Spinner, Button, Input, Icon } from "native-base";
import { styleVariables } from "../globals";
import QuestionPhotoCard from "../components/questionPhotoCard";
const { width, height } = Dimensions.get("window");
import ImageView from "react-native-image-view";
import { Constants } from "expo";
import Star from "../images/svgs/components/star";
import Accordion from "react-native-collapsible/Accordion";
import GlobalHeader from "../components/globalHeader";
// 'tasks' state structure, where data: is prop itself
// data: [
//     ...
// {
// Name: String
// Content: String
// TaskId: Int
// TasksAssignedToId: Int
// Status: Int
// Answer: String
// StudentId: Int
// }
// ...
// ]
// const SECTIONS = [
//   {
//     title: "Atsakyti",
//     content: (

//     )
//   }
// ];
const style = StyleSheet.create({
    statusText: { fontSize: styleVariables.fontSize, color: "white" }
});
export class tasksScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openTaskDetails: false,
            text: "",
            tab: 2,
            show: [],
            activeSections: []
        };
        this.renderPhotoList = this.renderPhotoList.bind(this);
        this.renderAnswerView = this.renderAnswerView.bind(this);
        this.changeTab = this.changeTab.bind(this);
    }
    static navigationOptions = ({ navigation }) => {
        return {
            header: <GlobalHeader withBack={false}>
                <TouchableOpacity onPress={() => {
                    let innerBack = navigation.getParam("innerBack", true);
                    if (innerBack) {
                        navigation.goBack();
                    } else {
                        navigation.setParams({ innerBack: true });
                    }
                }}>
                    <Icon
                        name="arrow-back"
                        style={{
                            paddingHorizontal: 16,
                            paddingVertical: 10,
                            backgroundColor: '#8281e4',
                            borderRadius: 8,
                            color: '#fff'
                        }}>
                    </Icon>
                </TouchableOpacity>
            </GlobalHeader>
        };
    };

    componentDidMount() {
        this.props.getTasks();
        this.props.navigation.setParams({ innerBack: true });
    }
    changeTab(tab) {
        this.setState({ tab });
    }
    _renderSectionTitle = section => {
        return (
            <View>
                <Text>{section.content}</Text>
            </View>
        );
    };

    _renderHeader = section => {
        return (
            <View style={{ alignSelf: "center", width: "100%" }}>
                <Text
                    style={{
                        padding: 15,
                        fontSize: styleVariables.fontSize,
                        color: "#000",
                        backgroundColor: "#498fff",
                        borderRadius: 20
                    }}
                >
                    {section.title}
                </Text>
            </View>
        );
    };

    _renderContent = section => {
        return section.content;
    };

    renderListItem(item) {
  
        return (
            <View
                key={item.Name}
                style={{
                    flexGrow: 1,
                    paddingTop: 30,
                    paddingHorizontal: 15,
                    marginBottom: 15,
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center",
                    fontFamily: 'CaveatBrush',
                }}
            >
                <View style={{
                    height: 50,
                    width: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    borderWidth: 4,
                    borderRadius: 8,
                    borderColor: "#fff",
                    marginRight: 20,

                }}>
                    {/* display on finished task goes flex */}
                    <Icon style={{ color: "#f5a623", fontSize: 45, height: 45, width: 45, display: item.Status ? "flex" : "none" }} name="check" type="Feather"> </Icon>
                </View>
                <View
                    style={{
                        padding: 10,
                        flex: 1,
                        flexDirection: "column",
                        alignItems: "flex-start",
                        justifyContent: "center",
                    }}
                >
                    <Text
                        adjustsFontSizeToFit
                        style={{
                            color: "#f5a623",
                            fontFamily: 'CaveatBrush',
                            flexWrap: "wrap",
                            fontSize: styleVariables.fontSize,
                            paddingBottom: 15,
                            fontWeight: "bold",
                        }}
                    >
                        {item.Name}
                    </Text>
                    <Text
                        adjustsFontSizeToFit
                        style={{
                            flex: 1,
                            color: "white",
                            flexWrap: "wrap",
                            fontSize: styleVariables.fontSize,
                            fontFamily: 'CaveatBrush',

                        }}
                    >
                        {item.Content}
                    </Text>
                </View>
                <View>
                    <TouchableOpacity
                        style={{
                            backgroundColor: "#f5a623",
                            borderRadius: 15,
                            alignItems: "center",
                            justifyContent: "center",
                            width: 120,
                            height: 50
                        }}
                        onPress={() => {
                            this.props.getCurrentTask(item.TasksAssignedToId);
                            this.setState({ openTaskDetails: true });
                            this.props.navigation.setParams({ innerBack: false });
                        }}
                    >
                        <Text
                            adjustsFontSizeToFit
                            style={{
                                color: "white",
                                alignSelf: "center",
                                fontSize: styleVariables.fontSize * 1.2,
                                fontFamily: 'CaveatBrush',

                            }}
                        >
                            Plačiau
              </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    openClose(i) {

        this.setState(prev => {
            let newState = prev.show;
            newState[i] = !newState[i];
            return newState;
        });
    }

    renderPhotoList(photos) {
        let photoList = [];
        photos.forEach((element, i) => {
            let imageModal = [
                {
                    source: {
                        uri: `http://filaretai.lexitacrm.lt${element.Path}`
                    }
                }
            ];
            photoList.push(
                <View
                    key={element.Path.toString() + i.toString()}
                >
                    <ImageView
                        images={imageModal}
                        imageIndex={0}
                        isVisible={
                            typeof this.state.show[i] === "undefined"
                                ? false
                                : this.state.show[i]
                        }
                        onClose={() => {
                            this.openClose(i);
                        }}
                    />

                    <TouchableOpacity
                        onPress={() => {
                            this.openClose(i);
                        }}
                        style={{ flex: 1 }}
                    >
                        <Image
                            style={{
                                resizeMode: "cover",
                                width: width / 2.5,
                                height: width / 2.5
                            }}
                            source={{ uri: `http://filaretai.lexitacrm.lt${element.Path}` }}
                        />
                    </TouchableOpacity>

                </View>
            );
        });
        return photoList;
    }

    _updateSections = activeSections => {
        this.setState({ activeSections });
    };

    renderAnswerView() {

        if (!this.props.currentTask.data.Status) {
            return (
                <View style={{ marginVertical: 10 }}>
                    <Accordion
                        activeSections={this.state.activeSections}
                        headerStyle={{ alignItems: "center" }}
                        sections={[

                            {
                                title: "Atsakyti",
                                content: (
                                    <ScrollView
                                        style={{
                                            flexGrow: 1,
                                            height: Dimensions.get("screen").height + 320
                                        }}
                                    >

                                        <View
                                            style={{ flexDirection: "column", flexGrow: 1 }}
                                        >
                                            <QuestionPhotoCard name={""} task={true} />
                                        </View>
                                        <Input
                                            placeholder="Įveskite atsakymą"
                                            placeholderTextColor="#fff"
                                            placeholderStyle={{ padding: 10 }}
                                            multiline
                                            style={{
                                                height: 100,
                                                backgroundColor: "rgba(0,0,0,0)",
                                                width: "90%",
                                                color: "#fff",
                                                fontFamily: 'CaveatBrush',
                                                borderRadius: 30,
                                                borderWidth: 5,
                                                borderColor: "#fff",
                                                padding: 10,
                                                alignSelf: "center"
                                            }}
                                        />
                                        <View
                                            style={{
                                                marginVertical: 25,
                                                height: "100%",
                                                flexDirection: "row",
                                                justifyContent: "center",
                                                alignSelf: "center"
                                            }}
                                        >
                                            <Button
                                                style={{
                                                    flex: 0.5,
                                                    alignItems: "center",
                                                    justifyContent: "center",
                                                    backgroundColor: "#f5a623",
                                                    borderRadius: 15,
                                                    alignItems: "center",
                                                    justifyContent: "center",

                                                }}
                                                onPress={() => {
                                                  
                                                    this.props.submitTask(
                                                        this.props.currentTask.data.TasksAssignedToId,
                                                        this.state.text
                                                    );
                                                }}
                                            >
                                                <Text
                                                    adjustsFontSizeToFit
                                                    style={{
                                                        padding: 15,
                                                        alignItems: "center",
                                                        fontFamily: 'CaveatBrush',
                                                        color: "#fff",
                                                        alignSelf: "center",

                                                        fontSize: styleVariables.fontSize * 1.2,
                                                    }}
                                                >
                                                    Atsakyti
                                  </Text>
                                            </Button>
                                        </View>

                                    </ScrollView>
                                )
                            }
                        ]}
                        renderHeader={this._renderHeader}
                        renderContent={this._renderContent}
                        onChange={this._updateSections}
                    />
                </View>
            )
        }
        else {
            return null;
        }
    }

    render() {
        let render = [];
        if (this.props.tasks.init) {
            render.push(<Spinner />);
        } else if (!this.props.navigation.getParam("innerBack") && this.props.currentTask) {
            render.push(
                <KeyboardAvoidingView
                    behavior="position"
                    keyboardVerticalOffset={Platform.OS === "android" ? 100 : 0}
                    contentContainerStyle={{
                        height: height * 0.92
                    }}
                    key={this.props.currentTask.data.TaskId}
                >
                    <ImageBackground
                        source={require("../images/png/backgrounds/board.png")}
                        style={{ height: "100%", width: "100%" }}
                    >
                        <ScrollView
                            contentContainerStyle={{
                                flexGrow: 1,
                                zIndex: 1
                            }}
                            style={{ margin: '10%', marginBottom: "24%", }}
                        >
                            <View style={{ flex: 0.5, justifyContent: "center", }}>
                                <View style={{ alignItems: "center", paddingHorizontal: 15, paddingVertical: 5 }}>
                                    <Text
                                        adjustsFontSizeToFit
                                        style={{ fontSize: styleVariables.fontSize * 2, marginBottom: 20, color: "#f5a623", fontFamily: 'CaveatBrush', }}
                                    >
                                        {this.props.currentTask.data.Name}
                                    </Text>
                                    <Text
                                        adjustsFontSizeToFit
                                        style={{ color: "white", fontSize: styleVariables.fontSize, fontFamily: 'CaveatBrush' }}
                                    >
                                        {this.props.currentTask.data.Content}
                                    </Text>
                                </View>
                                <View
                                    style={{
                                        flexDirection: "row",
                                        flexWrap: "wrap",
                                        alignItems: "center",
                                        alignContent: "center",
                                        alignSelf: "center"
                                    }}
                                >
                                    {this.renderPhotoList(this.props.currentTask.data.Photos)}
                                </View>
                                {this.renderAnswerView()}

                            </View>
                        </ScrollView>
                    </ImageBackground>
                </KeyboardAvoidingView>
            );
        } else if (this.props.tasks.data) {
            if (this.props.tasks.count > 0) {
                render.push(
                    <ImageBackground
                        key={"itemList"}
                        source={require("../images/png/backgrounds/board.png")}
                        style={{ height: "100%", width: "100%", }}
                    >
                        <View style={{ position: "absolute", top: 90, alignSelf: "center", height: "68%", width: "80%" }}>
                            <Text style={{ textAlign: "center", fontSize: 32, color: "#fff", fontFamily: 'CaveatBrush', fontWeight: "300" }}>Užduotys</Text>
                            <FlatList
                                scrollEnabled
                                contentContainerStyle={{
                                    flexGrow: 1,
                                }}
                                data={this.props.tasks.data}
                                renderItem={({ item }) => {
                                    return this.renderListItem(item);
                                }}
                                keyExtractor={(item, index) => item.TaskId.toString() + index.toString()}
                            />
                        </View>
                    </ImageBackground>
                );
            } else {
                render.push(
                    <View>
                        <Text style={[style.statusText, { textAlign: "center", fontFamily: 'CaveatBrush', color: '#010048', }]}>Užduočių nėra...</Text>
                    </View>
                )
            }
        } else {
            render.push(
                <View
                    key={"error"}
                >
                    <Text>Klaida</Text>
                    <Text>{this.props.tasks.msg}</Text>
                </View>
            );
        }
        return (
            <View style={{ flex: 1 }}>
                <View
                    style={{
                        flex: 1,
                    }}
                >
                    <View style={{ zIndex: 999, flexGrow: 1 }}>{render}</View>
                </View>
                <View style={{ position: 'relative' }}>
                    <View style={{ position: 'absolute', bottom: 0, left: 0, width: '100%', height: 80, backgroundColor: '#4F3D0B' }} />
                </View>
            </View>
        );
    }
}

const mapStateToProps = state => ({
    tasks: state.tasks,
    currentTask: state.currentTask,
    user: state.user
});

function mapDispatchToProps(dispatch) {
    return bindActionCreators(
        {
            getTasks,
            getCurrentTask,
            resetCurrentTask,
            submitTask
        },
        dispatch
    );
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(tasksScreen);
