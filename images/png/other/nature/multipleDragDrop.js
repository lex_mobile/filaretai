import React, { Component } from "react";
import {
    Text,
    View,
    StyleSheet,
    Image,
    PixelRatio,
    Dimensions,
    TouchableHighlight,
    TouchableOpacity,
    ScrollView
} from "react-native";
import { GestureHandler } from "expo";
const { PanGestureHandler, State } = GestureHandler;
import HTMLView from "react-native-htmlview";
import DraggableContainer from "./draggableContainerNew";
import Dropzone from "./dropzone";
import { htmlViewStyle } from "../globals";
import ImageView from "react-native-image-view";
const { height } = Dimensions.get("screen");

export default class questionDnd extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalStatus: []
        };
        this.dropzoneValues = [];
        this.dropZoneStates = new Map(this.props.opts.map((item, i) => {
            return [i, []];
        }));
        this.measureCallback = this.measureCallback.bind(this);
        this.getDropZoneValues = this.getDropZoneValues.bind(this);
        this.updateDropzoneChild = this.updateDropzoneChild.bind(this);
        this.checkAnswers = this.checkAnswers.bind(this);
        this.openClose = this.openClose.bind(this);
    }
    checkAnswers() {
        let fields = [];
        //console.log(this.props.opts, "[DragAndDrop] Opts");
        //console.log(this.dropZoneStates, "[DragAndDrop] dropZoneStates");
        let requiredMatches = 0;
        for (let i = 0; i < this.props.opts.length; i++) {
            requiredMatches += this.props.opts[i].Details.length;
            //console.log(this.props.opts[i].Details, "opts Details");
        }
        if (this.dropZoneStates) {
            for (const [key, value] of this.dropZoneStates) {
                for (let v = 0; v < value.length; v++) {
                    let ifCorrect = false;
                    for (let i = 0; i < this.props.opts.length; i++) {
                        if (value[v].pollOptionId === this.props.opts[i].id && key === i) {
                            for (let k = 0; k < this.props.opts[i].Details.length; k++) {
                                if (value[v].index === this.props.opts[i].Details[k].PollQuestionAnswerDetailId) {
                                    ifCorrect = true;
                                    //console.log(value[v].title, "MATCHED");
                                    break;
                                }
                            }
                        }
                    }
                    if (ifCorrect) {
                        fields.push(ifCorrect);
                    }
                }
            }
            //console.log(fields);
        }
        let result = fields.length > 0 && fields.length === requiredMatches ? fields.every(success => { return success === true }) : false;
        this.props.feedback({
            result,
            options: fields,
            id: this.props.id,
            time: Date.now()
        });
        return result;
    }
    openClose(j) {
        this.setState(prevState => {
            const newState = prevState.modalStatus.map((item, i) => {
                if (i === j) {
                    item = !item;
                }
                return item;
            });
            return { modalStatus: newState };
        });
    }
    componentDidMount() {
        //console.log(this.props.opts, "OPTS");
        this.setState({
            modalStatus: new Array(this.props.opts.length).fill(false)
        });
    }
    renderOptions() {
        let options = [];
        let squareDimensions =
            (height - height / 3) /
            this.props.opts.length /
            this.props.boxMarginFactor;
        //console.log(this.props.opts, "OPTS OPTS OPTS");
        this.props.opts.map((item, i) => {
            let gg = i + 1;
            let imageModal = [
                {
                    source: {
                        uri: `http://filaretai.lexitacrm.lt${item.PhotoUrl}`
                    }
                }
            ];
            //console.log(item.Details, "ITEM " + i);
            let details = null;
            //console.log(this.props.boxSize);
            if (item && item.Details !== null) {
                details = item.Details.map((element, i) => {
                    if (element && element.Question) {
                        return (
                            <DraggableContainer
                                title={element.Question}
                                arrayIndex={element.PollQuestionAnswerDetailId}
                                identifier={element.PollOptionId}
                                getData={this.getDropZoneValues}
                                marginOffset={this.props.boxSize / 50}
                                checkAnswers={this.checkAnswers}
                            >
                                <View
                                    style={{
                                        minHeight: this.props.boxSize / 2,
                                        height: undefined,
                                        width: undefined,
                                        minWidth: this.props.boxSize,
                                        maxWidth: this.props.boxSize * 2.5,
                                        alignItems: "center",
                                        justifyContent: "center",
                                        borderColor: "#F5FCFF",
                                        backgroundColor: "#00C853",
                                        borderWidth: 1,
                                        borderColor: "black",
                                        borderRadius: 8,
                                        opacity: 0.65
                                    }}
                                >
                                    <Text
                                        style={{
                                            color: "black",
                                            textAlign: "center",
                                            padding: 5
                                        }}
                                    >
                                        {element.Question}
                                    </Text>
                                </View>
                            </DraggableContainer>
                        );
                    }
                });
            }
            options.push(
                <View style={[styles.optionRow, { flex: 1 }]} key={item.id}>
                    <View style={{ flex: 1.5 }}>
                        {details}
                    </View>
                    <View
                        style={{
                            width: "40%",
                            alignItems: "center",
                            justifyContent: "center",
                            flex: 2
                        }}
                    >
                        <Dropzone
                            style={styles.dropzone}
                            title={item.Name}
                            arrayIndex={i}
                            measureCallback={this.measureCallback}
                        />
                    </View>
                    {/* <Image
            source={{ uri: `http://filaretai.lexitacrm.lt${item.PhotoUrl}` }}
            style={{ width: this.props.boxSize*2, height: this.props.boxSize*2, resizeMode: "contain" }}
          /> */}
                    {/* <ImageView
            images={imageModal}
            isVisible={
              typeof this.state.modalStatus[i] !== "undefined" &&
              this.state.modalStatus[i] !== null
                ? this.state.modalStatus[i]
                : false
            }
            onClose={() => {
              this.openClose(i);
            }}
          /> */}
                    {/* flex block */}
                    <View style={{ flex: 1 }}>
                        <Text>{item.Name}</Text>
                        {/* <TouchableOpacity
              onPress={() => {
                this.openClose(i);
              }}
            >
              <Image
                source={{
                  uri: `http://filaretai.lexitacrm.lt${item.PhotoUrl}`
                }}
                resizeMode="contain"
                resizeMethod="scale"
                style={{
                  width: this.props.boxSize * 2,
                  height: this.props.boxSize * 2,
                  padding: 10,
                  resizeMode: "contain"
                }}
              />
            </TouchableOpacity> */}
                    </View>
                </View>
            );
        });
        return options;
    }
    updateDropzoneChild(id) {
        // console.log(this.dropzoneValues[id].ref)
    }
    getDropZoneValues() {
        return { values: this.dropzoneValues, states: this.dropZoneStates };
    }
    measureCallback(values, ref, cref) {
        this.dropzoneValues.push({ values, ref, cref });
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={{ paddingHorizontal: 15, paddingVertical: 5 }}>
                    <HTMLView stylesheet={htmlViewStyle} value={this.props.name} />
                </View>
                <View style={styles.scrollContainer}>{this.renderOptions()}</View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    scrollContainer: {
        flex: 10,
        justifyContent: "flex-start",
        flexDirection: "column"
    },
    optionRow: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-evenly",
        alignItems: "center"
    },
    dropzone: {
        borderRadius: 20,
        borderColor: "black",
        borderWidth: 2,
        flex: 1,
        height: "100%",
        width: "100%"
    },
    container: { flex: 1 },
    questionText: { fontSize: PixelRatio.get() * 16 }
});
