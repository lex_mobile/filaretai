import React from "react";
import Svg, { Defs, Path, G, Use } from "react-native-svg";
// SVGR has dropped some elements not supported by react-native-svg: title, mask
const Bg = props => (
  <Svg
    width={850}
    height={1280}
    xmlnsXlink="http://www.w3.org/1999/xlink"
    {...props}
  >
    <Defs>
      <Path id="a" d="M0 0h264v308H0z" />
      <Path id="c" d="M.167.31H291v54H.167z" />
    </Defs>
    <G fill="none" fillRule="evenodd" >
      <Path
        fill="#01A99E"
        fillRule="nonzero"
        d="M-2.583 1h854.167v1123H-2.583z"
      />
      <G fill="#E1EDED" fillRule="nonzero" opacity={0.3}>
        <Path d="M-46-4h92v889h-92zM123-4h92v889h-92zM293-4h92v889h-92zM463-4h92v889h-92zM633-4h92v889h-92zM803-4h92v889h-92z" />
      </G>
      <Path
        fill="#E1EDED"
        fillRule="nonzero"
        d="M-2.583 885.397h854.167V1281H-2.583z"
      />
      <G transform="translate(211 386)">
        <Use fill="#D8D8D8" xlinkHref="#a" />
        <G mask="url(#b)">
          <G opacity={0.4}>
            <Path
              fill="#156577"
              fillRule="nonzero"
              d="M-131.925 9.895h393.258V307.08h-393.258z"
            />
            <G fill="#156577" fillRule="nonzero">
              <Path d="M-100.492 158.488h35.021V307.08h-35.021zM130.133 158.488h35.021V307.08h-35.021zM188.558 129.452h35.021V307.08h-35.021zM26.438 307.08H-18.15V120.913l44.587 37.576zM-129.875 73.09h20.671v233.99h-20.671z" />
              <Path d="M-83.067 250.889h78.583v56.192h-78.583zM75.637 194.526h78.583V307.08H75.637z" />
              <Path d="M53.258 158.488h35.021V307.08H53.258z" />
              <Path d="M62.654 108.957h16.058V307.08H62.654z" />
            </G>
            <Path
              stroke="#156577"
              strokeWidth={6}
              d="M-131.925 9.895h393.258V307.08h-393.258z"
            />
            <G fill="#156577" fillRule="nonzero">
              <Path d="M249.204 76.676c-2.392-18.104-33.312-26.644-37.242-9.052-.17-.683-.512-1.366-1.025-1.879-4.783-4.611-10.933-7.173-17.254-3.928-3.246 1.708-4.954 4.611-5.466 7.856-11.959 0-24.088 10.248-20.671 23.57 3.246 12.64 19.646 16.055 30.579 10.76 8.2 9.053 23.917 8.37 32.8 0 1.88 1.538 4.1 2.733 6.15 3.416 10.762 4.1 26.308-.512 29.042-12.638 2.391-11.102-7.346-17.422-16.913-18.105z" />
              <Path
                d="M53.942 76.676C51.55 58.572 20.629 50.032 16.7 67.624c-.17-.683-.513-1.366-1.025-1.879-4.783-4.611-10.933-7.173-17.254-3.928-3.246 1.708-4.954 4.611-5.467 7.856-11.958 0-24.087 10.248-20.67 23.57 3.245 12.64 19.645 16.055 30.578 10.76 8.2 9.053 23.917 8.37 32.8 0 1.88 1.538 4.1 2.733 6.15 3.416 10.763 4.1 26.309-.512 29.042-12.638 2.563-11.102-7.175-17.422-16.912-18.105z"
                opacity={0.7}
              />
            </G>
            <Path
              fill="#156577"
              fillRule="nonzero"
              d="M4.4 9.895h9.225V307.08H4.4z"
            />
            <Path
              fill="#156577"
              fillRule="nonzero"
              d="M-126.8 16.556H5.254v9.735H-126.8zM-126.8 34.319H5.254v9.735H-126.8zM-126.8 51.91H5.254v9.736H-126.8zM-126.8 69.674H5.254v9.735H-126.8zM-126.8 87.266H5.254V97H-126.8zM-126.8 105.028H5.254v9.735H-126.8zM-126.8 122.62H5.254v9.735H-126.8zM-126.8 140.213H5.254v9.735H-126.8zM-126.8 157.975H5.254v9.735H-126.8zM-126.8 175.567H5.254v9.735H-126.8zM-126.8 193.33H5.254v9.735H-126.8zM-126.8 210.922H5.254v9.735H-126.8z"
            />
            <Path
              fill="#156577"
              fillRule="nonzero"
              d="M-126.8 13.31H5.254v9.736H-126.8zM-126.8 31.074H5.254v9.735H-126.8zM-126.8 48.666H5.254V58.4H-126.8zM-126.8 66.428H5.254v9.735H-126.8zM-126.8 84.02H5.254v9.735H-126.8zM-126.8 101.783H5.254v9.735H-126.8zM-126.8 119.375H5.254v9.735H-126.8zM-126.8 136.967H5.254v9.735H-126.8zM-126.8 154.73H5.254v9.735H-126.8zM-126.8 172.322H5.254v9.735H-126.8zM-126.8 190.085H5.254v9.735H-126.8zM-126.8 207.677H5.254v9.735H-126.8z"
            />
          </G>
          <Path
            fill="#8ADAF5"
            fillRule="nonzero"
            d="M-138.758 3.063H254.5v297.186h-393.258z"
          />
          <G fill="#61AAC0" fillRule="nonzero">
            <Path d="M-107.325 151.656h35.02v148.593h-35.02zM123.3 151.656h35.02v148.593H123.3zM181.725 122.62h35.02v177.628h-35.02zM19.604 300.249h-44.587V114.08l44.587 37.575zM-136.708 66.258h20.67v233.99h-20.67z" />
            <Path d="M-89.9 244.057h78.583v56.192H-89.9zM68.804 187.694h78.583v112.555H68.804z" />
            <Path d="M46.425 151.656h35.021V300.25h-35.02z" />
            <Path d="M55.821 102.125H71.88V300.25H55.821z" />
          </G>
          <Path
            stroke="#FFF"
            strokeWidth={6}
            d="M-138.758 3.063H254.5v297.186h-393.258z"
          />
          <G transform="translate(-39.917 49.69)">
            <Path
              d="M282.288 20.154c-2.392-18.104-33.313-26.644-37.242-9.052-.171-.683-.513-1.367-1.025-1.879-4.783-4.611-10.933-7.173-17.254-3.928-3.246 1.708-4.954 4.611-5.467 7.856-11.958 0-24.088 10.248-20.67 23.57 3.245 12.64 19.645 16.055 30.578 10.76 8.2 9.053 23.917 8.37 32.8 0 1.88 1.538 4.1 2.733 6.15 3.416 10.763 4.1 26.309-.512 29.042-12.639 2.392-11.101-7.346-17.42-16.912-18.104z"
              fill="#F7FAFB"
              fillRule="nonzero"
              mask="url(#d)"
            />
            <Path
              d="M87.025 20.154C84.633 2.05 53.712-6.49 49.783 11.102c-.17-.683-.512-1.367-1.025-1.879-4.783-4.611-10.933-7.173-17.254-3.928-3.246 1.708-4.954 4.611-5.466 7.856-11.959 0-24.088 10.248-20.671 23.57 3.246 12.64 19.645 16.055 30.579 10.76 8.2 9.053 23.916 8.37 32.8 0 1.879 1.538 4.1 2.733 6.15 3.416 10.762 4.1 26.308-.512 29.041-12.639 2.563-11.101-7.174-17.42-16.912-18.104z"
              fill="#F7FAFB"
              fillRule="nonzero"
              opacity={0.7}
              mask="url(#d)"
            />
          </G>
          <Path
            fill="#FFF"
            fillRule="nonzero"
            d="M-2.433 3.063h9.225v297.186h-9.225zM127.567 3.063h9.225v297.186h-9.225z"
          />
          <G fillRule="nonzero">
            <G opacity={0.4} fill="#156577">
              <Path d="M-3.633 9.724h132.054v9.735H-3.633zM-3.633 27.487h132.054v9.735H-3.633zM-3.633 45.08h132.054v9.734H-3.633zM-3.633 62.841h132.054v9.735H-3.633zM-3.633 80.434h132.054v9.735H-3.633zM-3.633 98.196h132.054v9.735H-3.633zM-3.633 115.788h132.054v9.735H-3.633zM-3.633 133.38h132.054v9.735H-3.633zM-3.633 151.143h132.054v9.735H-3.633zM-3.633 168.735h132.054v9.735H-3.633zM-3.633 186.498h132.054v9.735H-3.633zM-3.633 204.09h132.054v9.735H-3.633z" />
            </G>
            <G fill="#FFF">
              <Path d="M-3.633 6.48h132.054v9.734H-3.633zM-3.633 24.241h132.054v9.735H-3.633zM-3.633 41.833h132.054v9.735H-3.633zM-3.633 59.596h132.054v9.735H-3.633zM-3.633 77.188h132.054v9.735H-3.633zM-3.633 94.951h132.054v9.735H-3.633zM-3.633 112.543h132.054v9.735H-3.633zM-3.633 130.135h132.054v9.735H-3.633zM-3.633 147.898h132.054v9.735H-3.633zM-3.633 165.49h132.054v9.735H-3.633zM-3.633 183.253h132.054v9.735H-3.633zM-3.633 200.845h132.054v9.735H-3.633z" />
            </G>
          </G>
        </G>
      </G>
    </G>
  </Svg>
);

export default Bg;
