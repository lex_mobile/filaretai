import React from 'react'
import { Svg } from 'expo';
const { Defs, Path, G, Use } = Svg;
import {Dimensions} from 'react-native';
const { width, height } = Dimensions.get('window');

const SvgComponent = () => <Svg width={width} height={height} xmlnsXlink="http://www.w3.org/1999/xlink">
  <Defs>
    <Path id="path-1" d="M0 0h264v308H0z" />
    <Path id="path-3" d="M.167.31H291v54H.167z" />
  </Defs>
  <G id="Page-1" fill="none" fillRule="evenodd">
    <G id="Pixel">
      <G id="403231-PD3IXG-89" transform="translate(-46 -4)">
        <G id="Background">
          <Path id="Rectangle-path" fill="#01A99E" fillRule="nonzero" d="M43.417 5h854.167v1123H43.417z" />
          <G id="wall" fill="#E1EDED" fillRule="nonzero" opacity={.3}>
            <Path id="Rectangle-path" d="M0 0h92v889H0zM169 0h92v889h-92zM339 0h92v889h-92zM509 0h92v889h-92zM679 0h92v889h-92zM849 0h92v889h-92z" />
          </G>
          <Path id="Rectangle-path" fill="#E1EDED" fillRule="nonzero" d="M43.417 889.397h854.167V1285H43.417z" />
          <G id="window" transform="translate(257 390)">
            <Use id="Mask" fill="#D8D8D8" href="#path-1" />
            <G id="Group" mask="url(#mask-2)">
              <G opacity={.4} transform="translate(-132.917 9.832)">
                <Path id="Rectangle-path" fill="#156577" fillRule="nonzero" d="M.992.063H394.25v297.186H.992z" />
                <G transform="translate(3.042 63.062)" fill="#156577" fillRule="nonzero">
                  <Path id="Rectangle-path" d="M29.383 85.594h35.021v148.593H29.383zM260.008 85.594h35.021v148.593h-35.021zM318.433 56.558h35.021v177.628h-35.021z" />
                  <Path id="Shape" d="M156.313 234.187h-44.588V48.019l44.588 37.575z" />
                  <Path id="Rectangle-path" d="M0 .196h20.671v233.991H0zM46.808 177.995h78.583v56.192H46.808zM205.512 121.632h78.583v112.555h-78.583z" />
                  <Path id="Rectangle-path" d="M183.133 85.594h35.021v148.593h-35.021z" />
                  <Path id="Rectangle-path" d="M192.529 36.063h16.058v198.124h-16.058z" />
                </G>
                <Path id="Rectangle-path" stroke="#156577" strokeWidth={6} d="M.992.063H394.25v297.186H.992z" />
                <G fill="#156577" fillRule="nonzero" id="Shape">
                  <Path d="M278.288 19.154c-2.392-18.104-33.313-26.644-37.242-9.052-.171-.683-.513-1.367-1.025-1.879-4.783-4.611-10.933-7.173-17.254-3.928-3.246 1.708-4.954 4.611-5.467 7.856-11.958 0-24.088 10.248-20.67 23.57 3.245 12.64 19.645 16.055 30.578 10.76 8.2 9.053 23.917 8.37 32.8 0 1.88 1.538 4.1 2.733 6.15 3.416 10.763 4.1 26.309-.512 29.042-12.639 2.392-11.101-7.346-17.42-16.912-18.104z" transform="translate(103.833 47.69)" />
                  <Path d="M83.025 19.154C80.633 1.05 49.712-7.49 45.783 10.102c-.17-.683-.512-1.367-1.025-1.879-4.783-4.611-10.933-7.173-17.254-3.928-3.246 1.708-4.954 4.611-5.466 7.856-11.959 0-24.088 10.248-20.671 23.57 3.245 12.64 19.645 16.055 30.579 10.76 8.2 9.053 23.916 8.37 32.8 0 1.879 1.538 4.1 2.733 6.15 3.416 10.762 4.1 26.308-.512 29.041-12.639 2.563-11.101-7.174-17.42-16.912-18.104z" opacity={.7} transform="translate(103.833 47.69)" />
                </G>
                <Path id="Rectangle-path" fill="#156577" fillRule="nonzero" d="M137.317.063h9.225v297.186h-9.225z" />
                <Path id="Rectangle-path" fill="#156577" fillRule="nonzero" d="M6.117 6.724h132.054v9.735H6.117zM6.117 24.487h132.054v9.735H6.117zM6.117 42.079h132.054v9.735H6.117zM6.117 59.842h132.054v9.735H6.117zM6.117 77.434h132.054v9.735H6.117zM6.117 95.196h132.054v9.735H6.117zM6.117 112.788h132.054v9.735H6.117zM6.117 130.381h132.054v9.735H6.117zM6.117 148.143h132.054v9.735H6.117zM6.117 165.735h132.054v9.735H6.117zM6.117 183.498h132.054v9.735H6.117zM6.117 201.09h132.054v9.735H6.117z" />
                <Path id="Rectangle-path" fill="#156577" fillRule="nonzero" d="M6.117 3.479h132.054v9.735H6.117zM6.117 21.242h132.054v9.735H6.117zM6.117 38.834h132.054v9.735H6.117zM6.117 56.596h132.054v9.735H6.117zM6.117 74.188h132.054v9.735H6.117zM6.117 91.951h132.054v9.735H6.117zM6.117 109.543h132.054v9.735H6.117zM6.117 127.135h132.054v9.735H6.117zM6.117 144.898h132.054v9.735H6.117zM6.117 162.49h132.054v9.735H6.117zM6.117 180.253h132.054v9.735H6.117zM6.117 197.845h132.054v9.735H6.117z" />
              </G>
              <G transform="translate(-139.75 3)">
                <Path id="Rectangle-path" stroke="#FFF" strokeWidth={6} d="M.992.063H394.25v297.186H.992z" />
                <Path id="Rectangle-path" fill="#8ADAF5" d="M.992.063H394.25v297.186H.992z" />
                <G transform="translate(3.042 63.062)" fill="#61AAC0" fillRule="nonzero">
                  <Path id="Rectangle-path" d="M29.383 85.594h35.021v148.593H29.383zM260.008 85.594h35.021v148.593h-35.021zM318.433 56.558h35.021v177.628h-35.021z" />
                  <Path id="Shape" d="M156.313 234.187h-44.588V48.019l44.588 37.575z" />
                  <Path id="Rectangle-path" d="M0 .196h20.671v233.991H0zM46.808 177.995h78.583v56.192H46.808zM205.512 121.632h78.583v112.555h-78.583z" /> */}
                   <G id="Rectangle-path">
                    <Path d="M.342 49.727h35.021V198.32H.342z" transform="translate(182.792 35.867)" />
                    <Path d="M9.738.196h16.058V198.32H9.738z" transform="translate(182.792 35.867)" />
                  </G>
                </G>
             
                <G transform="translate(99.833 46.69)" id="Shape-+-Shape-Mask">
                  <Path d="M282.288 20.154c-2.392-18.104-33.313-26.644-37.242-9.052-.171-.683-.513-1.367-1.025-1.879-4.783-4.611-10.933-7.173-17.254-3.928-3.246 1.708-4.954 4.611-5.467 7.856-11.958 0-24.088 10.248-20.67 23.57 3.245 12.64 19.645 16.055 30.578 10.76 8.2 9.053 23.917 8.37 32.8 0 1.88 1.538 4.1 2.733 6.15 3.416 10.763 4.1 26.309-.512 29.042-12.639 2.392-11.101-7.346-17.42-16.912-18.104z" id="Shape" fill="#F7FAFB" fillRule="nonzero" mask="url(#mask-4)" />
                  <Path d="M87.025 20.154C84.633 2.05 53.712-6.49 49.783 11.102c-.17-.683-.512-1.367-1.025-1.879-4.783-4.611-10.933-7.173-17.254-3.928-3.246 1.708-4.954 4.611-5.466 7.856-11.959 0-24.088 10.248-20.671 23.57 3.246 12.64 19.645 16.055 30.579 10.76 8.2 9.053 23.916 8.37 32.8 0 1.879 1.538 4.1 2.733 6.15 3.416 10.762 4.1 26.308-.512 29.041-12.639 2.563-11.101-7.174-17.42-16.912-18.104z" id="Shape" fill="#F7FAFB" fillRule="nonzero" opacity={.7} mask="url(#mask-4)" />
                </G>
                <Path id="Rectangle-path" fill="#FFF" fillRule="nonzero" d="M137.317.063h9.225v297.186h-9.225z" />
                <Path id="Rectangle-path-Copy" fill="#FFF" fillRule="nonzero" d="M267.317.063h9.225v297.186h-9.225z" />
                <G fillRule="nonzero" id="Rectangle-path">
                  <G opacity={.4} fill="#156577">
                    <Path d="M.367.025h132.054V9.76H.367zM.367 17.788h132.054v9.735H.367zM.367 35.38h132.054v9.735H.367zM.367 53.142h132.054v9.735H.367zM.367 70.735h132.054v9.735H.367zM.367 88.497h132.054v9.735H.367zM.367 106.089h132.054v9.735H.367zM.367 123.681h132.054v9.735H.367zM.367 141.444h132.054v9.735H.367zM.367 159.036h132.054v9.735H.367zM.367 176.799h132.054v9.735H.367zM.367 194.391h132.054v9.735H.367z" transform="translate(135.75 6.7)" />
                  </G>
                  <G fill="#FFF">
                    <Path d="M.367.196h132.054v9.735H.367zM.367 17.958h132.054v9.735H.367zM.367 35.55h132.054v9.735H.367zM.367 53.313h132.054v9.735H.367zM.367 70.905h132.054v9.735H.367zM.367 88.668h132.054v9.735H.367zM.367 106.26h132.054v9.735H.367zM.367 123.852h132.054v9.735H.367zM.367 141.615h132.054v9.735H.367zM.367 159.207h132.054v9.735H.367zM.367 176.97h132.054v9.735H.367zM.367 194.562h132.054v9.735H.367z" transform="translate(135.75 3.283)" />
                  </G>
                </G>
              </G>
            </G>
          </G>
        </G>
      </G>
    </G>
  </G>
</Svg>


export default SvgComponent