import React from 'react'
import { Svg } from 'expo';
const { G, LinearGradient, Stop, Path } = Svg;
const SvgComponent = props => (
    <Svg viewBox="-179 232 40.1 38" {...props}>
    <G transform="translate(531 29)">
      <LinearGradient
        id="a"
        gradientUnits="userSpaceOnUse"
        x1='-1428.299'
        y1='48.775'
        x2='-1428.299'
        y2='49.93'
        gradientTransform="matrix(40 0 0 38 56442 -1646)"
      >
        <Stop offset='0' stopColor="#ffcf95" />
        <Stop offset='0.427' stopColor="#ffc954" />
        <Stop offset='1' stopColor="#ffc200" />
      </LinearGradient>
      <Path
        d="M-688.4 203.9l5.3 10.7 11.8 1.7c1.3.2 1.9 1.8.9 2.8l-8.6 8.3 2 11.7c.2 1.3-1.2 2.4-2.4 1.7l-10.5-5.5-10.6 5.5c-1.2.6-2.6-.4-2.4-1.7l2-11.7-8.6-8.3c-1-.9-.4-2.6.9-2.8l11.8-1.7 5.3-10.7c.7-1.2 2.5-1.2 3.1 0z"
        fill="url(#a)"
      />
      <LinearGradient
        id="b"
        gradientUnits="userSpaceOnUse"
        x1='-1468.738'
        y1='64.15'
        x2='-1470.711'
        y2='64.15'
        gradientTransform="matrix(7 0 0 20 9602 -1070)"
      >
        <Stop offset='0' stopColor="#ffc200" stopOpacity='0' />
        <Stop offset='0.203' stopColor="#fb0" stopOpacity='0.203' />
        <Stop offset='0.499' stopColor="#ffa700" stopOpacity='0.499' />
        <Stop offset='0.852' stopColor="#f80" stopOpacity='0.852' />
        <Stop offset='1' stopColor="#ff7800" />
      </LinearGradient>
      <Path
        d="M-682.9 214.5l-5.5-10.6c-.3-.6-.9-.9-1.5-.9v20l7-8.5z"
        fill="url(#b)"
      />
      <LinearGradient
        id="c"
        gradientUnits="userSpaceOnUse"
        x1='-1436.963'
        y1='96.909'
        x2='-1437.14'
        y2='95.17'
        gradientTransform="matrix(20 0 0 10 28062 -735)"
      >
        <Stop offset='0' stopColor="#ffc200" stopOpacity='0' />
        <Stop offset='0.203' stopColor="#fb0" stopOpacity='0.203' />
        <Stop offset='0.499' stopColor="#ffa700" stopOpacity='0.499' />
        <Stop offset='0.852' stopColor="#f80" stopOpacity='0.852' />
        <Stop offset='1' stopColor="#ff7800" />
      </LinearGradient>
      <Path
        d="M-679 228l8.6-8.3c.5-.5.6-1.1.4-1.7l-19.9 5.8 10.9 4.2z"
        fill="url(#c)"
      />
      <LinearGradient
        id="d"
        gradientUnits="userSpaceOnUse"
        x1='-1449.689'
        y1='68.253'
        x2='-1448.565'
        y2='67.48'
        gradientTransform="matrix(12 0 0 18 16702 -986.002)"
      >
        <Stop offset='0' stopColor="#ffc200" stopOpacity='0' />
        <Stop offset='0.203' stopColor="#fb0" stopOpacity='0.203' />
        <Stop offset='0.499' stopColor="#ffa700" stopOpacity='0.499' />
        <Stop offset='0.852' stopColor="#f80" stopOpacity='0.852' />
        <Stop offset='1' stopColor="#ff7800" />
      </LinearGradient>
      <Path
        d="M-677.9 240.7l-12-17.7v12.2l10.3 5.6c.6.3 1.2.2 1.7-.1z"
        fill="url(#d)"
      />
      <LinearGradient
        id="e"
        gradientUnits="userSpaceOnUse"
        x1='-1447.175'
        y1='69.347'
        x2='-1446.39'
        y2='69.832'
        gradientTransform="matrix(13 0 0 17 18109 -954)"
      >
        <Stop offset='0' stopColor="#ffc200" stopOpacity='0' />
        <Stop offset='0.203' stopColor="#fb0" stopOpacity='0.203' />
        <Stop offset='0.499' stopColor="#ffa700" stopOpacity='0.499' />
        <Stop offset='0.852' stopColor="#f80" stopOpacity='0.852' />
        <Stop offset='1' stopColor="#ff7800" />
      </LinearGradient>
      <Path
        d="M-689.9 223l-11 4.1-2 11.4c-.1.6.2 1.2.6 1.5l12.4-17z"
        fill="url(#e)"
      />
      <LinearGradient
        id="f"
        gradientUnits="userSpaceOnUse"
        x1='-1436.931'
        y1='101.932'
        x2='-1437.139'
        y2='103.287'
        gradientTransform="matrix(20 0 0 9 28042 -707)"
      >
        <Stop offset='0' stopColor="#ffc200" stopOpacity='0' />
        <Stop offset='0.203' stopColor="#fb0" stopOpacity='0.203' />
        <Stop offset='0.499' stopColor="#ffa700" stopOpacity='0.499' />
        <Stop offset='0.852' stopColor="#f80" stopOpacity='0.852' />
        <Stop offset='1' stopColor="#ff7800" />
      </LinearGradient>
      <Path
        d="M-709.9 217l20 6-6.8-9-11.9 1.8c-.6.1-1.1.6-1.3 1.2z"
        fill="url(#f)"
      />
      <LinearGradient
        id="g"
        gradientUnits="userSpaceOnUse"
        x1='-1436.152'
        y1='94.789'
        x2='-1437.67'
        y2='96.082'
        gradientTransform="matrix(20 0 0 10 28042 -735)"
      >
        <Stop offset='0' stopColor="#ffc200" stopOpacity='0' />
        <Stop offset='0.203' stopColor="#fb0" stopOpacity='0.203' />
        <Stop offset='0.499' stopColor="#ffa700" stopOpacity='0.499' />
        <Stop offset='0.852' stopColor="#f80" stopOpacity='0.852' />
        <Stop offset='1' stopColor="#ff7800" />
      </LinearGradient>
      <Path
        d="M-700.8 228l-8.6-8.3c-.5-.5-.6-1.1-.4-1.7l19.9 5.8-10.9 4.2z"
        fill="url(#g)"
      />
    </G>
  </Svg>
)

export default SvgComponent
