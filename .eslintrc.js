module.exports = {
  "parser": "babel-eslint",
    "extends": [
    "eslint:recommended",
    "airbnb",
    "plugin:react-redux/recommended"
],
    "env": {
        "react-native/react-native": true,
        "jest": true,
      },
    "parserOptions": {
        "ecmaFeatures": {
          "jsx": true
        }
      },
      "plugins": [
        "react",
        "react-native",
        "react-redux",
      ],
      "rules": {
        "react/destructuring-assignment": [0, 'never'],
        "no-console": 1,
        "no-plusplus": 0,
        "max-len": 0,
        "consistent-return": 0,
        "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }],
        "react/prop-types": 0,
        "no-param-reassign": ["error", { "props": false }],
        "class-methods-use-this": 0,
        "eqeqeq": 0,
        "prefer-const": 0,
        "react/prefer-stateless-function": 0,
        "default-case": 0,
        "no-else-return": 0,
        "linebreak-style": 0,
      }
};