import React from 'react';
import {
  View,
  Animated,
  PanResponder,
  Dimensions,
} from 'react-native';
import {Text} from 'native-base';
import { styleVariables } from '../globals';
class DragableComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pan: new Animated.ValueXY(),
      dropZoneValues: {},
      draggableCenter: {},
      color: '#cfe076',
    };
    this.PanResponder = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => {
        let dropZone = this.props.dropZoneValue;
        let found = false;
        dropZone.map((val, i) => {
          if (evt.nativeEvent.pageX > val.x && evt.nativeEvent.pageX < val.x + val.width &&
              evt.nativeEvent.pageY > val.y && evt.nativeEvent.pageY < val.y + val.height) {
            val.i = -1;
            found = true;
          }
        });
        if (!found) {
          let centerX = (72 / 2) - evt.nativeEvent.locationX;
          let centerY = (72 / 2) - evt.nativeEvent.locationY;
          this.state.pan.x._startingValue = evt.nativeEvent.pageX;
          this.state.pan.y._startingValue = evt.nativeEvent.pageY;
          this.state.pan.setOffset({ x: -centerX, y: -centerY });
        } else {
          this.props.saveAnswer({id: this.props.id, A: null});
          Animated.spring(this.state.pan, { toValue: { x: 0, y: 0 } }).start();
          this.state.pan.setOffset({ x: 0, y: 0 });
        }
        return !found;
      },
      onPanResponderMove: (event, gestureState) => {
        return Animated.event([
          null,
          {
            dx: this.state.pan.x,
            dy: this.state.pan.y,
          }
        ])(event, gestureState);
      },
      onPanResponderRelease: (e, gesture) => {
        let isDropZoneResult = this.isDropZone(gesture);
        if (isDropZoneResult.isInZone) {
          if(this.props.dropZoneValue[isDropZoneResult.whichZone].id === this.props.id) {
            this.props.saveAnswer({id: this.props.id, A: true});
          }
          else {
            this.props.saveAnswer({id: this.props.id, A: false});
          }
        } else {
          this.props.saveAnswer({id: this.props.id, A: null});
          Animated.spring(this.state.pan, { toValue: { x: 0, y: 0 } }).start();
          this.state.pan.setOffset({ x: 0, y: 0 });
        }
      },
      onPanResponderTerminate: (e, gestureState) => { return this.state.found; },
    });
    }

  isDropZone(gesture) {
    let dropZone = this.props.dropZoneValue;
    let isInZone = false;
    let whichZone;
    dropZone.map((val, i) => {
      if (Math.floor(gesture.moveX) > Math.floor(val.x) && Math.floor(gesture.moveX) <Math.floor(val.x + val.width)
          && Math.floor(gesture.moveY) > Math.floor(val.y) && Math.floor(gesture.moveY) < Math.floor(val.y + val.height)) {
        isInZone = true;
        whichZone = i;
        var setCenterX = (val.x + (val.width/3));
        var setCenterY = (val.y + (val.height/2));
        let offsetDx = setCenterX - gesture.moveX;
        let offsetDy = setCenterY - gesture.moveY;
        dropZone[i].i = i;
        Animated.spring(this.state.pan, { toValue: { x: this.state.pan.x._value + offsetDx, y: this.state.pan.y._value + offsetDy } }).start(()=>{
          this.state.pan.x._value = this.state.pan.x._value + offsetDx;
          this.state.pan.y._value = this.state.pan.y._value + offsetDy;
        });
      }
    });
    return {isInZone, whichZone};
  }

  render() {
    const circleText = this.props.circleText;
    return (
      <View style={{ width: Dimensions.get('window').width / 4}}>
       <Animated.View
          {...this.PanResponder.panHandlers}
          style={[
            this.state.pan.getLayout(),
            {
              flexShrink: 1,
              backgroundColor:'#e9fc88',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'flex-start',
              zIndex: 999,
              width: '100%',
              borderColor: 'black',
              borderWidth: 1,
              
              
            },
          ]}
        >
          <Text adjustsFontSizeToFit style={{ textAlignVertical: "center", textAlign: "center", fontSize:styleVariables.fontSize, alignSelf:'stretch'}}>{circleText}</Text>
        </Animated.View>
      </View>
    );
  }
}

export default DragableComponent;
