import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  Image,
  PixelRatio,
  Dimensions,
  TouchableOpacity,
  Platform,
  TouchableHighlight,
  ScrollView
} from "react-native";
import HTMLView from "react-native-htmlview";
import DraggableContainer from "./draggableContainerNew";
import Dropzone from "./dropzone";
import { htmlViewStyle } from "../globals";
import { URL } from "../actions/index";
import ImageView from "react-native-image-view";

export default class multipleDragDrop extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalStatus: [],
      show: false
    };
    this.dropzoneValues = [];
    this.dropZoneStates = new Map(
      this.props.opts.map((item, i) => {
        return [i, []];
      })
    );
    this.measureCallback = this.measureCallback.bind(this);
    this.getDropZoneValues = this.getDropZoneValues.bind(this);
    this.checkAnswers = this.checkAnswers.bind(this);
    this.openClose = this.openClose.bind(this);
    this.details = [];
  }

  checkAnswers() {
    let fields = [];

    let requiredMatches = this.state.requiredMatches;

    if (this.dropZoneStates) {
      for (const [key, value] of this.dropZoneStates) {
        for (let v = 0; v < value.length; v++) {
          let ifCorrect = false;
          for (let i = 0; i < this.props.opts.length; i++) {
            if (value[v].pollOptionId === this.props.opts[i].id && key === i) {
              for (let k = 0; k < this.props.opts[i].Details.length; k++) {
                if (
                  value[v].index ===
                  this.props.opts[i].Details[k].PollQuestionAnswerDetailId
                ) {
                  ifCorrect = true;

                  break;
                }
              }
            }
          }
          if (ifCorrect) {
            fields.push(ifCorrect);
          }
        }
      }
    }
    let result =
      fields.length > 0 && fields.length === requiredMatches
        ? fields.every(success => {
            return success === true;
          })
        : false;
    this.props.feedback({
      result,
      options: fields,
      id: this.props.id,
      time: Date.now()
    });
    return result;
  }
  openClose(j) {
    console.log("openClose multiple");
    this.setState(prevState => {
      const newState = prevState.modalStatus.map((item, i) => {
        if (i === j) {
          item = !item;
        }
        return item;
      });
      return { modalStatus: newState };
    });
  }
  showQuestionImage() {
    this.setState(prevState => {
      return { show: !prevState.show };
    });
  }
  componentDidMount() {
    console.log("multipleDragDrop mounted");
    let requiredMatches = 0;
    for (let i = 0; i < this.props.opts.length; i++) {
      requiredMatches += this.props.opts[i].Details.length;
    }
    this.setState({
      modalStatus: new Array(this.props.opts.length).fill(false),
      requiredMatches
    });
  }
  renderOptions = () => {
    this.details = [];
    let options = [];
    let imageModal;
    this.props.opts.map((item, i) => {
      if (item.PhotoUrl) {
        imageModal = [
          {
            source: {
              uri: `http://filaretai.lexitacrm.lt${item.PhotoUrl}`
            }
          }
        ];
      }
      if (item && item.Details !== null) {
        let tempDetails = item.Details.map((element, i) => {
          if (element && element.Question) {
            return (
              <DraggableContainer
                key={`${element.PollQuestionAnswerDetailId}${
                  element.pollOptionId
                }`}
                title={element.Question}
                arrayIndex={element.PollQuestionAnswerDetailId}
                identifier={element.PollOptionId}
                getData={this.getDropZoneValues}
                marginOffset={this.props.boxSize / 50}
                checkAnswers={this.checkAnswers}
              >
                <View
                  style={[styles.dragContainer, { minWidth: this.props.boxSize / 3}]}
                >
                  <Text
                    style={styles.dragContainerText}
                  >
                    {element.Question}
                  </Text>
                </View>
              </DraggableContainer>
            );
          }
        });
        this.details[this.props.randomOrder[i]] = tempDetails;
      }
      options.push(
        <View style={[styles.optionRow]} key={item.id}>
          <View
            style={{
              // width: "100%",
              alignItems: "center",
              justifyContent: "center",
              flex: 2
            }}
          >
            <Dropzone
              style={styles.dropzone}
              title={item.Name}
              arrayIndex={i}
              measureCallback={this.measureCallback}
            />
          </View>
          <ImageView
            images={imageModal}
            isVisible={
              typeof this.state.modalStatus[i] !== "undefined" &&
              this.state.modalStatus[i] !== null
                ? this.state.modalStatus[i]
                : false
            }
            onClose={() => {
              this.openClose(i);
            }}
          />
          <View style={{ flex: 1, padding: 10, alignItems: "center" }}>
            {item.PhotoUrl ? (
              <TouchableOpacity
                onPress={() => {
                  this.openClose(i);
                }}
              >
                <Image
                  source={{
                    uri: `http://filaretai.lexitacrm.lt${item.PhotoUrl}`
                  }}
                  resizeMode="contain"
                  style={{
                    minHeight: this.props.boxSize * 1.5,
                    minWidth: this.props.boxSize * 1.5,
                    maxHeight: this.props.boxSize * 3,
                    maxWidth: this.props.boxSize * 3,
                    borderColor: "black",
                    borderRightWidth: 3,
                    paddingRight: 50
                  }}
                />
              </TouchableOpacity>
            ) : (
              <Text style={{ fontSize: 20, textAlign: "center", fontFamily: "CaveatBrush" }}>
                {item.Name}
              </Text>
            )}
          </View>
        </View>
      );
    });

    return options;
  };
  /**
   * Shuffles array in place.
   * @param {Array} a items An array containing the items.
   */
  shuffle = a => {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
      j = Math.floor(Math.random() * (i + 1));
      x = a[i];
      a[i] = a[j];
      a[j] = x;
    }
    return a;
  };

  getDropZoneValues() {
    return { values: this.dropzoneValues, states: this.dropZoneStates };
  }

  measureCallback(values, ref, cref) {
    this.dropzoneValues.push({ values, ref, cref });
    if (this.dropzoneValues.length === this.props.opts.length) {
      this.dropzoneValues.sort(
        (a, b) => parseInt(a.values.index) - parseInt(b.values.index)
      );
    }
  }
  renderQuestionImage() {
    if (this.props.image) {
      return (
        <View style={{ flex: 1 }}>
          <ImageView
            images={[
              {
                source: {
                  uri: `${URL}${this.props.image}`
                }
              }
            ]}
            imageIndex={0}
            isVisible={this.state.show}
            onClose={() => {
              this.showQuestionImage();
            }}
          />
          <TouchableHighlight
            onPress={() => {
              this.showQuestionImage();
            }}
            style={{ flex: 1 }}
          >
            <Image
              source={{ uri: `${URL}${this.props.image}` }}
              resizeMode="contain"
              style={styles.questionImage}
            />
          </TouchableHighlight>
        </View>
      );
    }
  }
  render() {
    return (
      <View style={styles.container}>
      <View style={{ flex: 1, maxHeight: "30%" }}>
        <ScrollView style={{ flexGrow: 1}}>
            <HTMLView stylesheet={htmlViewStyle} value={this.props.name} />
            {this.renderQuestionImage()}
        </ScrollView>
        </View>
        <View style={styles.scrollContainer}>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-around",
              height: "100%"
            }}
          >
            <View
              style={[
                { flex: 1 },
                Platform.OS === "ios" ? { zIndex: 10 } : null
              ]}
            >
              {this.details}
            </View>
            <View
              style={[
                { flex: 2, paddingVertical: 25, paddingHorizontal: 10 },
                Platform.OS === "ios" ? { zIndex: 0 } : null
              ]}
            >
              {this.renderOptions()}
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  scrollContainer: {
    flex: 1,
    justifyContent: "space-around",
    flexDirection: "column"
  },
  optionRow: {
    flex: 2,
    flexDirection: "row",
    alignItems: "center",
    
  },
  dropzone: {
    borderRadius: 10,
    borderColor: "black",
    borderWidth: 2,
    flex: 1,
    width: "88%",
    height: "100%",
    zIndex: 0
  },
  questionImage: {
    width: Dimensions.get("screen").width - 20,
    height: Dimensions.get("screen").height / 4,
    flex: 1
  },
  container: { flexGrow: 1, backgroundColor: "#fff", opacity: 0.8 },
  questionText: { fontSize: PixelRatio.get() * 16 },
  dragContainerText: {
    color: "black",
    textAlign: "center",
    paddingVertical: 10,
    fontSize: 20,
    fontFamily: "CaveatBrush"
  },
  dragContainer: {
    width: undefined,
    height: undefined,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#00C853",
    borderWidth: 0.5,
    borderColor: "black",
    borderRadius: 6,
    opacity: 0.8,
    zIndex: 99999
  }
});
