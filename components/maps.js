import React, { Component } from "react";
import { StyleSheet, View, FlatList, TouchableOpacity } from "react-native";
import { Input, Button, Text, Spinner, ListItem, Icon } from "native-base";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import MapView from "react-native-maps";
import { fetchMaps } from "../actions/index";
import Geojson from "react-native-geojson";
import { moderateScale } from "./ScaleElements";

class Maps extends Component {
  constructor(props) {
    super(props);
    this.state = { text: "", selectedObject: null, display: false };
    this.map = null;
    this.mapKey = "mapKey";
  }

  render() {
    if( this.state.selectedObject &&
      this.props.mapData.data &&
      this.props.mapData.data.length > 0 &&
      this.props.mapData.data[0].geojson ) {
    return (
      <View style={{ flex: 1 }}>
        {this.props.mapData.inProgress ? <Spinner color="blue" /> : null}
        <View style={styles.inputBlock}>
          <Input
            placeholder="Įveskite šalį, ar miestą.."
            // style={{color: "white"}}
            // placeholderTextColor="white"
            value={this.state.text}
            onChangeText={text => {
              this.setState({ text });
            }}
          />
          <Button
            iconLeft
            style={styles.button}
            onPress={() => {
              if (this.state.text !== "") {
                this.props.fetchMaps(this.state.text, data => {
                  this.mapKey=Math.random().toString();
                  this.setState({ selectedObject: data[0] }, () => {
                    if(this.state.selectedObject && this.state.selectedObject.lat && this.state.selectedObject.lon && this.state.selectedObject.boundingbox) {
                      this.map.animateToRegion(
                        {
                          latitude: parseFloat(this.state.selectedObject.lat),
                          longitude: parseFloat(this.state.selectedObject.lon),
                          latitudeDelta:
                            parseFloat(this.state.selectedObject.boundingbox[1]) -
                            parseFloat(this.state.selectedObject.boundingbox[0]),
                          longitudeDelta:
                            parseFloat(this.state.selectedObject.boundingbox[3]) -
                            parseFloat(this.state.selectedObject.boundingbox[2])
                        },
                        500
                      );
                    }
                  });
                });
              }
            }}
          >
            <Icon
              name="search"
              type="Ionicons"
              style={{ fontSize: moderateScale(16) }}
            />
            <Text
              style={{ fontFamily: "CaveatBrush", fontSize: moderateScale(14) }}
            >
              {" "}
              Paieška
            </Text>
          </Button>

          {this.props.mapData.data && this.props.mapData.data.length > 1 ? (
            <Button
              iconLeft
              style={styles.button}
              onPress={() => {
                console.log(this.state.display);
                this.setState(prev => {
                  return { display: !prev.display };
                });
              }}
            >
              <Icon
                style={{ color: "#fff" }}
                name="arrow-dropdown"
                type="Ionicons"
              />
              <Text
                style={{
                  fontFamily: "CaveatBrush",
                  fontSize: moderateScale(14)
                }}
              >
                Pasirinkti iš sąrašo
              </Text>
            </Button>
          ) : null}
        </View>
        {this.props.mapData.data &&
        this.props.mapData.data.length > 1 &&
        this.state.display ? (
          <View style={styles.listContainer}>
            <FlatList
              style={{ height: 400 }}
              scrollEnabled
              data={this.props.mapData.data}
              renderItem={({ item }) => {
                return (
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({ selectedObject: item, display: false });
                      this.map.animateToRegion(
                        {
                          latitude: parseFloat(item.lat),
                          longitude: parseFloat(item.lon),
                          latitudeDelta:
                            parseFloat(item.boundingbox[1]) -
                            parseFloat(item.boundingbox[0]),
                          longitudeDelta:
                            parseFloat(item.boundingbox[3]) -
                            parseFloat(item.boundingbox[2])
                        },
                        500
                      );
                    }}
                  >
                    <View style={styles.listItem}>
                      <Text style={styles.listText}>{item.display_name}</Text>
                    </View>
                  </TouchableOpacity>
                );
              }}
            />
          </View>
        ) : null}
        <MapView
          // key={this.mapKey}
          ref={ref => {
            this.map = ref;
          }}
          provider={"google"}
          style={{ flex: 1 }}
          initialRegion={{
            latitude: 55.3500003,
            longitude: 23.7499997,
            latitudeDelta: 2.553632,
            longitudeDelta: 6.1817368
          }}
        >
            <Geojson
              viewKey={this.mapKey}
              strokeColor={"#4286f4"}
              strokeWidth={10}
              geojson={{
                type: "FeatureCollection",
                features: [
                  {
                    type: "Feature",
                    properties: {},
                    geometry: { ...this.state.selectedObject.geojson }
                  }
                ]
              }}
            />
        </MapView>
      </View>
    );
    }
    else {
      return (
        <View style={{ flex: 1 }}>
          {this.props.mapData.inProgress ? <Spinner color="blue" /> : null}
          <View style={styles.inputBlock}>
            <Input
              placeholder="Įveskite šalį, ar miestą.."
              // style={{color: "white"}}
              // placeholderTextColor="white"
              value={this.state.text}
              onChangeText={text => {
                this.setState({ text });
              }}
            />
            <Button
              iconLeft
              style={styles.button}
              onPress={() => {
                if (this.state.text !== "") {
                  this.props.fetchMaps(this.state.text, data => {
                    this.setState({ selectedObject: data[0] });
                    this.map.animateToRegion(
                      {
                        latitude: parseFloat(data[0].lat),
                        longitude: parseFloat(data[0].lon),
                        latitudeDelta:
                          parseFloat(data[0].boundingbox[1]) -
                          parseFloat(data[0].boundingbox[0]),
                        longitudeDelta:
                          parseFloat(data[0].boundingbox[3]) -
                          parseFloat(data[0].boundingbox[2])
                      },
                      500
                    );
                  });
                }
              }}
            >
              <Icon
                name="search"
                type="Ionicons"
                style={{ fontSize: moderateScale(16) }}
              />
              <Text
                style={{ fontFamily: "CaveatBrush", fontSize: moderateScale(14) }}
              >
                {" "}
                Paieška
              </Text>
            </Button>
  
            {this.props.mapData.data && this.props.mapData.data.length > 1 ? (
              <Button
                iconLeft
                style={styles.button}
                onPress={() => {
                  console.log(this.state.display);
                  this.setState(prev => {
                    return { display: !prev.display };
                  });
                }}
              >
                <Icon
                  style={{ color: "#fff" }}
                  name="arrow-dropdown"
                  type="Ionicons"
                />
                <Text
                  style={{
                    fontFamily: "CaveatBrush",
                    fontSize: moderateScale(14)
                  }}
                >
                  Pasirinkti iš sąrašo
                </Text>
              </Button>
            ) : null}
          </View>
          {this.props.mapData.data &&
          this.props.mapData.data.length > 1 &&
          this.state.display ? (
            <View style={styles.listContainer}>
              <FlatList
                style={{ height: 400 }}
                scrollEnabled
                data={this.props.mapData.data}
                renderItem={({ item }) => {
                  return (
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({ selectedObject: item, display: false });
                        this.map.animateToRegion(
                          {
                            latitude: parseFloat(item.lat),
                            longitude: parseFloat(item.lon),
                            latitudeDelta:
                              parseFloat(item.boundingbox[1]) -
                              parseFloat(item.boundingbox[0]),
                            longitudeDelta:
                              parseFloat(item.boundingbox[3]) -
                              parseFloat(item.boundingbox[2])
                          },
                          500
                        );
                      }}
                    >
                      <View style={styles.listItem}>
                        <Text style={styles.listText}>{item.display_name}</Text>
                      </View>
                    </TouchableOpacity>
                  );
                }}
              />
            </View>
          ) : null}
          <MapView
            ref={ref => {
              this.map = ref;
            }}
            provider={"google"}
            style={{ flex: 1 }}
            initialRegion={{
              latitude: 55.3500003,
              longitude: 23.7499997,
              latitudeDelta: 2.553632,
              longitudeDelta: 6.1817368
            }}
          >
          </MapView>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  inputBlock: {
    flexDirection: "row",
    justifyContent: "center",
    borderBottomWidth: 1,
    borderColor: "#328CC1"
  },
  button: {
    alignItems: "center",
    backgroundColor: "#328CC1",
    margin: 3
  },
  listContainer: {
    padding: 5,
    borderBottomWidth: 1,
    borderColor: "#328CC1"
  },
  listItem: {
    borderWidth: 1,
    borderRadius: 4,
    borderColor: "#d9d9d9",
    marginVertical: 1,
    height: 60,
    justifyContent: "center"
  },
  listText: {
    fontSize: 16,
    textAlign: "left",
    paddingHorizontal: 6
  }
});
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchMaps }, dispatch);
}
function mapStateToProps(state) {
  return {
    default: state.Default,
    mapData: state.fetchMaps
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Maps);
