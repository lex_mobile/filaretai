import React from 'react';
import { Text, View, TouchableOpacity, Image, Dimensions, Modal, TouchableHighlight} from 'react-native';
import { Camera, Permissions, Constants } from 'expo';
import { Spinner, Button, Left, Right, Header, Icon} from 'native-base';
var {height, width} = Dimensions.get('window');
import Gallery from 'react-native-image-gallery';
import { connect } from 'react-redux';
import { pushAnswer } from '../actions/index';
import { bindActionCreators } from 'redux';

class CameraExample extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
       header: <Header style={{marginTop: Constants.statusBarHeight}}>
         <Left>
           <Button transparent onPress={navigation.getParam('saved')} style={{justifyContent: 'center', alignItems: 'center', alignContent: 'center'}}>
            <Icon name="arrow-back" />
            <Text
              style={{ fontSize: 18, color: "white", marginLeft: 10 }}>
              {' '}Saugoti{' '}
            </Text>
           </Button>
         </Left>
        <Right>
        </Right>
       </Header>,
    };
 }

    camera = null;
    _carousel = null;
    constructor(props) {
        super(props);
        this.state = {
            hasCameraPermission: null,
            type: Camera.Constants.Type.back,
            photos: [],
            ratio: '4:3',
            modalVisible: false,
            loading: 'none',
            startedRecording: false,
            videos: []
        };
        this.save = this.save.bind(this);
    }
  async componentDidMount() {
    this.props.navigation.setParams({ saved: this.save });
    const { status } = await Permissions.askAsync(Permissions.CAMERA, Permissions.AUDIO_RECORDING);
    this.setState({ hasCameraPermission: status === 'granted'});
  }

  snap = async () => {
        console.log('snap');
        if (this.camera) {
          let ratios = await this.camera.getSupportedRatiosAsync();
          this.setState({loading: 'spinner', ratio: ratios[0]});
            let photo = await this.camera.takePictureAsync({quality: 0, skipProcessing: true, exif: false, base64: false});
            this.setState((prevState)=>{
              return { photos: prevState.photos.concat({type: "photo", source: {uri: photo.uri}, dimensions: {width: photo.width, height: photo.height}}) } 
            }, () => {
             console.log(this.state.photos)
             this.setState({loading: 'preview'});
            });
    }
  }

  record = async () => {
    console.log("record");
    if(this.camera) {
      if(!this.state.startedRecording) {
        this.setState({ startedRecording: true });
        let video = await this.camera.recordAsync({quality: Camera.Constants.VideoQuality['4:3'], maxDuration: 60, maxFileSize: 50000000});
        this.setState((prevState)=> {
          return { videos: prevState.videos.concat({ type: "video", source: { uri: video.uri } })};
        }, () => {
          console.log("recording:", this.state.videos);
        });
      } else {
        console.log("stop recording");
        this.setState({startedRecording: false});
        this.camera.stopRecording();
      }
    }
  }

  save(){
    //this.props.pushAnswer({type: 'photo', data: this.state.photos});
    this.props.navigation.getParam('onGoBack')(this.state.photos, this.state.videos);
    console.log("length length length", this.state.photos.length, this.state.videos.length);
    this.props.navigation.goBack();
  }
  _renderItem ({item, index}) {
    return (
      <View style={{flex: 1, width}}>
        <Image source={{uri: item.uri}} style={{width: width, height: height/2}} resizeMode="contain"/>
      </View>
    );
}

setModalVisible(visible) {
  this.setState({modalVisible: visible});
}
renderIcon() {
  if(this.state.loading === 'none') {
    return null;
  } else if (this.state.loading === 'spinner') {
    return <Spinner/>
  } else if(this.state.loading === 'preview') {
    return (<Text style={{ fontSize: 20, marginBottom: 12, color: "white", fontWeight: "bold", borderWidth: 1, borderColor: "#d9d9d9", borderRadius: 8, padding: 10 }}>
    {" "}
    Peržiūrėti{" "}
  </Text>);
  }
}
    render() {
      const { hasCameraPermission } = this.state;
      if (hasCameraPermission === null) {
        return <View />;
      } else if (hasCameraPermission === false) {
        return <Text>No access to camera</Text>;
      } else {
        return (
          <View style={{ flex: 1, flexDirection: 'column'}}>
            <Camera style={{ flex: 1 }} type={this.state.type} ref={ref => {this.camera = ref}} ratio={this.state.ratio}>
              <View
                style={{
                  flex: 1,
                  backgroundColor: 'transparent',
                  flexDirection: 'row',
                  alignItems: 'flex-end',
                }}>
                <View style={{ flexDirection: 'row', flex: 1, alignItems: "center"}}>
                <TouchableOpacity
                  style={{
                    flex: 1,
                    alignItems: "flex-end",
                  }}
                  onPress={() => {
                    this.setState({
                      type: this.state.type === Camera.Constants.Type.back
                        ? Camera.Constants.Type.front
                        : Camera.Constants.Type.back,
                    });
                  }}>
                  <Icon name="md-reverse-camera" style={{fontSize: 60, color: 'white'}} />
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    flex: 1,
                    alignItems: "flex-end",
                  }}
                  onPressIn={() => {
                    this.snap();
                  }}>
                  <Icon name="md-camera" style={{fontSize: 60, color: 'white'}} />
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    flex: 1,
                    alignItems: "flex-end",
                    
                  }}
                  onPressIn={() => {
                    this.record();
                  }}>
                  <Icon name={!this.state.startedRecording ? "md-videocam" : 'pause'} style={{fontSize: 60, color: 'white'}} />
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    flex: 1,
                    alignItems: "flex-end",
                    display: this.state.loading != "null"? "flex":"none"
                  }}
                  onPress={() => {
                    this.state.loading ==='preview'? this.setModalVisible(true): null;
                  }}>
                  {this.renderIcon()}
                </TouchableOpacity>
                </View>
              </View>
            </Camera>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={()=> {
            console.log('closed');
          }}
         >
         <View style={{ backgroundColor: "#3D5AAE", paddingVertical: 15}}>
         <TouchableHighlight
        //  style={{marginTop: Constants.statusBarHeight}}
         onPress={() => {
           this.setModalVisible(!this.state.modalVisible);
         }}>
         <Icon style={{marginLeft: 10, color: "white"}} name="arrow-back" />
       </TouchableHighlight>
       </View>
          <View style={{ flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center'}}>
            <View>
              {/* kad sutvarkyt warninga Gallery npm package viewPager/index.js reik pakeist keyExtractor returna toString() */}
              <Gallery
                style={{ flex: 1, backgroundColor: 'black' }}
                images={this.state.photos}
              />
            </View>
          </View>
        </Modal>
      </View>
        );
      }
    }
  }

  function mapDispatchToProps(dispatch) {
    return bindActionCreators({ pushAnswer }, dispatch);
  }
  export default connect(null, mapDispatchToProps)(CameraExample);
