import React, { Component } from 'react';
import { Image } from 'react-native';
import {
 Card, CardItem, Text, Body, Button, Left, Right, Toast, Picker, Item,
} from 'native-base';
import { URL } from '../actions/index';

export default class QuestionSort extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Options: [],
      result: false,
      showToast: false,
    };
  }

  componentDidMount() {
    let i = 0;
    const answ = this.props.options.map(() => { return i + 1; });
    const opts = this.props.options.map((item) => {
      return ({
        name: item.name,
        image: item.image,
        correct: item.correct,
        sort: 0,
        answ,
      });
    });
    this.setState({ Options: opts });
    this.checkBoxClick = this.checkBoxClick.bind(this);
  }

  onPickerSelect(index, name) {
    this.setState((prevState) => {
      const newState = prevState.Options.map((item) => {
        if (item.name == name) {
          item.sort = index;
        }
      });
      return { Options: newState };
    });
  }

  checkBoxClick(name) {
    this.setState((prevState) => {
      const newState = prevState.Options.map((item) => {
        if (item.name == name) {
          item.answ = !item.answ;
        }
        return (item);
      });
      return { Options: newState };
    });
  }

  renderOptions() {
    const options = this.state.Options.map((item, i) => {
      const image = item.image ? <Image source={{ uri: `${URL}${item.image}` }} style={{ height: 120, flex: 0.5 }} /> : null;

      return (
        <CardItem key={`${item.name}${i}`}>
          <Left>
            {image}
          </Left>
          <Text>{item.name}</Text>
          <Right>
            <Text>
              GROUP TIPO KLAUSIMAS
            </Text>
            {/* <Picker
              style={{ width: 150, height: 180 }}
              selectedValue={item.sort}
              itemStyle={{ color: 'white', fontSize: 26 }}
              onValueChange={(index) => { this.onPickerSelect(index, item.name); }}>
              {
                item.answ.map((value, i) => (<Item label={value} value={i} key={`sort${value}`} />))
              }
            </Picker> */}
          </Right>
        </CardItem>
      );
    });

    return options;
  }

  render() {
    return (
      <Card>
        <CardItem header style={{ alignItems: 'center', justifyContent: 'center' }}>
          <Text>{this.props.name}</Text>
        </CardItem>
        {this.renderOptions()}
        <CardItem footer>
          <Button
            primary
            style={{ flex: 1 }}
            onPress={() => {
              this.checkAnswer();
            }}
          >
            <Text> Tikrinti </Text>
          </Button>
          <Button
            primary
            style={{ flex: 1 }}
            onPress={() => {
              this.props.next();
            }}
          >
            <Text> Next </Text>
          </Button>
        </CardItem>
      </Card>
    );
  }
}
