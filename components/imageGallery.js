import React, { Component, } from "react";
import { Dimensions } from 'react-native';
import { connect } from "react-redux";
import { pushAnswer } from "../actions/index";
import { bindActionCreators } from "redux";
import {
  CameraRoll,
  Text,
  View,
  TouchableOpacity,
  FlatList
} from "react-native";
import {
  Spinner,
  Button,
  Left,
  Right,
  Header,
  Icon,
} from "native-base";
import { Constants, Permissions } from "expo";
import ImageOverlay from "react-native-image-overlay";

class ImageGallery extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      header: (
        <Header style={{ marginTop: Constants.statusBarHeight }}>
          <Left>
            <Button transparent onPress={navigation.getParam("saved")}>
              <Icon name="arrow-back" />
              <Text style={{ fontSize: 18 }}> Saugoti </Text>
            </Button>
          </Left>
          <Right />
        </Header>
      )
    };
  };
  constructor(props) {
    super(props);
    this.save = this.save.bind(this);
    this.state = {
      photos: null,
      endOfGallery: false,
    };
    this._getPhotosAsync = this._getPhotosAsync.bind(this);
    this._loadMorePhotos = this._loadMorePhotos.bind(this);
  }
  save() {
    const selected = this.state.photos.filter(item => item.selected);
    this.props.navigation.getParam("onGoBack")(selected);
    this.props.navigation.goBack();
  }
  async componentDidMount() {
    this.props.navigation.setParams({ saved: this.save });
    const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    this.setState({ hasCameraPermission: status === "granted" }, () => {
      if (this.state.hasCameraPermission) {
        this._getPhotosAsync().catch(error => {
          console.log(error)
        });
      }
    });
  }
  async _getPhotosAsync() {
    
    let photos = await CameraRoll.getPhotos({ first: 50 });
    const filtered = photos.edges.map(item => {
      return {
        type: "photo",
        source: { uri: item.node.image.uri },
        dimensions: {
          width: item.node.image.width,
          height: item.node.image.height
        },
        selected: false
      };
    });
    this.setState({ photos: filtered, cursor: photos.page_info.end_cursor });
  }
  async _loadMorePhotos() {
    if(this.state.endOfGallery) {
    let photos = await CameraRoll.getPhotos({
      first: 25,
      after: this.state.cursor
    });
    const filtered = photos.edges.map(item => {
      return {
        type: "photo",
        source: { uri: item.node.image.uri },
        dimensions: {
          width: item.node.image.width,
          height: item.node.image.height
        },
        selected: false
      };
    });
    this.setState(prevState => {
      return {
        photos: prevState.photos.concat(filtered),
        cursor: photos.page_info.end_cursor,
        endOfGallery: !photos.page_info.has_next_page
      };
    });
  }
  }

  renderPhoto = ({item}) => {
    let width = Dimensions.get('window').width / 3;
    let image = item;
    let overlay = image.selected ? (
      <Icon style={{ color: "green" }} name="md-checkmark-circle" />
    ) : null;
   return (
   <TouchableOpacity
    onPress={() => {
      this.setState(prevState => {
        let index = prevState.photos
          .map(e => {
            return e.source;
          })
          .indexOf(image.source);
        let switched = prevState.photos;
        switched[index].selected = !switched[index].selected;
        return { photos: switched };
      });
    }}
    key={image.source.uri}
  >
    <ImageOverlay
      source={image.source}
      overlayColor={"#08ce08"}
      overlayAlpha={image.selected ? 0.3 : 0}
      contentPosition={"center"}
      containerStyle={{ height: width - 2, width: width - 2, margin: 1 }}
    >
      {overlay}
    </ImageOverlay>
   </TouchableOpacity>);
  }

  // _renderPhotos(photos) {
  //   let images = [];
  //   for (let image of photos) {
  //     let overlay = image.selected ? (
  //       <Icon style={{ color: "green" }} name="md-checkmark-circle" />
  //     ) : null;
  //     images.push(
  //       <TouchableOpacity
  //         onPress={() => {
  //           this.setState(prevState => {
  //             //   const switched = prevState.photos.map(element => {
  //             //     if (element.source === image.source) {
  //             //       element.selected = !element.selected;
  //             //       return element;
  //             //     }
  //             //     return element;
  //             //   });
  //             //   return { photos: switched };
  //             // });
  //             let index = prevState.photos
  //               .map(e => {
  //                 return e.source;
  //               })
  //               .indexOf(image.source);
  //             let switched = prevState.photos;
  //             switched[index].selected = !switched[index].selected;
  //             return { photos: switched };
  //           });
  //         }}
  //         key={image.source.uri}
  //       >
  //         <ImageOverlay
  //           source={image.source}
  //           overlayColor={"#08ce08"}
  //           overlayAlpha={image.selected ? 0.3 : 0}
  //           contentPosition={"center"}
  //           containerStyle={{ height: 200, width: 200 }}
  //         >
  //           {overlay}
  //         </ImageOverlay>
  //       </TouchableOpacity>
  //     );
  //   }
  //   return images;
  // }
  render() {
    let { photos } = this.state;
    // let {height, width} = Dimensions.get('window');
    return (
      <View style={{ flex: 1, flexDirection: "column", alignItems: "center" }}>

          <FlatList
            numColumns={3}
            onEndReachedThreshold={0.5}
            data={photos}
            renderItem={this.renderPhoto}
            keyExtractor={(item, index) => item.source.uri}
            onEndReached={this._loadMorePhotos}
            extraData={this.state}
          />
          {photos ? null : <Spinner />}
        {/* <Button
          style={{ flex: 0.5, alignSelf: "stretch", justifyContent: "center" }}
          onPress={this._loadMorePhotos}
        >
          <Text> Daugiau </Text>
        </Button> */}
      </View>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ pushAnswer }, dispatch);
}
function mapStateToProps(state) {
  return {
    classes: state.classes,
    photos: state.testResults.photos
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ImageGallery);
