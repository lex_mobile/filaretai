import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Modal,
  Image,
  TouchableOpacity,
  Dimensions
} from "react-native";
import { Permissions } from "expo";
import { getCurrentTaskQr } from "../actions/index";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import BarCodeScanner from "expo-barcode-scanner";
import {moderateScale} from "./ScaleElements";

class BarcodeScanner extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hasCameraPermission: null
    };
    this.height = Dimensions.get('screen').height / 2;
    this.width = Dimensions.get('screen').width / 2;
  }

  async componentDidMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === "granted" });
  }

  render() {
    const { hasCameraPermission } = this.state;

    if (hasCameraPermission === null) {
      return <Text>Requesting for camera permission</Text>;
    }
    if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    }
    return (
      <View style={{ flex: 1 }}>
        {this.props.modal ? (
          <Modal
            visible={this.props.modal}
            onRequestClose={this.props.CloseModal}
            presentationStyle="fullScreen"
          >
            <View
                style={{
                  flex: 1,
                  backgroundColor: "#d9d9d9"
                }}
              >
            <View
              style={{alignItems:"center", justifyContent: "center", flex: 1}}>
                <Image
                  source={require("../assets/img/wrongQR.png")}
                  style={{resizeMode: "contain", width: "80%"}}
                />
            <TouchableOpacity
            style={{
              backgroundColor: "#f5a623",
              borderRadius: 15,
              alignItems: "center",
              justifyContent: "center",
              width: 150,
              height: 60
            }}
            onPress={() => {
              this.props.CloseModal();
            }}
          >
            <Text
              adjustsFontSizeToFit
              style={{
                color: "white",
                alignSelf: "center",
                fontSize: moderateScale(22),
                fontFamily: "CaveatBrush",
                padding: 18
              }}
            >
              Grįžti
            </Text>
          </TouchableOpacity>
          </View>
          </View>
          </Modal>
        ) : (
          <BarCodeScanner.BarCodeScanner
            onBarCodeScanned={scan => {
              this.handleBarCodeScanned(scan);
            }}
            style={StyleSheet.absoluteFill}
          />
        )}
      </View>
    );
  }

  handleBarCodeScanned = ({ type, data }) => {
    console.log(data);
    if (!this.props.modal) {
      this.props.getCurrentTaskQr(data);
    }
  };
}

function mapStateToProps(state) {
  return {
    modal: state.currentTask.qr_modal
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      NavToTask: () => ({ type: "NAV_TASKS" }),
      CloseModal: () => ({ type: "RESET_ERROR" }),
      getCurrentTaskQr
    },
    dispatch
  );
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BarcodeScanner);
