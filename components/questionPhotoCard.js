import React, { Component } from "react";
import {
  Image,
  TouchableHighlight,
  StyleSheet,
  Dimensions,
  FlatList,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
  Alert
} from "react-native";
import HTMLView from "react-native-htmlview";
import {
  Card,
  CardItem,
  Text,
  CheckBox,
  Button,
  Left,
  Toast,
  Content,
  Thumbnail,
  Icon,
  View
} from "native-base";
import ImageView from "react-native-image-view";
import { URL } from "../actions/index";
import { connect } from "react-redux";
import { updateTaskAnswerPhotos } from "../actions/index";
import { bindActionCreators } from "redux";
import { NavigationActions } from "react-navigation";
import { NAV_CAMERA, NAV_GALLERY } from "../navigation";
import { styleVariables, htmlViewStyle } from "../globals";
import { Video, ImagePicker, Permissions} from "expo";

const style = StyleSheet.create({
  ContentContainerQuestion: {
    flex: 1,
    flexDirection: "column"
  },
  question: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
    backgroundColor: "transparent",
    borderColor: "transparent"
  },
  header: {
    flex: 5,
    flexDirection: "column",
    width: Dimensions.get("screen").width,
    justifyContent: "center"
  },
  options: {
    flex: 4,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "stretch",
    paddingHorizontal: 20
  },
  option: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-start",
    paddingHorizontal: 20
  },
  footer: {
    flex: 1
  },
  gallery: {
    justifyContent: "space-between",
    flex: 1,
    flexDirection: "row",
    alignItems: "center"
  },
  roundButton: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 20,
    backgroundColor: "#498fff",
    padding: 15,
    margin: 5
  }
});

class QuestionPhotoCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Options: [],
      NChecked: 0,
      NCorrect: 0,
      show: false,
      finish: false,
      photos: [],
      videos: [],
      hasGalleryPermission: false
    };
    this.open = false;
    this.getPhotos = this.getPhotos.bind(this);
  }

  showQuestionImage() {
    this.setState(prevState => {
      return { show: !prevState.show };
    });
  }
  _pickImage = async () => {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if(status === 'granted') {
        let result = await ImagePicker.launchImageLibraryAsync({
          allowsEditing: true,
          aspect: [4, 3],
          mediaTypes: ImagePicker.MediaTypeOptions.All
        });
        if (!result.cancelled) {
          this.setState(
            prevState => {
                return { photos: prevState.photos.concat({...result, source: {uri: result.uri}}) };
            },
            () => {
              if(this.props.task) {
                this.props.photoAddEvent(this.state.photos.length);
              }
            }
          );
        }
      } else {
        Alert.alert(
          'Funkcija negalima',
          'Nesuteikėte teisių aplikacijai naudotis jūsų įrenginio galerija',
          [
            {text: 'OK', onPress: () => console.log('OK Pressed')},
          ],
          { cancelable: false }
        )
      }
 
    
  };
  componentDidUpdate(prevProps, prevState) {
    if (prevState.photos !== this.state.photos) {
      this.props.updateTaskAnswerPhotos(this.state.photos);
    }
  }
  renderQuestionImage() {
    if (this.props.image) {
      return (
        <View>
          <ImageView
            images={[
              {
                source: {
                  uri: `${URL}${this.props.image}`
                }
              }
            ]}
            imageIndex={0}
            isVisible={this.state.show}
            onClose={() => {
              this.showQuestionImage();
            }}
          />
          <TouchableOpacity
            onPress={() => {
              this.showQuestionImage();
            }}
            style={{ flex: 0.5 }}
          >
            <Image
              source={{ uri: `${URL}${this.props.image}` }}
              resizeMode="contain"
              style={{
                width: Dimensions.get("screen").width - 20,
                height: Dimensions.get("screen").height / 4,
                flex: 1
              }}
            />
          </TouchableOpacity>
        </View>
      );
    }
  }
  renderGallery() {
    if (this.state.photos) {
     // console.log(this.state.photos);
      return (
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <TouchableOpacity>
          <Icon name="chevron-left" type="FontAwesome" style={{color: "#fff", display: this.props.task && this.state.photos.length > 0 ? "flex" : "none"}} />
          </TouchableOpacity>
          <FlatList
            horizontal
            style={{ flexWrap: "wrap" }}
            data={this.state.photos}
            renderItem={({ item }) => {
              if(item.type === "photo" || item.type === "image") {
                return (
                  <View
                    key={item.source.uri}
                    style={{ justifyContent: "center", alignItems: "center" }}
                  >
                    <ImageBackground
                      source={item.source}
                      // resizeMode="contain"
                      style={{
                        position: "relative",
                        margin: 3,
                        width: Dimensions.get("screen").width / 2.5 ,
                        height: Dimensions.get("screen").width / 2.5 
                      }}
                    >
                      <TouchableOpacity
                        onPress={() => {
                          this.deletePhoto(item);
                        }}
                        style={{ position: "absolute", zIndex: 999, right: 0, top: 0, padding: 10 }}
                      >
                        <Icon style={{ color: "#b30000",  fontSize: 40, }} name="md-close" />
                      </TouchableOpacity>
                    </ImageBackground>
                  </View>
                );
              } else if(item.type === "video") {
                return (
                  <View
                    key={item.source.uri}
                    style={{ justifyContent: "center", alignItems: "center" }}
                  >
                    <TouchableOpacity
                      onPress={() => {
                        this.deletePhoto(item);
                      }}
                      style={{ position: "absolute", zIndex: 999, right: 0, top: 0, padding: 10 }}
                    >
                      <Icon style={{ color: "#b30000",  fontSize: 40,}} name="md-close" />
                    </TouchableOpacity>
                    <Icon
                        name="film"
                        type="Feather"
                        style={{
                            backgroundColor: '#8281e4',
                            borderRadius: 8,
                            color: '#fff',
                            position: "absolute",
                            right: 0,
                            bottom: 0,
                            padding: 6,
                            bottom: 10,
                            right: 10,
                            zIndex: 999
                        }} />
                      <Video
                        source={item.source}
                     
                        style={{
                            width: Dimensions.get("screen").width / 2.5,
                            height: Dimensions.get("screen").width / 2.5
                        }}
                      />
                  </View>
                );
              }
       
            }}
            keyExtractor={(item, index) => item.source.uri+index}
          />
          <TouchableOpacity>
        <Icon name="chevron-right" type="FontAwesome" style={{color: "#fff", display: this.props.task && this.state.photos.length > 0 ? "flex" : "none"}} />
        </TouchableOpacity>
        </View>
      );
    } else {
      return null;
    }
  }
  deletePhoto(photo) {
    const filtered = this.state.photos.filter(
      item => item.source.uri != photo.source.uri
    );
    this.setState({ photos: filtered }, () => {
      if(this.props.task) {
        this.props.photoAddEvent(this.state.photos.length);
      }
    });
  }
  getPhotos(photos, videos) {
    this.setState(
      prevState => {
          if(typeof videos !== "undefined") {
            return { photos: prevState.photos.concat(photos,videos) };
          }
          return { photos: prevState.photos.concat(photos) };
      },
      () => {
        if(this.props.task) {
          this.props.photoAddEvent(this.state.photos.length);
        }
      }
    );
  }
  render() {
    return (
        <Card
          key={this.props.name}
          style={[
            style.question,
            {
              backgroundColor: this.props.task
                ? "transparent"
                : styleVariables.questionColor,
            }
          ]}
        >
          <CardItem
            header
            style={[
              style.header,
              {
                backgroundColor: this.props.task
                  ? "transparent"
                  : styleVariables.questionColor
              }
            ]}
          >
            <HTMLView value={this.props.name} stylesheet={htmlViewStyle} />
          </CardItem>
          <CardItem
            style={{
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "row",
              backgroundColor: this.props.task
                ? "transparent"
                : styleVariables.questionColor
            }}
          >
            <View style={{flexDirection: 'row'}}>
              <TouchableHighlight
                style={style.roundButton}
                disabled={this.state.finish}
                onPress={() => {
                  this.props.NavCamera(NAV_CAMERA, this.getPhotos);
                }}
              >
                <Text style={{ fontSize: styleVariables.fontSize, color: "#fff", fontFamily: 'CaveatBrush' }}>
                  Fotografuoti
                </Text>
              </TouchableHighlight>
              <TouchableHighlight
                primary
                style={style.roundButton}
                disabled={this.state.finish}
                onPress={this._pickImage}
              >
                <Text style={{ fontSize: styleVariables.fontSize, color: "#fff", fontFamily: 'CaveatBrush' }}>
                  Pasirinkti iš galerijos
                </Text>
              </TouchableHighlight>
            </View>
          </CardItem>
          <CardItem
            style={[
              style.gallery,
              {
                backgroundColor: this.props.task
                  ? "transparent"
                  : styleVariables.questionColor
              }
            ]}
          >
          {this.renderGallery()}
          </CardItem>

          {/* <CardItem footer style={[style.footer]}>
            <TouchableHighlight
              primary
              style={style.roundButton}
              disabled={this.state.finish}
              onPress={() => {
                setTimeout(() => {
                  if (this.state.photos) {
                    this.props.pushAnswer({
                      type: "photo",
                      data: { photos: this.state.photos, id: this.props.id }
                    });
                  } else {
                    this.props.pushAnswer({
                      type: "photo",
                      data: { photos: [], id: this.props.id }
                    });
                  }
                  this.props.next();
                }, 1500);
              }}
            >
              <Text style={{fontSize: styleVariables.fontSize}}> Sekantis </Text>
            </TouchableHighlight>
          </CardItem> */}
        </Card>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      updateTaskAnswerPhotos,
      NavCamera: (route, callback) =>

          NavigationActions.navigate({
            routeName: route,
            params: { onGoBack: (photos, videos) => callback(photos, videos) }
          })
    },
    dispatch
  );
}
function mapStateToProps(state) {
  return {
    classes: state.classes,
    photos: state.testResults.photos,
    navigation: state.nav
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(QuestionPhotoCard);
