import React, { Component } from "react";
import { connect } from "react-redux";
import { Text, StyleSheet, View, TouchableOpacity, Image } from "react-native";
import { Constants } from "expo";
import { styleVariables } from "../globals";
import { Icon } from "native-base";
import { NavigationActions, StackActions} from "react-navigation";

const style = StyleSheet.create({
  container: {
    paddingTop: Constants.statusBarHeight,
    height: 100,
    backgroundColor: "#0B3C5D",
    // backgroundColor: "#AFAEEE",
    flexDirection: "row",
    alignItems: "center",
    // justifyContent: "space-between",
    borderBottomWidth:10,
    // borderColor:'#8281e4',
    borderColor: "#328CC1",
    paddingHorizontal:20,
  },
  backBtn:{
     paddingHorizontal: 16, 
     paddingVertical: 10, 
    //  backgroundColor: '#8281e4', 
    backgroundColor: "#328CC1",
     borderRadius: 8,
     color:'#fff'
  },
  points: { 
    flexDirection: 'row',
    alignItems: 'center',
    // backgroundColor: '#8281e4',
    backgroundColor: '#328CC1',
    borderRadius:8,
    paddingHorizontal:8
  }
});

class GlobalHeader extends Component {
  render() {
    let { withBack, dispatch, user, children, noPoints, title, ...other} = this.props;
    return(
      <View style={style.container}>
        {/* Nav left */}
        <View>
          {withBack ? (
            <TouchableOpacity onPress= {() => {
                  if(this.props.clearTestResults) {
                    dispatch({type: "RESET_TEST_ANSWERS"});
                  }
                  dispatch(NavigationActions.back());
                }}>
              <Icon name="arrow-back" style={style.backBtn}></Icon>
              </TouchableOpacity>
          ) : null}
          {children}
        </View>
        {/* Nav center */}
        <View style={{ flex: 1, flexDirection:'row', alignItems:'center' ,justifyContent: 'center'}}>
          <Medal points={user ? user.points : 0}/>
          <Text style={{ marginLeft: 10, fontSize: styleVariables.fontSize, fontFamily: 'CaveatBrush', color:'#fff' }}>
             {user.name
              ? `${user.name} ${user.username}`
              : title ? title : "IŠMANUSIS TYRĖJAS"}
          </Text>
        </View>
        {/* Nav right */}
        {noPoints ? null : <View style={style.points}>
          <Text style={{ fontSize: styleVariables.fontSize, marginRight:16, color:'#fff' }}>
             {user.name ? user.points : 0}
          </Text>
          <Image source={require('../images/png/components/star.png')}/>
        </View> }
      </View>
    )
  //   if(this.props.user) {
  //   return (
  //     <View>
        
  //     </View>
  //     <View>
  //       <View style={style.container}>
  //         {this.props.withBack ? (
  //           <Button
  //             onPress={() => {
  //               const { dispatch } = this.props;
  //               dispatch(NavigationActions.back());
  //             }}
  //           >
  //             <Text>BACK</Text>
  //           </Button>
  //         ) : null}
  //         <Text style={{ marginLeft: 10, fontSize: styleVariables.fontSize }}>
  //           {this.props.user.name
  //             ? `${this.props.user.name} ${this.props.user.username}`
  //             : ""}
  //         </Text>
  //         <View style={{ flexDirection: "row", alignItems: "center" }}>
  //           <Text style={{ fontSize: styleVariables.fontSize }}>
  //             {this.props.user.name ? "150" : 0}
  //           </Text>
  //           <Star style={{ width: 50, height: 50, marginTop: 25 }} />
  //         </View>
  //       </View>
  //     </View>
  //   );
  // } else {
  //   <View>
  //   <View style={style.container}>
  //     {this.props.withBack ? (
  //       <Button
  //         onPress={() => {
  //           const { dispatch } = this.props;
  //           dispatch(NavigationActions.back());
  //         }}
  //       >
  //         <Text>BACK</Text>
  //       </Button>
  //     ) : null}
  //     <Text style={{ marginLeft: 10, fontSize: styleVariables.fontSize }}>
  //      Null Null Null
  //     </Text>
  //     <View style={{ flexDirection: "row", alignItems: "center" }}>
  //       <Text style={{ fontSize: styleVariables.fontSize }}>
  //         Null
  //       </Text>
  //       <Star style={{ width: 50, height: 50, marginTop: 25 }} />
  //     </View>
  //   </View>
  // </View>
  // }
  }
}
class Medal extends Component {
  constructor(props) {
    super(props);
    this.medals = [
      require('../images/png/components/medal_0.png'),
      require('../images/png/components/medal_1.png'),
      require('../images/png/components/medal_2.png'),
      require('../images/png/components/medal_3.png'),
    ];
    this.getMedal = this.getMedal.bind(this);
  }
  getMedal = () => {
    let currentMedal = null;
    let rewardPoints = this.props.rewardPoints || [50,150,300,500] ;
    if(this.props.points >= parseInt(rewardPoints[0]) && this.props.points !== 0 && this.props.points < parseInt(rewardPoints[1])) {

      currentMedal = this.medals[0];
    }
    else if (this.props.points >= parseInt(rewardPoints[1]) && this.props.points <= parseInt(rewardPoints[2])) {
    
      currentMedal = this.medals[1];
    }
    else if (this.props.points > parseInt(rewardPoints[2]) && this.props.points < parseInt(rewardPoints[3])) {
   
      currentMedal = this.medals[2];
    } 
    else if (this.props.points >= parseInt(rewardPoints[3])) {
    
      currentMedal =  this.medals[3];
    }
    return <Image source={currentMedal}/>
  }
  render(){
    return(
      <View>
        {this.getMedal()}
      </View>
    )
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
    rewardPoints: state.user.RewardPoints
  };
}
export default connect(
  mapStateToProps,
  null
)(GlobalHeader);
