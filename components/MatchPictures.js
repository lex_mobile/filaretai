import React from 'react';
import {
  View, Image, StyleSheet, TouchableHighlight, Text
} from 'react-native';
import DragableComponent from './DraggableComponent';
import { URL } from '../actions/index';
import ImageView from 'react-native-image-view';

const styles = StyleSheet.create({
  dragContainer: {
    borderWidth: 1,
    borderColor: 'green',
    flexGrow: 1,
    flexDirection: 'row',
    zIndex: 1,
    height: '100%',
  },
  photo: {
    flex: 2,
    flexDirection: 'row',
    zIndex: 1,
  },
  draggable: {
    borderWidth: 1,
    flex: 1,
    flexDirection: 'row',
    zIndex: 1,
  },
});

class MatchPictures extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dropZoneXY: [],
    };
    this.measureDropZones = this.measureDropZones.bind(this);
    this.viewRef = [];
    this.dragRef = [];
    this.answers = [];
    this.saveAnswer = this.saveAnswer.bind(this);
  }

  componentDidMount() {
    requestAnimationFrame(this.measureDropZones);
    const imageImgKeys = this.props.modules.map((item) => {
      return { uri: `${URL}${item.image}`, show: false };
    });
    let imageNames = this.props.modules.map((item) => {
      return {name: item.name, image: `${URL}${item.image}`};
    });
    imageNames = this.shuffle(imageNames);
    this.setState({imageImgKeys, imageNames})
  }

  measureDropZones() {
    let dropZone = [];
    for (let i = 0; i < this.viewRef.length; i++) {
      this.viewRef[i].ref.measure((fx, fy, width, height, px, py) => {
        dropZone.push({
          x: px,
          y: py,
          width,
          height,
          i: -1,
          id: this.viewRef[i].id,
        });
      });
    }
    this.setState({ dropZoneXY: dropZone }, () => {
      //console.log(this.state.dropZoneXY)
    });
  }

  saveAnswer(Ans) {

    if (this.answers.length === 0) {
      this.answers.push(Ans);
    }
    else {
      let push = true;
      this.answers.forEach((item, i) => {
        if (item.id === Ans.id && Ans.A == null) {
          this.answers = this.answers.filter((iitem, i) => {
            return iitem.id !== Ans.id;
          });
          push = false;
        } else if (item.id === Ans.id && Ans.A != null) {
          this.answers[i].A = Ans.A;
          this.props.answers(this.answers);
          push = false;
          return;
        }
      });
      if (push) {
        this.answers.push(Ans);
      }
    }
    this.props.answers(this.answers);
}

  shuffle(array) {
    let counter = array.length;

    // While there are elements in the array
    while (counter > 0) {
      // Pick a random index
      let index = Math.floor(Math.random() * counter);

      // Decrease counter by 1
      counter--;

      // And swap the last element with it
      let temp = array[counter];
      array[counter] = array[index];
      array[index] = temp;
    }

    return array;
  }

  componentWillReceiveProps(){
   
  }

  openClose(name) {
    this.setState((prevState) => {
      const newState = prevState.imageImgKeys.map((item) => {
        if (item.uri == name) {
          item.show = !item.show;
        }
        return (item);
      });
      return { imageImgKeys: newState };
    });
  }

  render() {
    const imageImgKeys = this.state.imageImgKeys;
    const imageNames = this.state.imageNames;
    if(imageImgKeys && imageNames){
    const render = imageImgKeys.map((imgKey, i) => {
        let imageModal = [
          {
            source: {
              uri: imgKey.uri,
            },
            title: 'text'
          },
        ];
        return (
          <View key={`${imgKey.uri}${i}`} style={{ flexShrink: 1, flexDirection: 'row', justifyContent: 'center', alignItems:'center', alignContent: 'center', height: '100%'}}>
            <DragableComponent
              saveAnswer={this.saveAnswer}
              circleColor="red"
              circleSize={36}
              circleText={imageNames[i].name}
              id={imageNames[i].image}
              key={i}
              dropZoneValue={this.state.dropZoneXY}
              style={[styles.draggable]}
            />
            <View
              id={imgKey.uri}
              ref={(ref) => { this.viewRef[i] = { ref, id: imgKey.uri }; }}
              style={styles.dragContainer} // onLayout={this.setDropZoneValues}
            />
            <ImageView
              images={(imageModal)}
              isVisible={imgKey.show}
              onClose={() => { this.openClose(imgKey.uri); }}
            />
            <TouchableHighlight onPress={() => { this.openClose(imgKey.uri); }} style={{ flex: 2 }}>
            <Image
              source={{ uri: imgKey.uri }}
              resizeMode="contain"
              resizeMethod="scale"
              style={[styles.photo]}
            />
            </TouchableHighlight>
          </View>
        );
      });
      return (
  
          render

       
        );
    } else {
      return (<Text> Loading </Text>)
    }
      
  }
}

export default MatchPictures;
