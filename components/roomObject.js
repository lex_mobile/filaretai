import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity} from "react-native";
const objects = { 
	//MAIN ROOM
	table: require(`../images/png/other/table.png`),
	books: require(`../images/png/other/books.png`),
	globe: require(`../images/png/other/globe.png`),
	bottles: require(`../images/png/other/bottles.png`),
	chair: require(`../images/png/other/chair.png`),
	blackboard: require(`../images/png/other/blackboard.png`),
	cactus: require(`../images/png/other/cactus.png`),
	cupFlower: require(`../images/png/other/cupFlower.png`),
	stickyNote: require("../images/png/other/stickyNote.png"),
	
	//NATURE ROOM
	tree: require(`../images/png/other/nature/tree.png`),
	tree_nr2: require(`../images/png/other/nature/tree.png`),
	flower: require(`../images/png/other/nature/flower.png`),
	bush: require(`../images/png/other/nature/bush.png`),
	stump: require(`../images/png/other/nature/stump.png`),
	weed: require(`../images/png/other/nature/weed.png`),
	nendre: [require(`../images/png/other/nature/nendre.png`), require(`../images/png/other/nature/nendreRupuze.png`)],
	shroom: [require(`../images/png/other/nature/shroom.png`), require(`../images/png/other/nature/shroomSolved.png`)],
	trash: require(`../images/png/other/nature/trash.png`),
	trashSolved: require(`../images/png/other/nature/trashSolved.png`),
	//LIBRARY ROOM
	// lamp: require(`../images/png/other/library/lamp.png`),
	flag: require(`../images/png/other/library/flag.png`),
	// shelfRight: require(`../images/png/other/library/shelf_right.png`),
	// shelfLeft: require(`../images/png/other/library/shelf_left.png`),
	sword: require(`../images/png/other/library/sword.png`),
	phone: require(`../images/png/other/library/phone.png`),
	shield: require(`../images/png/other/library/shield.png`),
	wheel: require(`../images/png/other/library/wheel.png`),
	herbas: require(`../images/png/other/library/herbas.png`),
	hook: require(`../images/png/other/library/hook.png`),
	//NATURE1
	kelmas: require(`../images/png/other/nature1/kelmas.png`),
	kelmasVoras: [require(`../images/png/other/nature1/kelmas.png`), require(`../images/png/other/nature1/kelmasVoras.png`)],
	liepa: require(`../images/png/other/nature1/liepa.png`),
	klevas: require(`../images/png/other/nature1/klevas.png`),
	sermuksnis: require(`../images/png/other/nature1/sermuksnis.png`),
	siuksle: require(`../images/png/other/nature1/siuksle.png`),
	siuksliadeze: [require(`../images/png/other/nature1/siuksle.png`), require(`../images/png/other/nature1/siuksliadeze.png`)],
	siuksliadeze_pirma: [require(`../images/png/other/nature1/siuksle.png`), require(`../images/png/other/nature1/siuksliadeze.png`)],
	egle: require(`../images/png/other/nature1/egle.png`),
	vynuoge: require(`../images/png/other/nature1/vynuoge.png`),
}

export default class RoomObject extends Component {

	renderImage = () => {
		if(Array.isArray(objects[this.props.object])) {
			
			if(this.props.finished.finishedPolls.some(e => e.PollCategoryId === this.props.finished.id)) {
				return (<Image source={objects[this.props.object][1]}/>);
			} else {
				return (<Image source={objects[this.props.object][0]}/>);
			}
		}
		return (<Image source={objects[this.props.object]}/>);
	}

	render() {
	return (
			// <Image source={require(imgSrc)} resizeMode="contain"></Image>
		// <Text>{`../assets/img/objects/${this.props.img}`}</Text>
		<View style={this.props.style}>
			{this.props.children}
			{typeof this.props.action === "undefined" ?
			<Image source={objects[this.props.object]}/> :
			<TouchableOpacity onPress={() => this.props.action()}>
				{this.renderImage()}
			</TouchableOpacity>}
		</View>
	)
  }
}


export class Blackboard extends Component {
	wordFormatter = (number) => {
		let last_digit = number % 10;
		if(last_digit === 0 || number % 100  >= 11 && number % 100 <=19) {
			return "naujų užduočių!"
		} else if(last_digit >= 2 && last_digit <= 9) {
			return "naujas užduotis!";
		} else if(last_digit === 1) {
			return "naują užduotį!";
		}
	}

	render(){
		return(
			<View style={this.props.style}>
				<View style={{position:'relative'}}>
					<TouchableOpacity onPress={() => this.props.action()}>
						<Image source={objects['blackboard']} />
						<View style={{position:'absolute', top:0,left:0,width:'100%', height:'100%', justifyContent:'center',alignItems:'center'}}>
							<Text style={{fontFamily:'CaveatBrush', fontSize:32, color:'#fff'}}>
								{this.props.count === 0 ? "Naujų užduočių neturi" : `Turi ${this.props.count} ${this.wordFormatter(this.props.count)}`} 
							</Text>
						</View>
					</TouchableOpacity>
				</View>
			</View>
		)
	}
}