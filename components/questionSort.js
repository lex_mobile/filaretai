import React, { Component } from 'react';
import { Image, StyleSheet, Platform, Animated, Dimensions, View, Easing, TouchableHighlight  } from 'react-native';
import {
 Card, CardItem, Text, Body, Button, Left, Right, Toast, Picker, Item, Content,
} from 'native-base';
import { URL } from '../actions/index';
const window = Dimensions.get('window');
import HTMLView from 'react-native-htmlview';
import SortableList from 'react-native-sortable-list';
import ImageView from 'react-native-image-view';
import { connect } from 'react-redux';
import { pushAnswer, getQuestionSortStateFromMap } from '../actions/index';
import { bindActionCreators } from 'redux';
import {styleVariables, htmlViewStyle, shuffle} from '../globals';
const style = StyleSheet.create({
  cardHeader: { alignItems: 'center', justifyContent: 'center', flex: 2, backgroundColor: styleVariables.questionColor },
  footerButton: { flex: 1, justifyContent: 'center', alignItems: 'center'},
  container: {
    flex: 1,
    height: '85%',
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundColor: styleVariables.questionColor,
    ...Platform.select({
      ios: {
        paddingTop: 20,
      },
    }),
  },

  title: {
    fontSize: 20,
    paddingVertical: 20,
    color: '#999999',
  },

  list: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },

  contentContainer: {
    width: window.width,

    ...Platform.select({
      ios: {
        paddingHorizontal: 30,
      },

      android: {
        paddingHorizontal: 0,
      },
    }),
  },

  row: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: styleVariables.questionColor,
    padding: 16,
    height: 80,
    flex: 1,
    marginTop: 7,
    marginBottom: 12,
    borderRadius: 4,

    ...Platform.select({
      ios: {
        width: window.width - 30 * 2,
        shadowColor: 'rgba(0,0,0,0.2)',
        shadowOpacity: 1,
        shadowOffset: { height: 2, width: 2 },
        shadowRadius: 2,
      },

      android: {
        width: window.width - 30 * 2,
        elevation: 0,
        marginHorizontal: 30,
      },
    }),
  },
  footer: {flex: 1, backgroundColor:'transparent'},
  image: {
    width: 80,
    height: 80,
    marginRight: 30,
    resizeMode: 'contain',
  },
  text: {
    fontSize: 24,
    color: '#222222',
  },
});
class QuestionSort extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Options: [],
      result: false,
      showToast: false,
      finish: false,
      order: {},
      listItems: []
    };

    this.order = [];
    this.savedOrder = {};
    this.checkAnswer = this.checkAnswer.bind(this);
  }

  componentDidMount() {
    let opts;
    let fromState = getQuestionSortStateFromMap(this.props.id);
    if(fromState) {
      opts = fromState.options;
      this.savedOrder = {order: fromState.order};
    }
    if(opts === null || opts === undefined) {
      opts = this.props.options.map((item) => {
        return ({
          id: item.id,
          name: item.name,
          image: item.image ? `${URL}${item.image}` : null,
          correct: item.correct,
          sort: item.order,
          answ: 0,
          text: item.name,
          key: item.id
        });
      });
    }
    
    this.setState({ Options: shuffle(opts)}, () => {
      let data = this.renderOptions();
      this.setState({listItems: data})
    });
  }

  //TVARKYT
  checkAnswer = (order) => {
    const correct = order.map((element, i) => {
      if(i === this.state.Options[parseInt(element)].sort){
        return true;
      } else {
        return false; 
      }
    });
    const result = correct.every(item => item);
    this.props.feedback({result, options: this.state.Options, id: this.props.id, time: Date.now(), order});

  }

  renderOptions() {
    const options = this.state.Options.map((item, i) => {
      const image = item.image ? `${URL}${item.image}` : null;
      return (
        {key: item.id, image, text: item.name }
      );
    });
    return options;
  }

  _renderRow = ({data, active}) => {
    return <Row data={data} active={active} />
  }

  renderHeader() {
    return (
      <CardItem header style={style.cardHeader}>
        <HTMLView
          stylesheet={htmlViewStyle}
          value={this.props.name}
        />
      </CardItem>
    );
  }

  renderFooter() {
    return (
      <CardItem style={style.footer}>
        <Button
          primary
          style={style.footerButton}
          disabled={this.state.finish}
          onPress={() => {
            this.checkAnswer();
            setTimeout(() => {
              this.props.next();
            }, 1500);
          }}
        >
          <Text> Sekantis </Text>
        </Button>
      </CardItem>
    );
  }

  render() {
    return (
      <Card style={[style.container]}>
        <SortableList
         {...this.savedOrder}
          style={ style.list }
          contentContainerStyle={ style.contentContainer }
          data={this.state.Options}
          onChangeOrder={(newOrder) => { this.order = newOrder;}}
          onReleaseRow={()=> {
            requestAnimationFrame(()=> {this.checkAnswer(this.order)});
          }}
          rowActivationTime={50}
          renderHeader={() => this.renderHeader()}
          showsHorizontalScrollIndicator={false}
          renderRow={ this._renderRow } />
      </Card>
    );
  }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ pushAnswer, getQuestionSortStateFromMap }, dispatch);
}
export default connect(null, mapDispatchToProps)(QuestionSort);

class Row extends Component {

  constructor(props) {
    super(props);
    this.open = false;
    this.openClose = this.openClose.bind(this);
    this.state = {
      open: false,
    };
    this._active = new Animated.Value(0);

    this._style = {
      ...Platform.select({
        ios: {
          transform: [{
            scale: this._active.interpolate({
              inputRange: [0, 1],
              outputRange: [1, 1.1],
            }),
          }],
          shadowRadius: this._active.interpolate({
            inputRange: [0, 1],
            outputRange: [2, 10],
          }),
        },

        android: {
          transform: [{
            scale: this._active.interpolate({
              inputRange: [0, 1],
              outputRange: [1, 1.07],
            }),
          }],
          elevation: this._active.interpolate({
            inputRange: [0, 1],
            outputRange: [2, 6],
          }),
        },
      })
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.active !== nextProps.active) {
      Animated.timing(this._active, {
        duration: 300,
        easing: Easing.bounce,
        toValue: Number(nextProps.active),
      }).start();
    }
  }
  
  openClose() {
    this.setState((prevState) => { return {open: !prevState.open}});
  }

  render() {
   const {data, active} = this.props;
   const images = [
    {
        source: {
            uri: data.image,
        },
        title: data.text,
    },
];
    return (
      <Animated.View style={[
        style.row,
        this._style,
      ]}>
      <TouchableHighlight onPressIn={() => this.openClose()}>
        <Image source={{uri: data.image}} style={style.image} />
      </TouchableHighlight>
        <ImageView
          images={images}
          imageIndex={0}
          isVisible={ this.state.open }
          onClose={() => { this.openClose(); }}
          renderFooter={(currentImage) => (<View><Text>{ data.text }</Text></View>)}
         />
        <Text style={style.text}>{data.text}</Text>
      </Animated.View>
    );
  }
}
