import React, { Component } from "react";
import { Dimensions } from "react-native";
import { PanGestureHandler, State } from "react-native-gesture-handler";
import Animated from "react-native-reanimated";
const { height} = Dimensions.get("screen");

const {
  set,
  cond,
  eq,
  add,
  lessThan,
  sub,
  Value,
  event,
  call,
  block,
  divide
} = Animated;


class Draggable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      onDropZone: false,
      On: null,
      prevOn: null,
      visible: true,
    };

    this._transX = 0;
    this._transY = 0;
    this.onDrop = this.onDrop.bind(this);

    const dragX = new Value(0);
    this.dragY = new Value(0);

    this.gestureState = new Value(-1);

    const dragVX = new Value(0);
    const dragVY = new Value(0);

    const absY = new Value(0);
    const absX = new Value(0);

    const relativeX = new Value(0);
    const relativeY = new Value(0);

    this._onGestureEvent = event([
      {
        nativeEvent: {
          translationX: dragX,
          translationY: this.dragY,
          velocityY: dragVY,
          velocityX: dragVX,
          absoluteY: absY,
          absoluteX: absX,
          x: relativeX,
          y: relativeY,
          state: this.gestureState
        }
      }
    ]);

    const transX = new Value(0);
    this.transY = new Value(0);

    const prevDragX = new Value(0);
    this.prevDragY = new Value(0);

    const addX = add(transX, sub(dragX, prevDragX));
    this._transX = cond(
      eq(this.gestureState, State.ACTIVE),
      [set(transX, addX), set(prevDragX, dragX), transX],
      [set(prevDragX, 0), transX]
    );
    const addY = add(this.transY, sub(this.dragY, this.prevDragY));
    this._transY = cond(
      eq(this.gestureState, State.ACTIVE),
      [
        cond(
          lessThan(sub(absY, divide(relativeY, 2)), divide(height, 4)),
          [block([set(this.dragY, 0), this.transY])],
          [block([set(this.transY, addY), set(this.prevDragY, this.dragY), this.transY])]
        )
      ],
      [
        cond(
          eq(this.gestureState, State.END),
          call(
            [sub(absX, divide(relativeX, 2)), sub(absY, divide(relativeY, 2))],
            this.onDrop
          )
        ),
        set(this.prevDragY, 0),
        this.transY
      ]
    );
  }
  onDrop = ([x, y]) => {
    let placedId = null;
    let removedId = null;
    for (let i = 0; i < this.props.dropzoneValues.length; i++) {
      let item = this.props.dropzoneValues[i].values;
      if (
        y < item.yPage + item.height &&
        y > item.yPage &&
        x < item.xPage + item.width &&
        x > item.xPage
      ) {
        placedId = i;
        let val = this.props.dropzoneStates.get(i);
        if (val) {
          if (!val.some(item => item.index === this.props.arrayIndex)) {
            this.props.dropzoneStates.set(
              i,
              val.concat({
                index: this.props.arrayIndex,
                title: this.props.title,
                pollOptionId: this.props.identifier,
                state: true
              })
            );
          }
        } else {
          this.props.dropzoneStates.set(i, [
            {
              index: this.props.arrayIndex,
              title: this.props.title,
              pollOptionId: this.props.identifier,
              state: true
            }
          ]);
        }
        this.setState(
          prev => {
            return {
              On: i,
              prevOn: {
                index: prev.On,
                yPage: prev.On
                  ? this.props.dropzoneValues[prev.On].values.yPage
                  : null,
                height: prev.On
                  ? this.props.dropzoneValues[prev.On].values.height
                  : null
              }
            };
          },
          () => {}
        );
      }
      if (x < item.xPage) {
        let val = this.props.dropzoneStates.get(this.state.On);
        if (val) {
          for (let j = 0; j < val.length; j++) {
            if (val[j].index === this.props.arrayIndex) {
              val.splice(j, 1);
              this.props.dropzoneStates.set(this.state.On, val);
              removedId = this.state.On;
            }
          }
        }
      }
      if (
        this.state.prevOn &&
        this.state.prevOn.index !== this.state.On &&
        (y > this.state.prevOn.yPage + this.state.prevOn.height ||
          y < this.state.prevOn.yPage)
      ) {
        let val = this.props.dropzoneStates.get(this.state.prevOn.index);
        if (val) {
          for (let j = 0; j < val.length; j++) {
            if (val[j].index === this.props.arrayIndex) {
              val.splice(j, 1);
              this.props.dropzoneStates.set(this.state.prevOn.index, val);
              removedId = this.state.prevOn.index;
            }
          }
        }
      }
    }

    if (placedId !== null) {
      this.props.dropzoneValues[placedId].cref.placeBox(
        this.props.dropzoneStates.get(placedId)
      );
    }
    if (removedId !== null) {
      this.props.dropzoneValues[removedId].cref.removeBox(
        this.props.dropzoneStates.get(removedId)
      );
    }
    this.props.checkAnswers();
  }

  render() {
    const { children, ...rest } = this.props;
    return (
      <PanGestureHandler
        {...rest}
        maxPointers={1}
        onGestureEvent={this._onGestureEvent}
        onHandlerStateChange={this._onGestureEvent}
      >
  
      {this.state.visible && (<Animated.View
          key={"animated"}
          style={[{
            zIndex: 999,
            margin: this.props.marginOffset,
          },
          {
            transform: [{ translateX: this._transX || 0} , {translateY: this._transY || 0}]
          }
        ]}
        >
          {children}
        </Animated.View>) }
      </PanGestureHandler>
    );
  }
}

export default class DraggableContainer extends Component {
  render() {
    return (
      <Draggable
        checkAnswers={this.props.checkAnswers}
        dropzoneValues={this.props.getData().values}
        dropzoneStates={this.props.getData().states}
        title={this.props.title}
        arrayIndex={this.props.arrayIndex}
        identifier={this.props.identifier}
        marginOffset={this.props.marginOffset}
      >
       {this.props.children}
      </Draggable>
    );
  }
}
