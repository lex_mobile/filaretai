import React, { Component } from "react";
import {
    Text,
    View,
    StyleSheet,
    Image,
    PixelRatio,
    Dimensions,
    TouchableHighlight,
    TouchableOpacity,
    ScrollView
} from "react-native";
import { GestureHandler } from "expo";
const { PanGestureHandler, State } = GestureHandler;
import HTMLView from "react-native-htmlview";
import DraggableContainer from "./draggableContainer";
import Dropzone from "./dropzone";
import { htmlViewStyle } from "../globals";
import ImageView from "react-native-image-view";
import { moderateScale } from "./ScaleElements";
const { height } = Dimensions.get("screen");

export default class questionDnd extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalStatus: []
        };
        this.dropzoneValues = [];
        this.dropZoneStates = new Map();
        this.measureCallback = this.measureCallback.bind(this);
        this.getDropZoneValues = this.getDropZoneValues.bind(this);
        this.updateDropzoneChild = this.updateDropzoneChild.bind(this);
        this.checkAnswers = this.checkAnswers.bind(this);
        this.openClose = this.openClose.bind(this);
    }
    checkAnswers() {
        let fields = [];
        console.log(this.props.opts, "[SingleDragAndDrop] Opts");
        console.log(this.dropZoneStates, "[SingleDragAndDrop] dropZoneStates");
        let requiredMatches = this.props.opts.length;
        if (this.dropZoneStates) {
            for (const [key, value] of this.dropZoneStates) {
                for (let v = 0; v < value.length; v++) {
                    let ifCorrect = false;
                    for (let i = 0; i < this.props.opts.length; i++) {
                        if (value[v].pollOptionId === this.props.opts[i].id && key === i) {
                            ifCorrect = true;
                            console.log(value[v].title, "MATCHED");
                            break;
                        }
                    }
                    if (ifCorrect) {
                        fields.push(ifCorrect);
                    }
                }
            }
            console.log(fields);
        }
        let result = fields.length > 0 && fields.length === requiredMatches ? fields.every(success => { return success === true }) : false;
        this.props.feedback({ result, options: fields, id: this.props.id, time: Date.now() });
        return result;
    }
    openClose(j) {
        this.setState(prevState => {
            const newState = prevState.modalStatus.map((item, i) => {
                if (i === j) {
                    item = !item;
                }
                return item;
            });
            return { modalStatus: newState };
        });
    }
    componentDidMount() {
        this.setState({
            modalStatus: new Array(this.props.opts.length).fill(false)
        });
    }
    renderOptions() {
        let options = [];
        let squareDimensions =
            (height - height / 3) /
            this.props.opts.length /
            this.props.boxMarginFactor;
        this.props.opts.map((item, i) => {
            console.log(item, "dnd opts")
            let imageModal = [
                {
                    source: {
                        uri: `http://filaretai.lexitacrm.lt${item.PhotoUrl}`
                    }
                }
            ];
            options.push(
                <View style={[styles.optionRow, { flex: 1 }]} key={item.id}>
                    <View style={{ flex: 1.5 }}>
                        <DraggableContainer
                            title={item.Name}
                            arrayIndex={i}
                            getData={this.getDropZoneValues}
                            arrayIndex={item.PollQuestionAnswerDetailId}
                            identifier={item.id}
                            marginOffset={this.props.boxSize / 2}
                            checkAnswers={this.checkAnswers}
                        >
                            <View
                                style={[styles.dragContainerBlock, { 
                                    minHeight: this.props.boxSize / 2,
                                    minWidth: this.props.boxSize,
                                    maxWidth: this.props.boxSize * 2,}]}
                            >
                                <Text style={styles.dragContainerText}>{item.Name}</Text>
                               
                            </View>
                        </DraggableContainer>
                    </View>

                    <View style={styles.dropzoneContainer}>
                        <Dropzone
                            style={styles.dropzone}
                            title={item.Name}
                            arrayIndex={i}
                            measureCallback={this.measureCallback}
                        />
                    </View>
                    {/* <Image
            source={{ uri: `http://filaretai.lexitacrm.lt${item.PhotoUrl}` }}
            style={{ width: this.props.boxSize*2, height: this.props.boxSize*2, resizeMode: "contain" }}
          /> */}
                    <ImageView
                        images={imageModal}
                        isVisible={
                            typeof this.state.modalStatus[i] !== "undefined" &&
                                this.state.modalStatus[i] !== null
                                ? this.state.modalStatus[i]
                                : false
                        }
                        onClose={() => {
                            this.openClose(i);
                        }}
                    />
                    {/* flex block */}
                    <View style={{ flex: 1 }}>
                        <TouchableOpacity
                            onPress={() => {
                                this.openClose(i);
                            }}
                        >
                            <Image
                                source={{ uri: `http://filaretai.lexitacrm.lt${item.PhotoUrl}` }}
                                resizeMode="contain"
                                resizeMethod="scale"
                                style={{
                                    width: this.props.boxSize * 2,
                                    height: this.props.boxSize * 2,
                                    padding: 10,
                                    resizeMode: "contain",
                                  
                                }}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
            );
        });
        return options;
    }
    updateDropzoneChild(id) {
        // console.log(this.dropzoneValues[id].ref)
    }
    getDropZoneValues() {
        return { values: this.dropzoneValues, states: this.dropZoneStates };
    }
    measureCallback(values, ref, cref) {
        this.dropzoneValues.push({ values, ref, cref });
        // if (this.dropzoneValues.length === this.props.opts.length) {
        //   console.log(this.dropzoneValues);
        // }
    }
    render() {
        return (
            <View style={styles.container}>
                <ScrollView style={{ flexWrap: "wrap", maxHeight: height / 3 }}>
                    <View style={{ paddingHorizontal: 15, paddingVertical: 10 }}>
                        <HTMLView stylesheet={htmlViewStyle} value={this.props.name} />
                    </View>
                </ScrollView>
                <View style={styles.scrollContainer}>{this.renderOptions()}</View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    scrollContainer: {
        flex: 10,
        justifyContent: "flex-start",
        flexDirection: "column"
    },
    optionRow: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-evenly",
        alignItems: "center",

    },
    dropzone: {
        borderRadius: 20,
        borderColor: "black",
        borderWidth: 2,
        flex: 1,
        height: "100%",
        width: "100%"
    },
    container: { flex: 1 },
    questionText: { 
        fontSize: PixelRatio.get() * 16
     },
     dropzoneContainer: {
        width: "40%", 
        alignItems: "center", 
        justifyContent: "center", 
        flex: 2,
     },
     dragContainerText: {
        color: "black", 
        textAlign: "center", 
        padding: 5, 
        fontFamily: "CaveatBrush"
     },
     dragContainerBlock: {
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#00C853",
        borderWidth: 1,
        borderColor: "black",
        borderRadius: 8,
        opacity: 0.65,
        height: undefined,
        width: undefined,
     }
});
