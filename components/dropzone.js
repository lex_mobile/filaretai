import React, { Component } from "react";
import { Text, View, StyleSheet } from "react-native";

export default class dropzone extends Component {
  constructor(props) {
      super(props);
      this.Ref = React.createRef();
      this.classRef = this;
      this.state = {
        color: false,
        text: ''
      }
      this.changeColor = this.changeColor.bind(this);
      this.placeBox = this.placeBox.bind(this);
      this.removeBox = this.removeBox.bind(this);
      this.timer = null;
  }
  componentDidMount () {
   
  }
  componentWillUnmount() {
    if(this.timer) {
      clearTimeout(this.timer);
    }
  }
  measure = () => {
    try {
      this.timer = setTimeout(()=> {
        this.Ref.current.measure((fx,fy,w,h,px,py) => {
          const object = {
              height: h,
              width: w,
              xFrame: fx,
              yFrame: fy,
              xPage: px,
              yPage: py,
              index: this.props.arrayIndex,
              name: this.props.title
          }
          this.props.measureCallback(object, this.Ref, this.classRef);
      });
      }, 500);
  } catch (error) {
    console.log(error);
  }
  }
  changeColor () {
    this.setState((prev)=> {
      let prevState = prev;
      prevState.color = !prevState.color;
      return prevState;
    });
  }
  placeBox(elems){
    let string = '';
    if(elems) {
      for(let i = 0; i< elems.length; i++) {
        string += ' ' + elems[i].title + '\n';
      }
    }
    this.setState({color: true, text: string});
  }
  removeBox(elems){
    let string = '';
    if(elems) {
      for(let i = 0; i< elems.length; i++) {
        string += ' ' + elems[i].title + '\n';
      }
      this.setState({text: string, color: string ? true : false});
    }  
  }
  render() {
    return (
      <View style={[this.props.style, style.mainContainer, {backgroundColor: this.state.color ? '#f5a623' : '#b3c2bf'}]} ref={this.Ref} onLayout={this.measure}>
        {/* <Text style={style.mainContainerText}>{this.state.text}</Text> */}
      </View>
    );
  }
}
const style = StyleSheet.create({
  mainContainer: {
    borderRadius: 20, 
    margin: 0.5
  },
  mainContainerText:{
    textAlign:'center', 
    fontSize: 18, 
    padding: 5, 
    borderRadius: 10, 
    fontFamily: "CaveatBrush"
  }
})