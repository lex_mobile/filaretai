import React, { Component } from "react";
import {
  Image,
  TouchableHighlight,
  StyleSheet,
  Dimensions,
  PixelRatio,
  TouchableOpacity,
  WebView
} from "react-native";
import HTMLView from "react-native-htmlview";
import {
  Card,
  CardItem,
  Text,
  // CheckBox,
  Left,
  Content,
  View,
  Icon
} from "native-base";
import ImageView from "react-native-image-view";
import { URL } from "../actions/index";
import { connect } from "react-redux";
import { pushAnswer, getQuestionTestStateFromMap } from "../actions/index";
import { bindActionCreators } from "redux";
import { styleVariables, htmlViewStyle } from "../globals";

class QuestionTest extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Options: [],
      NChecked: 0,
      NCorrect: 0,
      show: false,
      finish: false,
      showIcon: true
    };
    this.openClose = this.openClose.bind(this);
    this.checkBoxClick = this.checkBoxClick.bind(this);
    this.open = false;
  }

  componentDidMount() {
    let i = 0;
    this.props.options.forEach(element => {
      if (element.correct) {
        i++;
      }
    });
    let opts;
    opts = getQuestionTestStateFromMap(this.props.id);
    if(opts === null || opts === undefined) {
      opts = this.props.options.map((item, i) => {
        return {
          name: item.name || " ",
          image: item.image,
          correct: item.correct,
          clicked: false,
          show: false
        };
      });
    }

    this.setState({ Options: opts });
    this.setState({ NCorrect: i }, () => {});
  }

  openClose(name) {
    this.setState(prevState => {
      const newState = prevState.Options.map(item => {
        if (item.name == name) {
          item.show = !item.show;
        }
        return item;
      });
      return { Options: newState };
    });
  }

  checkBoxClick(name) {

    let ci;
    let t = 0;
    this.setState(
      prevState => {
        let newState = prevState.Options;
        //if only one option is correct then checkboxes work like radio buttons
        if (this.state.NCorrect == 1) {
          for (var i = 0; i < newState.length; ++i) {
            if (i != name) newState[i].clicked = false;
          }
        }
        newState[name].clicked = !newState[name].clicked;
        return { Options: newState };
      },
      () => {
        this.checkAnswer();
      }
    );
  }
  //TIKRINT (ATRODO VEIKIA, BET DOUBLE CHECK)
  checkAnswer() {
    this.setState({ finish: true });
    let result = false;
    for (item of this.state.Options) {
      if (item.correct == item.clicked) {
        result = true;
      } else {
        result = false;
        break;
      }
    }
    const filtered = this.state.Options.map(item => {
      return { correct: item.correct, clicked: item.clicked, name: item.name };
    });
    this.props.feedback({result, options: filtered, id: this.props.id, time: Date.now()});
  }

  showQuestionImage() {
    this.setState(prevState => {
      return { show: !prevState.show };
    });
  }
// 
  answerClicked(item) {
    this.setState({showIcon: !this.state.showIcon})
    // this.checkBoxClick(item);
    
  }

  renderOptions() {
    const options = this.state.Options.map((item, i) => {
      let image;
      let imageModal = [];
      if (item.image) {
        imageModal = [
          {
            source: {
              uri: `${URL}${item.image}`
            },
            title: item.name
          }
        ];
        image = (
          <Left style={{ flex: 1 }}>
            <ImageView
              images={imageModal}
              imageIndex={0}
              isVisible={item.show}
              onClose={() => {
                this.openClose(item.name);
              }}
            />
            <TouchableHighlight
              onPress={() => {
                this.openClose(item.name);
              }}
              style={{ flex: 0.5, }}
            >
              <Image
                source={{ uri: `${URL}${item.image}` }}
                style={{ height: 120, flex: 1, opacity: 1}}
              />
            </TouchableHighlight>
          </Left>
        );
      }
      return (
        <View key={`${item.name}${i}`}>
          <TouchableOpacity onPress={()=> {this.checkBoxClick(i);} }>
            <CardItem style={[style.option, {borderColor: "#BDBDBD", opacity: 1}]}>
            {image}
            <Text style={style.optionText}>{item.name}</Text>
            <View style={style.renderOptionsView}>
                <Icon style={[style.renderOptionsIcon, {display: item.clicked === true ? "flex" : "none" }]} name="check" type="Feather"> </Icon>
            </View>
            </CardItem>
          </TouchableOpacity>
        </View>
      );
    });

    return options;
  }

  renderNode(node) {
    if (node.name == "img") {
      let a = node.attribs;
      return (
        <Image
          key={a.src}
          source={{ uri: a.src }}
          style={style.renderNodeImg}
        />
      );
    }
  }

  renderQuestionImage() {
    if (this.props.image) {
      return (
        <View style={{ flex: 1 }}>
          <ImageView
            images={[
              {
                source: {
                  uri: `${URL}${this.props.image}`
                }
              }
            ]}
            imageIndex={0}
            isVisible={this.state.show}
            onClose={() => {
              this.showQuestionImage();
            }}
          />
          <TouchableHighlight
            onPress={() => {
              this.showQuestionImage();
            }}
            style={{ flex: 1 }}
          >
            <Image
              source={{ uri: `${URL}${this.props.image}` }}
              resizeMode="contain"
              style={style.questionImage}
            />
          </TouchableHighlight>
        </View>
      );
    }
  }
  // renderNode(node, index, siblings, parent, defaultRenderer) {
  //   if (node.name == 'a') {
  //     const a = node.attribs;
  //     const iframeHtml = `<a src="${a.src}">asdasd</a>`;
  //     return (
  //       <View key={index} style={{width: 50, height: 20, backgroundColor: "blue"}}>
  //         <WebView source={{html: iframeHtml}} />
  //       </View>
  //     );
  //   }
  // }
  // renderNode={this.renderNode}
  render() {
    return (
      <Content style={{paddingHorizontal: 10}}>
        <Card transparent key={this.props.name} style={[style.question]}>
          <CardItem header style={[style.header]}>
            <HTMLView value={this.props.name} stylesheet={htmlViewStyle}  />
            {this.renderQuestionImage()}
          </CardItem>

          <CardItem style={[style.options]}>{this.renderOptions()}</CardItem>

          <CardItem footer style={[style.footer]}>
            {/* <Button
              primary
              style={{ flex: 1, justifyContent: 'center', alignItems: 'center'  }}
              disabled = {this.state.finish}
              onPress={() => {
                this.checkAnswer();
                setTimeout(() => {
                  this.props.next();
                }, 1500);
              }}
            >
              <Text> Sekantis </Text>
            </Button> */}
          </CardItem>
        </Card>
      </Content>
    );
  }
}

const style = StyleSheet.create({
  ContentContainerQuestion: {
    flex: 1,
    flexDirection: "column"
  },
  question: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
    height: "auto",
    // backgroundColor: "transparent",
    backgroundColor: "#fff",
    opacity: 0.75
  },
  header: {
    flex: 2,
    flexDirection: "column",
    width: Dimensions.get("screen").width,
    justifyContent: "center",
    backgroundColor: styleVariables.questionColor
  },
  options: {
    flex: 4,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "stretch",
    paddingHorizontal: 20,
    backgroundColor: "transparent"
  },
  option: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    // paddingHorizontal: 10,
    borderWidth: 1,
    borderRadius: 10,
    marginBottom: 15,
    borderColor: "#fff",
    backgroundColor: styleVariables.questionColor,
    
  },
  footer: {
    flex: 1,
    backgroundColor: "transparent"
  },
  optionText: {
    textAlign: "left",
    flex: 0.8,
    flexWrap: "wrap",
    fontSize: PixelRatio.get() * 16,
    fontFamily: "CaveatBrush",
  },
  questionImage: {
    width: Dimensions.get("screen").width - 20,
    height: Dimensions.get("screen").height / 4,
    flex: 1
  },
  //
  renderNodeImg: {
    flex: 1,
    width: Dimensions.get("screen").width,
    height: Dimensions.get("screen").height / 3,
    paddingHorizontal: 30
  },
  renderOptionsView: {
    backgroundColor: styleVariables.checkBoxBgColor,
    height: 50, 
    width: 50, 
    alignItems: "center", 
    justifyContent: "center",
    borderColor: styleVariables.checkBoxBgColor,
    borderRadius: 5,
  },
  renderOptionsIcon: {
    color: "#fff",
    fontSize: 45,
    height: 45,
    width: 45,
  }
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ pushAnswer, getQuestionTestStateFromMap}, dispatch);
}
export default connect(
  null,
  mapDispatchToProps
)(QuestionTest);
