import React, { Component } from "react";
import { Dimensions } from "react-native";
import {
  Card,
  CardItem,
  Text,
  Body,
  Button,
  Left,
  Right,
  Toast,
  Content
} from "native-base";
import MatchPictures from "./MatchPictures";
import HTMLView from "react-native-htmlview";
import { connect } from "react-redux";
import { pushAnswer } from "../actions/index";
import { bindActionCreators } from "redux";
import { styleVariables, htmlViewStyle } from "../globals";
const { width, height } = Dimensions.get("window");
class QuestionMatch extends Component {
  constructor(props) {
    super(props);
    this.answers = [];
    this.saveAnswers = this.saveAnswers.bind(this);
    this.state = {
      finish: false
    };
  }

  saveAnswers(A) {
    this.answers = A;
    this.checkAnswer();
  }

  checkAnswer() {
    this.setState({ finish: true });
    const receivedAnswers = this.answers.every(item => item.A === true);
    let result;

    if (receivedAnswers && this.answers.length == this.props.options.length) {
      result = true;
    } else {
      result = false;
    }
    this.props.feedback({ result, id: this.props.id, time: Date.now()});

    // if (result) {
    //   Toast.show({
    //     text: "Teisingai!",
    //     buttonText: "Ok",
    //     type: "success",
    //     duration: 1400
    //   });
    // } else {
    //   Toast.show({
    //     text: "Neteisingai",
    //     buttonText: "Ok",
    //     type: "Warning",
    //     duration: 1400
    //   });
    // }
  }

  render() {
    return (
     
      <Card
        style={{
          flexDirection: "column",
          flex: 1,
          alignItems: "center",
          justifyContent: "space-around",
          backgroundColor: styleVariables.questionColor
        }}
      >
       <Content>
        <CardItem
          header
          style={{
            flexDirection: "column",
            width: Dimensions.get("screen").width,
            justifyContent: "center",
            marginTop: 15,
            flex: 1,
            height: '100%'
          }}
        >
          <HTMLView stylesheet={htmlViewStyle} value={this.props.name} />
        </CardItem>
        </Content>
        <CardItem
          style={{
            flex: 2,
            flexDirection: "column",
            justifyContent: "space-between",
            backgroundColor: 'transparent'
          }}
        >
  
          <MatchPictures
            answers={this.saveAnswers}
            modules={this.props.options}
          />
       
        </CardItem>
        {/* <CardItem style={{flex: 1}}>
          <Button
            primary
            style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
            disabled={this.state.finish}
            onPress={() => {
              this.checkAnswer();
              setTimeout(() => {
                this.props.next();
              }, 1500);
            }}
          >
            <Text> Sekantis </Text>
          </Button>
        </CardItem> */}
      </Card>
      
    );
  }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ pushAnswer }, dispatch);
}
export default connect(
  null,
  mapDispatchToProps
)(QuestionMatch);
