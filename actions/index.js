import axios from "axios";
import { store } from "../App";
import { resolve } from "url";
var querystring = require("querystring");
export const ROOT_URL = "https://filaretai.lexitacrm.lt/php/json_q.php";
export const URL = "https://filaretai.lexitacrm.lt";
export const FETCH_CATEGORIES = "FETCH_CATEGORIES";
export const SELECT_QUESTION = "SELECT_QUESTION";
export const ALL_DATA = "ALL_DATA";
export const FETCH_QUESTION = "FETCH_QUESTION";
export const GET_ROOMOBJECTS = "GET_ROOMOBJECTS";
export const FAIL_ROOMOBJECTS = "FAIL_ROOMOBJECTS";

export function fetchCategory(category) {
  const allData = store.getState().categories;
  const filtered = allData.filter(item => item.id == category.item.id);
  return {
    type: FETCH_CATEGORIES,
    payload: filtered
  };
}
export function getAllData() {
  const request = axios.get(ROOT_URL);
  return {
    type: ALL_DATA,
    payload: request
  };
}
function getQuestionsAndNav(category) {
  return {
    type: SELECT_QUESTION,
    payload: {
      questions: category.Questions,
      id: category.ParentId,
      nodeid: category.id
    }
  };
}
export function getQuestions(category) {
  return dispatch => {
    dispatch(getQuestionsAndNav(category));
    sendPollStartAction(category.ParentId);
    return { type: "NAV_POLL" };
  };
}
export function pushAnswer(answer) {
  if (answer.type === "photo") {
    return {
      type: "PUSH_PHOTO_ANSWER",
      payload: answer.data
    };
  } else {
    if (answer.result) {
      return {
        type: "PUSH_GOOD_ANSWER",
        payload: answer
      };
    } else {
      return {
        type: "PUSH_BAD_ANSWER",
        payload: answer
      };
    }
  }
}
export function logout() {
  return dispatch => {
    dispatch({type: "DELETE_USER"});
    dispatch({ type: "NAV_SPLASH" });
  }
}
export function resetAnswers() {
  return dispatch => {
    dispatch({ type: "RESET_TEST_ANSWERS" });
  };
}
export function getRoomObjects () {
  return function action(dispatch) {
    const url = `${URL}/php/json_q.php`;
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      }
    };
    const user = store.getState().user;
    const formData = new FormData();
    formData.append("StudentId", user.pk.toString());
    formData.append("type", "get_room_objects");
    const request = axios.post(url, formData, config);
    return request.then(data => {
      if (data.data.success) {
        dispatch({ type: GET_ROOMOBJECTS, payload: data.data });
      } else {
        dispatch({type: FAIL_ROOMOBJECTS});
      }
    }).catch((e)=>{dispatch(errorScreen(e))});
  }
}
export function submitTask(task_id, text, callback) {
  return function action(dispatch) {
    const url = `${URL}/php/json_q.php`;
    const user = store.getState().user;
    const taskPhotos = store.getState().currentTaskAnswer;
    let photoCounter = 0, videoCounter = 0;
    const formData = new FormData();
    if (taskPhotos.Photos.length > 0) {
     taskPhotos.Photos.forEach((element, i) => {
        if(element.type === "photo" || element.type === "image") {
          const file = new Blob([element.source], { type: 'image/jpeg' });
          formData.append(`photo[${photoCounter}]`, {uri: element.source.uri, type: 'image/jpg', name: `${i}.jpg`});
          photoCounter++;
        } else if(element.type === "video") {
          const file = new Blob([element.source], { type: 'video/mp4' });
          formData.append(`video[${videoCounter}]`, {uri: element.source.uri, type: 'video/mp4', name: `${i}.mp4`});
          videoCounter++;
        }
      });
    }
    formData.append("TasksAssignedToId", task_id.toString());
    formData.append("StudentId", user.pk.toString());
    formData.append("type", 'update_assigned_task');
    formData.append("photoCount", photoCounter);
    formData.append("videoCount", videoCounter);
    formData.append("Answer", text);    
    const config = {
      headers: {
        "Content-Type": "multipart/form-data; charset=utf-8;",
        Accept: "application/json"
      }
    };
    const request = axios.post(url, formData, config);
    return request.then(data => {
      if (data.data.success) {
        //dispatch({ type: "NAV_MENU" });
        if(typeof callback !== "undefined") {
          callback();
        }
        dispatch({type: 'ANSWER_TASK_RESET'});
      } else {
      }
    }).catch((e)=>{dispatch(errorScreen(e))});
  }
}
export function getTasks(callback) {
  return function action(dispatch) {
    const url = `${URL}/php/json_q.php`;
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      }
    };
    const user = store.getState().user.pk;
    const formData = new FormData();
    formData.append('type', 'student_tasks');
    formData.append("StudentId", user);
    const request = axios.post(url, formData, config);
    return request.then(data => {
      if (data.data.success) {
        if(typeof callback !== "undefined") {
          callback();
        } 
        console.log(data, "getTasks");
        dispatch(getTasksSuccess(data.data.data, data.data.TotalStudentPoints));
      } else {
        dispatch(getTasksFail(data.data.data));
      }
    });
  };
}
export function updateTaskAnswerPhotos(photos) {
  return {
    type : 'ANSWER_CURRENT_TASK',
    payload: photos
  }
}
function getTasksSuccess(data, points) {
  return {
    type: 'GET_TASK_SUCCESS',
    payload: data,
    points
  }
}
function getTasksFail(data) {
  return {
    type: 'GET_TASK_FAIL',
    payload: data,
  }
}
export function sendPollStartAction(id) {
  const user = store.getState().user;
  const config = {
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    }
  };
  const url = `${URL}/php/poll.php`;
  const formData = new FormData();
  formData.append("action", "start");
  formData.append("id", id.toString());
  formData.append("user", user.pk.toString());
  axios
    .post(url, formData, config)
    .then(resp => {
     // console.log(resp);
    })
    .catch(err => {
    //  console.log(err);
    });
}
export function resetCurrentTask() {
  return {
    type: 'RESET_ERROR'
  }
}
export function submitResults(id, allCount) {
  return dispatch => {
  const testResults = store.getState().testResults;
  const user = store.getState().user;
  const formData = new FormData();
  let correctCount = 0;
  if(testResults.objMap) {
    for (let [k, v] of testResults.objMap) {
      if(v.result) {
        correctCount++;
      }
    }
    formData.append("pollCategoryId", id.toString());
    formData.append("userId", user.pk.toString());
    formData.append("correctCount", correctCount);
    formData.append("allCount", allCount);
    formData.append("type", "update_fixed_points_count");
    formData.append("classGroupId", user.ClassGroupId);
    formData.append("questions", JSON.stringify([...testResults.objMap]), {
      type: "application/json"
    });
    const config = {
      headers: {
        "Content-Type": "multipart/form-data; charset=utf-8;",
        Accept: "application/json"
      }
    };
  
    axios
      .post(URL+"/php/poll.php", formData, config)
      .then(resp => {
        console.log(resp, "SUBMIT RESULTS");
      })
      .catch(err => {
       // console.log(err);
      });
  
      dispatch({type: "SEND_ANSWERS",
        payload: correctCount === allCount ? id : -1
      });
      if(correctCount === allCount) {
        dispatch({type: "USER_UPDATE_POINTS", payload: {points: user.points+allCount*10, id}});
      }
    }
  }
}
function registerUser(user) {
  return function action(dispatch) {
    dispatch(registerAttempt());
    const formData = new FormData();
    formData.append("Name", user.name);
    formData.append("Surname", user.username);
    formData.append("Password", user.password);
    formData.append("ClassGroupId", user.classId);
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      }
    };
    const url = `${URL}/php/register.php`;
    const request = axios.post(url, formData, config);
    return request.then(data => {
      dispatch ({type: 'RESET_ATTEMPT' });
      let withId = Object.assign({
        pk: data.data.StudentId,
        name: user.name, username: user.surname,
        ClassGroupId: data.data.ClassGroupId,
        points: data.data.TotalPoints,
        complete_successfully: data.data.complete_successfully,
        class: data.data.Class,
        rooms: data.data.rooms,
        RewardPoints: data.data.RewardPoints
      }, user);

      // let withId = {
      //   pk: data.data.StudentId,
      //   name: user.name, username: user.surname,
      //   ClassGroupId: data.data.ClassGroupId,
      //   points: data.data.FixedTasksPoints,
      //   complete_successfully: data.data.complete_successfully,
      //   class: data.data.Class
      // };

      if (data.data.success) {
        dispatch(registerSuccess(withId));
        console.log(data);
        dispatch({ type: "NAV_MENU" });
      } else {
        dispatch(authFailed(data.data.msg));
      }
    }).catch((e)=> {
      dispatch ({type: 'RESET_ATTEMPT' });
      dispatch(authFailed(e.toString()));
    });
  };
}
function registerAttempt() {
  return {
    type: 'REGISTER_ATTEMPT'
  };
}
function registerSuccess(withId) {
  return {
    type: "REGISTER",
    payload: withId
  };
}
export function getCurrentTask(id) {
  return function action(dispatch) 
 {
  dispatch(currentTaskAttempt());
  const formData = new FormData();
  formData.append('type', 'assigned_to');
  formData.append('TasksAssignedToId', id);
  const config = {
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    }
  };
  const url = `${URL}/php/json_q.php`;
  const request = axios.post(url, formData, config);
  return request.then(data => {
    dispatch ({type: 'RESET_ATTEMPT' });
    if (data.data.success) {
      console.log(data.data, "GET CURRENT TASK");
      dispatch(getCurrentTaskSuccess(data.data));
    } else {
      dispatch(getCurrentTaskFail(data.data.msg));
    }
  }).catch((e)=> {
    dispatch ({type: 'RESET_ATTEMPT' });
    dispatch(errorScreen(e.toString()));
  });
 }
}

export function getCurrentTaskQr(id) {
  return function action(dispatch) 
 {
  dispatch(currentTaskAttempt());
  const user = store.getState().user;
  const formData = new FormData();
  formData.append('type', 'assigned_to');
  formData.append('TaskId', id);
  formData.append('StudentId', user.pk);
  const config = {
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    }
  };
  const url = `${URL}/php/json_q.php`;
  const request = axios.post(url, formData, config);
  return request.then(data => {
    console.log(data, "data", data.data.success);
    dispatch ({type: 'RESET_ATTEMPT' });
    if (data.data.success) {
      console.log("success qr");
      dispatch(getCurrentTaskFromQRSuccess(data.data));
    } else {
      dispatch(getCurrentTaskFailQr());
    }
  }).catch((e)=> {
    dispatch ({type: 'RESET_ATTEMPT' });
    dispatch(errorScreen(e.toString()));
  });
 }
}
export const fetchMaps = (query, callback) => {
  return function action(dispatch) {
    dispatch({type: "ATTTEMPT_FETCH_MAPS"});
    let url = `https://nominatim.openstreetmap.org/search/${query}?format=json&polygon_geojson=1`;
    const request = axios.get(url);
    return request.then(data => {
      dispatch({type: "FETCH_MAPS_SUCCESS", payload: data.data});
      callback(data.data)
    }).catch((e)=> {
      console.log(e);
      dispatch({type: 'FETCH_MAPS_FAILURE'});
    });
  }
}

function getCurrentTaskFail(msg) {
  return {
    type: 'GET_CURRENT_TASK_FAIL',
    payload: {Name: 'klaida', Content: msg.toString()}
  }
}
function getCurrentTaskFailQr() {
  return {
    type: 'GET_CURRENT_TASK_FROM_QR_FAIL',
  }
}
function getCurrentTaskSuccess(payload) {
  return {
  type: 'GET_CURRENT_TASK_SUCCESS',
  payload
  }
}

function getCurrentTaskFromQRSuccess(payload) {
  return {
  type: 'GET_CURRENT_TASK_FROM_QR_SUCCESS',
  payload
  }
}

function currentTaskAttempt() {
  return {
    type: 'CURRENT_TYPE_ATTEMPT'
  }
}
function loginUser(user) {
  return function action(dispatch) {
    dispatch(loginAttempt());
    const formData = new FormData();
    formData.append("Name", user.name);
    formData.append("Surname", user.surname);
    formData.append("Password", user.password);

    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      }
    };
    const url = `${URL}/php/login.php`;
    const request = axios.post(url, formData, config);
    return request.then(data => {
      dispatch ({type: 'RESET_ATTEMPT' });
      if (data.data.success) {
        let withId = {
          pk: data.data.StudentId,
          name: user.name, username: user.surname,
          ClassGroupId: data.data.ClassGroupId,
          points: data.data.TotalPoints,
          complete_successfully: data.data.complete_successfully,
          class: data.data.Class,
          rooms: data.data.rooms,
          RewardPoints: data.data.RewardPoints
        };
        
        dispatch(loginSuccess(withId));
        console.log(data);
        dispatch({ type: "NAV_MENU" });
      } else {
        dispatch(authFailed(data.data.msg));
      }
    }).catch((e)=> {
      console.log(e);
      dispatch ({type: 'RESET_ATTEMPT' });
      dispatch(authFailed("Patikrinkite interneto ryšį"));
    });
  };
}
function loginAttempt() {
  return {
    type: 'LOGIN_ATTEMPT'
  };
}
function authFailed(message) {
  return {
    type: "FAIL",
    payload: message
  };
}
function loginSuccess(withId) {
  return {
    type: "LOGIN",
    payload: withId
  };
}
function errorScreen(e) {
  dispatch({type: 'ERROR', payload: e.toString()})
  dispatch({type: NAV_ERROR});
}
export function getQuestionTestStateFromMap(id) {
  let mapJson = store.getState().testResults.map;
  let map;
  if (mapJson === "init") {
    return null;
  } else {
    map = new Map(JSON.parse(mapJson));
    let item = map.get(id);
    if (item) {
      return item.options;
    } else {
      return null;
    }
  }
}
export function getQuestionSortStateFromMap(id) {
  let mapJson = store.getState().testResults.map;
  let map;
  if (mapJson === "init") {
    return null;
  } else {
    map = new Map(JSON.parse(mapJson));
    let item = map.get(id);
    if (item) {
      return { options: item.options, order: item.order };
    } else {
      return null;
    }
  }
}
export function getClasses() {
  return function action(dispatch) {
    let result = axios.post(
      `${URL}/php/json_q.php`,
      querystring.stringify({
        type: "schools"
      }),
      {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded"
        }
      }
    );
    return result.then(data => {
      if (data.data.success) {
        dispatch(getClassesSuccess(data));
        dispatch({ type: "NAV_REGISTRATION" });
      }
    }).catch((e)=> {
      dispatch(errorScreen(e));
    });
  };
}
function getClassesSuccess(result) {
  return {
    type: "GET_CLASSES",
    payload: result
  };
}
export function login(name, surname, password) {
  const user = { name, surname, password };
  if (user != null && surname != null && password != null) {
    return dispatch => {
      dispatch(loginUser(user));
    };
  }
}
export function register(name, username, classId, password, classString) {
  const user = { name, username, classId, password, classString };
  if (name != null && username != null && classId != null && password != null) {
    return dispatch => {
      dispatch(registerUser(user));
    };
  }
}
function fetchQ(id) {
  return axios.post(
    `${URL}/php/json_q.php`,
    querystring.stringify({
      nodeid: id.toString(),
      type: "questions"
    }),
    {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      }
    }
  );
}
export function fetchQuestion(id, room) {
  return dispatch => {
    fetchQ(id).then(res => {
      dispatch({
        type: FETCH_QUESTION,
        payload: { questions: res.data.data, id: id }
      });
      dispatch({ type: "NAV_POLL", payload: room });
    });
  };
}