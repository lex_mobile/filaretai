import {
  createStackNavigator,
} from 'react-navigation';
import {
  reduxifyNavigator,
  createReactNavigationReduxMiddleware,
} from 'react-navigation-redux-helpers';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import React, {Component } from 'react';
import SplashScreen from './containers/splashScreen';
import MenuScreen from './containers/mainMenu';
import QuestionsScreen from './index';
import PollSreen from './containers/questionDetails';
import RegistrationScreen from './containers/registration';
import MainRoom from './containers/mainRoom';
import NatureRoom from './containers/natureRoom';
import questionPhoto from './components/questionPhoto';
import CameraGalleryScreen from './components/imageGallery';
import GlobalHeader from './components/globalHeader';
import Login from './containers/login';
import UserTasks from './containers/tasksScreen';
import ErrorScreen from './containers/errorScreen';
import Nature1 from './containers/Nature1';
import Room from './containers/room';
import Scanner from './components/qrCodeScanner';
import map from './components/maps';

export const NAV_NATURE1 = 'NAV_NATURE1';
export const NAV_SPLASH = 'NAV_SPLASH';
export const NAV_MENU = 'NAV_MENU';
export const NAV_QUESTIONS = 'NAV_QUESTIONS';
export const NAV_POLL = 'NAV_POLL';
export const NAV_SETTINGS = 'NAV_SETTINGS';
export const NAV_STUDENTAREA = 'NAV_STUDENTAREA';
export const NAV_REGISTRATION = 'NAV_REGISTRATION';
export const NAV_MAINROOM = 'NAV_MAINROOM';
export const NAV_NATUREROOM = 'NAV_NATUREROOM';
export const NAV_LIBRARYROOM = 'NAV_LIBRARYROOM';
export const NAV_CAMERA = 'NAV_CAMERA';
export const NAV_GALLERY = 'NAV_GALLERY';
export const NAV_LOGIN = 'NAV_LOGIN';
export const NAV_TASKS = 'NAV_TASKS';
export const NAV_ERROR = 'NAV_ERROR';
export const NAV_SCANNER = "NAV_SCANNER";
export const NAV_MAPS = "NAV_MAPS";

const middleware = createReactNavigationReduxMiddleware(
  'root',
  state => state.nav,
);

const RootNavigator = createStackNavigator({
  NAV_SPLASH: { screen: SplashScreen },
  NAV_MENU: { screen: MainRoom },
  NAV_QUESTIONS: { screen: QuestionsScreen },
  NAV_POLL: { screen: PollSreen },
  NAV_REGISTRATION: { screen: RegistrationScreen },
  NAV_MAINROOM: { screen: MainRoom },
  NAV_CAMERA: {screen: questionPhoto },
  NAV_GALLERY: {screen: CameraGalleryScreen },
  NAV_NATUREROOM: {screen: Room },
  NAV_LIBRARYROOM: {screen: Room },
  NAV_LOGIN: {screen: Login},
  NAV_TASKS: {screen: UserTasks},
  NAV_ERROR: {screen: ErrorScreen},
  NAV_NATURE1: {screen: Room},
  NAV_SCANNER: {screen: Scanner},
  NAV_MAPS: {screen: map}
},
{
  initialRouteName: NAV_SPLASH,
  navigationOptions: {
    header: <GlobalHeader withBack={true} />,
  }
});

const AppWithNavigationState = reduxifyNavigator(RootNavigator, 'root');
const mapStateToProps = state => ({
  state: state.nav,
});

const AppNavigator = connect(mapStateToProps)(AppWithNavigationState);

export { RootNavigator, AppNavigator, middleware };
