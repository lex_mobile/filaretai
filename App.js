import React, { Component } from "react";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import ReduxPromise from "redux-promise";
import ReduxThunk from "redux-thunk";
import { Root, Spinner } from "native-base";
import * as Expo from "expo";
import reducers from "./reducers";
import { AppNavigator } from "./navigation";
import {createLogger} from "redux-logger";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import { PersistGate } from "redux-persist/lib/integration/react";
import firebase from 'react-native-firebase';

let Analytics = firebase.analytics();
let rnCrashlytics = firebase.crashlytics();
const persistConfig = {
  key: "root",
  timeout: 10000,
  storage: storage,
  whitelist: ["user"] // see "Merge Process" section for details.
};
const logger = createLogger({

});
const pReducer = persistReducer(persistConfig, reducers);

export const store = createStore(
  pReducer,
  applyMiddleware(ReduxPromise, ReduxThunk)
);

export const persistor = persistStore(store);
export default class App extends Component {
  constructor(props) {
    super(props);
    Analytics.setAnalyticsCollectionEnabled(true);
    this.state = { loading: true };
    rnCrashlytics.enableCrashlyticsCollection(true);
  }

  async componentWillMount() {
    //socket = io('http://localhost:3000', {query: {id: }});
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"), 
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf"),
      CaveatBrush: require("./assets/fonts/CaveatBrush-Regular.ttf"),
      Pangolin: require("./assets/fonts/Pangolin-Regular.ttf"),
    });
    this.setState({ loading: false });
  }

  render() {
    if (this.state.loading) {
      return <Expo.AppLoading />;
    }

    return (
      <Provider store={store}>
        <PersistGate loading={<Spinner />} persistor={persistor}>
          <Root>
            <AppNavigator />
          </Root>
        </PersistGate>
      </Provider>
    );
  }
}
//1153,1219,1160