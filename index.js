import React, { Component } from 'react';
import { TouchableWithoutFeedback, Keyboard, Image } from 'react-native';
import {
  Container,
  Header,
  Left,
  Right,
  Title,
  Body,
  Icon,
  Button,
  Tabs,
  Tab,
  Content,
  Item,
  Input,
  Spinner,
  Root,
  Text,
} from 'native-base';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchCategory, getAllData } from './actions/index';
import CategoryList from './containers/categoryList';
import { Constants } from 'expo';
import QuestionDetails from './containers/questionDetails';
import navigation from './reducers/navigation';

const initReqParams = { deep: 0, questions: false };


class App extends Component {
  constructor(props) {
    super(props);
    this.state = { page: 0, search: '' };
    this.changePage = this.changePage.bind(this);
    this.resetSearch = this.resetSearch.bind(this);
  }

  static navigationOptions = ({ navigation }) => {
    return {
      header: <Header style={{ marginTop: Constants.statusBarHeight }}>
        <Left>
          <Button transparent onPress={() => { navigation.goBack() }}>
            <Icon name="arrow-back" />
          </Button>
        </Left>
        <Body style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <Item style={{ justifyContent: 'center' }}>
            <Input
              placeholder="Paieška"
              value={navigation.getParam('search')}
              onChangeText={(search) => { navigation.setParams({ 'search': search }) }}
            />
          </Item>
        </Body>
        <Right>
          <Text>{navigation.getParam('title', '')} </Text>
        </Right>
      </Header>,
    };
  }
  componentDidMount() {
    let name = `${this.props.user.name} ${this.props.user.username} ${this.props.user.classString}`;
    this.props.navigation.setParams({ 'title': name });
    this.props.navigation.setParams({ 'search': '' });
    this.props.getAllData();
  }

  changePage(pageNo) {
    this.setState({ page: pageNo });
  }

  load() {
    this.props.initLoad(initReqParams);
  }

  resetSearch() {
    this.setState({ search: '' });
    Keyboard.dismiss();
  }

  renderQuestion() {
    return (<QuestionDetails />);
  }

  render() {
    if (!this.props.categories) {
      return (<Spinner />);
    }
    return (

      <Container>
        <TouchableWithoutFeedback onPressIn={Keyboard.dismiss} accessible={false}>
          <CategoryList changePage={this.changePage} search={this.props.navigation.getParam('search')} resetSearch={this.resetSearch} />
        </TouchableWithoutFeedback>
      </Container>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ initLoad: fetchCategory, getAllData }, dispatch);
}

function mapStateToProps(state) {
  return {
    showQuestion: state.questions,
    categories: state.categories,
    alldata: state.allData,
    user: state.user,
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(App);
