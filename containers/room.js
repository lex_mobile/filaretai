import React, { Component } from "react";
import { ImageBackground, Platform } from "react-native";
import { View } from "native-base";
import RoomObject from "../components/roomObject";
import { setPosition, translateX, translateY, LIBRARY, NATURE, NATURE1} from "../globals";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchQuestion } from "../actions/index";
import { moderateScale } from "../components/ScaleElements";

class Room extends Component {

constructor(props) {
    super(props);
    this.objectStyles = {
      // top, right, bottom, left
      ["cabinet"]: [setPosition(0, 0, 240, "50%"), translateX(475)],
      // ["shelfRight"]: setPosition(0, "0%", '25%', 0),
      // ["shelfLeft"]: setPosition(0, 0, '25%', "0%"),
      ["wheel"]: setPosition(
        0,
        moderateScale(80),
        Platform.OS == "ios" ? "76%" : "71%",
        0
      ),
      ["shield"]: setPosition(0, 120, "20%", 0),
      ["herbas"]: setPosition(Platform.OS == "ios" ? 20 : 50, 0, 0, 150),
      ["flag"]: setPosition(0, "35%", Platform.OS == "ios" ? "58%" : "56%", 0),
      ["hook"]: setPosition(0, 0, Platform.OS == "ios" ? "25%" : "30%", "20%"),
      ["tree"]: setPosition(null, 50, Platform.OS == "ios" ? 300 : 200),
      ["flower"]: setPosition(
        null,
        null,
        Platform.OS == "ios" ? 30 : 50,
        Platform.OS == "ios" ? 30 : 100
      ),
      ["bush"]: setPosition(null, null, Platform.OS == "ios" ? 135 : 200, 50),
      ["weed"]: setPosition(null, 10, 20),
      ["stump"]: [setPosition(null, "50%", 120), translateX(-221)],
      ["nendre"]: setPosition(
        null,
        Platform.OS == "ios" ? "20%" : "38%",
        Platform.OS == "ios" ? "15%" : "21%",
        0
      ),
      ["shroom"]: [
        setPosition(null, "50%", Platform.OS == "ios" ? "2%" : "4%"),
        translateX(-221)
      ],
      ["bin"]: [setPosition(null, "50%", 120), translateX(-221)],
      ["kelmas"]: [setPosition(0, "30%", "20%", 0)],
      ["kelmasVoras"]: [setPosition(0, "30%", "11%", 0)],
      ["siuksliadeze"]: [
        setPosition(0, 0, "10%", "25%"),
        translateX(-50),
        translateY(-100)
      ],
      ["siuksliadeze_pirma"]: [
        setPosition(null, "50%", "4%"),
        translateX(-221)
      ],
      ["liepa"]: [
        setPosition(0, 0, Platform.OS == "ios" ? "32%" : "33%", 20),
        translateX(100)
      ],
      ["klevas"]: [
        setPosition(0, 20, Platform.OS == "ios" ? 300 : 350, 0),
        translateX(-150)
      ],
      ["sermuksnis"]: [setPosition(0, 0, 70, 45), translateX(-50)],
      ["siuksle"]: [
        setPosition(moderateScale(350), null, 30, "20%"),
        translateX(-221)
      ],
      ["egle"]: setPosition(0, 170, 320, 0),
      ["vynuoge"]: setPosition(0, 20, 5, 0),
      ["tree_nr2"]: [setPosition(0, "35%", "39.6%", 0), translateX(-150)]
    };
    this.backgrounds = {
        [LIBRARY]: require('../images/png/backgrounds/libraryBackground2.png'),
        [NATURE1]: require('../images/png/backgrounds/natureRoom_2.png'),
        [NATURE]: require('../images/png/backgrounds/natureRoom_1.png'),
    };
  }

  
 shouldShow = (array, ids, success, fail) => {
  if(ids.length === 0) {
    return success;
  }
  let res = ids.every((item)=> array.some((initem) => initem.PollCategoryId === item))? success : fail;
  return res;
}

  generateRoomObject = (object, id, unlockedAfter, key) => {
    //console.log(object, "obj", unlockedAfter, shouldShow(this.props.finishedPolls, unlockedAfter, object, ""));
    return <RoomObject
      key={key}
      object={this.shouldShow(this.props.finishedPolls, unlockedAfter, object, "")}
      solved={this.shouldShow(this.props.finishedPolls, unlockedAfter, true, false)}
      finished={{finishedPolls: this.props.finishedPolls, id}}
      action={()=> {this.props.fetchQuestion(id, this.props.navigation.getParam('roomName', ''))}}
      style={this.objectStyles[object]}
    />;
   }

  render() {
    let objects = this.props.user.rooms[this.props.navigation.getParam('roomName', '')].map((item, key)=> {
      return this.generateRoomObject(item.object, item.id, item.unlockedAfter, key);
    });
    return (
    <View style={{ flex: 1 }}>
    <ImageBackground
      source={this.backgrounds[this.props.navigation.getParam('roomName', '')]}
      style={{
        width: "100%",
        height: "100%"
      }}
      resizeMode="cover"
    >
    {objects}
    </ImageBackground>
    </View>
    );
  }
}

function mapStateToProps(state) {
    return {
        user: state.user,
        finishedPolls: state.user.complete_successfully
    };
  }
  function mapDispatchToProps(dispatch) {
    return bindActionCreators({ fetchQuestion }, dispatch);
  }

  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(Room);