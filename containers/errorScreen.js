import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { connect } from 'react-redux'

export class errorScreen extends Component {


  render() {
    return (
      <View>
        <Text> ERROR </Text>
        <Text>{this.props.error}</Text>
      </View>
    )
  }
}

const mapStateToProps = (state) => ({
  error: state.error
})

const mapDispatchToProps = {
  
}

export default connect(mapStateToProps, mapDispatchToProps)(errorScreen)
