import React, { Component } from "react";
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  Image,
  Dimensions,
  ScrollView,
  KeyboardAvoidingView,
  StyleSheet,
  ImageBackground,
} from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  getTasks,
  getCurrentTask,
  resetCurrentTask,
  submitTask
} from "../actions/index";

import { Spinner, Button, Input, Icon, } from "native-base";
import { styleVariables } from "../globals";
import QuestionPhotoCard from "../components/questionPhotoCard";
const { width, height } = Dimensions.get("window");
import ImageView from "react-native-image-view";
import { Video } from "expo";
import Accordion from "react-native-collapsible/Accordion";
import GlobalHeader from "../components/globalHeader";
import { moderateScale } from "../components/ScaleElements";
// 'tasks' state structure, where data: is prop itself
// data: [
//     ...
// {
// Name: String
// Content: String
// TaskId: Int
// TasksAssignedToId: Int
// Status: Int
// Answer: String
// StudentId: Int
// }
// ...
// ]

const style = StyleSheet.create({
  statusText: { fontSize: styleVariables.fontSize, color: "white" }
});
export class tasksScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openTaskDetails: false,
      text: "",
      tab: 2,
      show: [],
      activeSections: [],
      photoCount: 0,
      opacityTrigger: false
    };
    this.renderPhotoList = this.renderPhotoList.bind(this);
    this.renderAnswerView = this.renderAnswerView.bind(this);
    this.changeTab = this.changeTab.bind(this);
    this.videoRefs = [];
    this.scrollview = null;
  }
  static navigationOptions = ({ navigation }) => {
    return {
      header: (
        <GlobalHeader withBack={false}>
          <TouchableOpacity
            onPress={() => {
              let innerBack = navigation.getParam("innerBack", true);
              if (innerBack) {
                navigation.goBack();
              } else {
                navigation.setParams({ innerBack: true });
              }
            }}
          >
            <Icon
              name="arrow-back"
              style={{
                paddingHorizontal: 16,
                paddingVertical: 10,
                backgroundColor: "#8281e4",
                borderRadius: 8,
                color: "#fff"
              }}
            />
          </TouchableOpacity>
        </GlobalHeader>
      )
    };
  };

  componentDidMount() {
    let fromQr = this.props.navigation.getParam("fromQr", false);
    if(!fromQr) {
      this.props.getTasks();
    }
    this.props.navigation.setParams({ innerBack: !fromQr ? true : false});
  }
  changeTab(tab) {
    this.setState({ tab });
  }
  _renderSectionTitle = section => {
    return (
      <View>
        <Text>{section.content}</Text>
      </View>
    );
  };

  _renderHeader = section => {
    return (
      <View style={{ alignSelf: "center", width: "100%" }}>
        <Text
          style={{
            padding: 15,
            fontSize: styleVariables.fontSize,
            color: "#000",
            backgroundColor: "#498fff",
            borderRadius: 20
          }}
        >
          {section.title}
        </Text>
      </View>
    );
  };

  _renderContent = section => {
    return section.content;
  };

  renderGivenPoints(points, style) {
    return (
      <View>
        <Text
          adjustsFontSizeToFit
          style={[{
            color: "black",
            fontFamily: "CaveatBrush",
            flexWrap: "wrap",
            fontSize: 15,
            paddingBottom: 15
          }, style]}
        >
          Gavai {points} tasku uz sia uzduoti
        </Text>
      </View>
    );
  }

  renderListItem(item) {
    return (
      <View
        key={item.Name}
        style={{
          flexGrow: 1,
          paddingTop: 30,
          paddingHorizontal: 15,
          marginBottom: 15,
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
          fontFamily: "CaveatBrush",

        }}
      >
      <View style={{height: 50,width: 50, borderWidth: 4, borderRadius: 8,  borderColor: "#fff", marginRight: 10}}>
          {/* display on finished task goes flex */}
          <View style={{height: 50,width: 50, display: item.Status === 1 ? "flex" : "none"}}>
            <Icon
              style={{
                color: "#f5a623",
                fontSize: 46,
              }}
              name="check"
              type="Feather"
            >
            </Icon>
          </View>
          <View style={{height: 50,width: 50, display: item.Status === 0 ? "flex" : "none"}}>
            <Icon
              style={{
                color: "#CD5C5C",
                alignSelf: "center",
                fontSize: 43,
                marginRight: moderateScale(4),
              }}
              name="md-close"
              type="Ionicons"
            >
            </Icon>
          </View>
          <View style={{height: 50,width: 50, alignItems: "center", display: item.Status === 3 ? "flex" : "none"}}>
          <Icon
            style={{
              color: "#CD5C5C",
              alignSelf: "center",
              flex: 1,
              marginRight: moderateScale(4),
              fontSize: 45,
            }}
            name="information"
            type="Ionicons"
          >
          </Icon>
        </View>
        </View>
        <View
          style={{
            padding: 10,
            flex: 1,
            flexDirection: "column",
            alignItems: "flex-start",
            justifyContent: "center"
          }}
        >
          <Text
            adjustsFontSizeToFit
            style={{
              color: "#f5a623",
              fontFamily: "CaveatBrush",
              flexWrap: "wrap",
              fontSize: styleVariables.fontSize,
              paddingBottom: 15,
              fontWeight: "bold",
     
            }}
          >
            {item.Name}
          </Text>
          <Text
            numberOfLines={6}
            adjustsFontSizeToFit
            style={{
              flex: 1,
              color: "white",
              flexWrap: "wrap",
              fontSize: styleVariables.fontSize,
              fontFamily: "CaveatBrush",
            }}
          >
            {item.Content}
          </Text>
        </View>

        <View>
          <TouchableOpacity
            style={{
              backgroundColor: "#f5a623",
              borderRadius: 15,
              alignItems: "center",
              justifyContent: "center",
              width: 120,
              height: 50
            }}
            onPress={() => {
              this.props.getCurrentTask(item.TasksAssignedToId);
              this.setState({ openTaskDetails: true });
              this.props.navigation.setParams({ innerBack: false });
            }}
          >
            <Text
              adjustsFontSizeToFit
              style={{
                color: "white",
                alignSelf: "center",
                fontSize: styleVariables.fontSize * 1.2,
                fontFamily: "CaveatBrush"
              }}
            >
              Plačiau
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  openClose(i) {
    this.setState(prev => {
      let newState = prev.show;
      newState[i] = !newState[i];
      return newState;
    });
  }

  renderPhotoList(photos) {
    let photoList = [];
    photos.forEach((element, i) => {
      let imageModal = [
        {
          source: {
            uri: `http://filaretai.lexitacrm.lt${element.Path}`
          }
        }
      ];
      photoList.push(
        <View key={element.Path.toString() + i.toString()}>
          <ImageView
            images={imageModal}
            imageIndex={0}
            isVisible={
              typeof this.state.show[element.Path] === "undefined"
                ? false
                : this.state.show[element.Path]
            }
            onClose={() => {
              this.openClose(element.Path);
            }}
          />

          <TouchableOpacity
            onPress={() => {
              this.openClose(element.Path);
            }}
            style={{ flex: 1, margin: 1, flexDirection: "row" }}
          >
            <Image
              style={{
                resizeMode: "cover",
                width: width / 2.58,
                height: width / 2.58,
                
              }}
              source={{ uri: `http://filaretai.lexitacrm.lt${element.Path}` }}
            />
          </TouchableOpacity>
        </View>
      );
    });
    return photoList;
  }

  _updateSections = activeSections => {
    this.setState({ activeSections });
  };

  photoAddEvent = count => {
    this.setState({ photoCount: count });
  };

  renderRemark(remark, title, explicit) {
    if (
        this.props.currentTask.data.Status === 1 ||
        this.props.currentTask.data.Status === 0 ||
        explicit
      ) {
    return (
      <View style={{ paddingVertical: 50 }}>
        <Text style={{
            fontSize: 40,
            color: "#f5a623",
            fontFamily: "CaveatBrush",
            textAlign: 'center'
            }}
        >
            {title}
        </Text>
        <Text style={{ fontSize: 25, fontFamily: "CaveatBrush", color: "white", paddingLeft: 50 , textAlign: 'left'}}>{remark}</Text>
      </View>
    );
    }
  }
  _renderHeader = () => {
    return (
      <View style={{ justifyContent: "center", flex: 1, marginBottom: 5, backgroundColor: "#f5a623", paddingVertical: 10, borderRadius: 30, borderWidth: 5, borderColor: "#fff", flexDirection: "row" }} >
        <Text style={{color: "#fff", textAlign: "center", fontFamily: "CaveatBrush", fontSize: 34, marginRight: 10}}>Pradėti</Text>
      </View>
    );
  };

  //nauja atsakyta perkelt i virsu
  renderAnswerView() {
    if (
      this.props.currentTask.data.Status === 2 ||
      this.props.currentTask.data.Status === 0
    ) {
      return (
        <View style={{ marginVertical: 10, paddingBottom: 30 }}>
          <Accordion
            activeSections={this.state.activeSections}
            onChange={()=> {
              this.scrollview.scrollToEnd();
            }}
            contentStyle={{ alignItems: "center", textAlign: "center" }}
            renderHeader={this.renderHeader}
            sections={[
              {
                content: (
                  <ScrollView
                    ref={(ref)=> {this.scrollview = ref}}
                    style={{
                      flexGrow: 1,
                      height:
                        this.state.photoCount > 0
                          ? Dimensions.get("screen").width + 200
                          : undefined
                    }}
                  >
                    <View style={{  
                      height: 120,
                      borderRadius: 30,
                      borderWidth: 5,
                      borderColor: "#fff",
                    }}>
                    <Input
                      placeholder="Įveskite atsakymą"
                      placeholderTextColor="#fff"
                      placeholderStyle={{fontSize: 35, paddingLeft: 30}}
                      multiline
                      style={{
                        color: "#fff",
                        fontFamily: "CaveatBrush",
                        paddingLeft: 30,
                        textAlign: "left",
                        fontSize: 35,
                        opacity: (this.state.text? 1 : 0.5)
                      }}
                      // onPress={() => this.setState({opacityTrigger: !this.state.opacityTrigger})}
                      value={this.state.text}
                      onChangeText={text => this.setState({ text })}
                    />
                    </View>
                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "center",
                        alignSelf: "center",
                      }}
                    >
                    </View>
                    <View style={{ flexDirection: "column", flexGrow: 1,}}>
                      <QuestionPhotoCard
                        photoAddEvent={count => {
                          this.photoAddEvent(count);
                        }}
                        name={""}
                        task={true}
                      />
                    </View>
                    <Button
                        style={{
                          flex: 1,
                          width: "80%",
                          alignSelf: "center",
                          alignItems: "center",
                          justifyContent: "center",
                          backgroundColor: "#f5a623",
                          borderRadius: 15,
                        }}
                        onPress={() => {
                          this.props.submitTask(
                            this.props.currentTask.data.TasksAssignedToId,
                            this.state.text,
                            () => {this.props.getTasks(()=> {this.props.navigation.setParams({ innerBack: true })})}
                          );
                          this.setState({text: ""});
                        }}
                      >
                        <Text
                          adjustsFontSizeToFit
                          style={{
                            fontFamily: "CaveatBrush",
                            color: "#fff",
                            alignSelf: "center",
                            fontSize: styleVariables.fontSize * 1.2
                          }}
                        >
                          Atsakyti
                        </Text>
                      </Button>
                  </ScrollView>
                )
              }
            ]}
            renderHeader={this._renderHeader}
            renderContent={this._renderContent}
            onChange={this._updateSections}
          />
        </View>
      );
    } else {
      return null;
    }
  }

  renderVideoList = videoList => {
    let rendervideoList = [];
    videoList.forEach((item, i) => {
      rendervideoList.push(
        <View key={item.Path.toString() + i.toString()}>
          <View>
            <TouchableOpacity
              onPress={() => {
                this.videoRefs[item.Path].presentFullscreenPlayer();
              }}
            >
              <Icon
                name="film"
                type="Feather"
                style={{
                  padding: 6,
                  backgroundColor: "#8281e4",
                  borderRadius: 8,
                  color: "#fff",
                  position: "absolute",
                  right: 10,
                  bottom: 10
                }}
              />
              <Video
                ref={ref => {
                  this.videoRefs[item.Path] = ref;
                }}
                source={{ uri: `http://filaretai.lexitacrm.lt${item.Path}` }}
              
                style={{
                  width: width / 2.58,
                  height: width / 2.58,
                  margin: 1
                }}
              />
              <Icon
                name="film"
                type="Feather"
                style={{
                  padding: 6,
                  backgroundColor: "#8281e4",
                  borderRadius: 8,
                  color: "#fff",
                  position: "absolute",
                  right: 10,
                  bottom: 10
                }}
              />
            </TouchableOpacity>
          </View>
        </View>
      );
    });
    return rendervideoList;
  };

  renderStudentAnswer = () => {
    if (
      this.props.currentTask.data.Status === 1 ||
      this.props.currentTask.data.Status === 0 || 
      this.props.currentTask.data.Status === 3
    ) {
      return (
        <View>
        {this.renderRemark(this.props.currentTask.data.StudentAnswer, "Tavo paskutinis atsakymas:", true)}
        <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
          {this.renderPhotoList(this.props.currentTask.data.StudentPhotos)}
          {this.renderVideoList(this.props.currentTask.data.StudentVideos)}
        </View>
        </View>
      );
    }
  };

  render() {
    let render = [];
    if (this.props.tasks.init && this.props.tasks.data.success) {
      render.push(<Spinner />);
    } else if (
      !this.props.navigation.getParam("innerBack") &&
      this.props.currentTask.data
    ) {
      render.push(
        <KeyboardAvoidingView
          behavior="position"
          contentContainerStyle={{
            height: height * 0.92
          }}
          key={this.props.currentTask.data.TaskId}
        >
          <ImageBackground
      
            source={require("../images/png/backgrounds/board.png")}
            style={{ height: "100%", width: "100%", }}
          >
            <ScrollView
              contentContainerStyle={{
                flexGrow: 1,
                zIndex: 1
              }}
              style={{ margin: "10%", marginBottom: "24%" }}
            >
              <View style={{ flex: 0.5, justifyContent: "center" }}>
                <View
                  style={{
                    alignItems: "center",
                    paddingHorizontal: 15,
                    paddingVertical: 5
                  }}
                >
                  <Text
                    adjustsFontSizeToFit
                    style={{
                      fontSize: styleVariables.fontSize * 2,
                      marginBottom: 20,
                      color: "#f5a623",
                      fontFamily: "CaveatBrush"
                    }}
                  >
                    {this.props.currentTask.data.Name}
                  </Text>
                  <Text
                    adjustsFontSizeToFit
                    style={{
                      color: "white",
                      fontSize: styleVariables.fontSize,
                      fontFamily: "CaveatBrush",
                      textAlign: 'left',
                      alignSelf: 'flex-start',
                      paddingLeft: 50 
                    }}
                  >
                    {this.props.currentTask.data.Content}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    flexWrap: "wrap"
                  }}
                >
                  {this.renderPhotoList(this.props.currentTask.data.Photos)}
                  {this.renderVideoList(this.props.currentTask.data.Videos)}
                </View>
                {this.renderStudentAnswer()}
                {this.renderRemark(this.props.currentTask.data.TeacherComment, "Mokytojos komentaras:")}
                {this.props.currentTask.data.Status === 3 ? <View>
                  <Text style={{
                       fontSize: 40,
                       color: "#f5a623",
                       fontFamily: "CaveatBrush",
                       textAlign: 'center'
                  }}>
                    Laukiama mokytojos įvertinimo
                  </Text>
                </View> : null}
                {this.renderAnswerView()}
              </View>
            </ScrollView>
          </ImageBackground>
        </KeyboardAvoidingView>
      );
    } else if (this.props.tasks.data) {
      if (this.props.tasks.count > 0) {
        render.push(
          <ImageBackground
            
           
            key={"itemList"}
            source={require("../images/png/backgrounds/board.png")}
            style={{ height: "100%", width: "100%", }}
          >
            <View
              style={{
                position: "absolute",
                top: 90,
                alignSelf: "center",
                height: "68%",
                width: "80%"
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  fontSize: 32,
                  color: "#fff",
                  fontFamily: "CaveatBrush",
                  fontWeight: "300"
                }}
              >
                Užduotys
              </Text>
              <FlatList
                scrollEnabled
                contentContainerStyle={{
                  flexGrow: 1
                }}
                data={this.props.tasks.data}
                renderItem={({ item }) => {
                  return this.renderListItem(item);
                }}
                keyExtractor={(item, index) =>
                  item.TaskId.toString() + index.toString()
                }
              />
            </View>
          </ImageBackground>
        );
      } else {
        render.push(
          <View>
            <ImageBackground
          
            key={"itemList"}
            source={require("../images/png/backgrounds/board.png")}
            style={{ height: "100%", width: "100%", }}
          >
          <View style={{ 
                  top: "12%",
                  left:0,
                  right: 0,
                  bottom: 0,     
                  alignItems: 'center',
                  // justifyContent: "center",
                  position: "absolute",
                  
                  }}>
          <Text
              style={[
                style.statusText,
                {
                
                  textAlign: "center",
                  fontFamily: "CaveatBrush",
                  // color: "#010048",
                  color: "#fff",
                  fontSize: 40, 
            
                }
              ]}
            >
              Užduočių nėra...
            </Text>
          </View>
       
            </ImageBackground>
          </View>
        );
      }
    } else {
      render.push(
        <View key={"error"}>
          <Text>Klaida</Text>
          <Text>{this.props.tasks.msg}</Text>
        </View>
      );
    }
    return (
      <View style={{ flex: 1 }}>
        <View
          style={{
            flex: 1
          }}
        >
          <View style={{ zIndex: 999, flexGrow: 1 }}>{render}</View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  tasks: state.tasks,
  currentTask: state.currentTask,
  user: state.user
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getTasks,
      getCurrentTask,
      resetCurrentTask,
      submitTask
    },
    dispatch
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(tasksScreen);
