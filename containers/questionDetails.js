import React, { Component } from "react";
import { Text, Card, CardItem, Body } from "native-base";
import { StyleSheet } from "react-native";
import { connect } from "react-redux";
import { submitResults, resetAnswers, pushAnswer } from "../actions/index";
import { bindActionCreators } from "redux";
import {
  View,
  Modal,
  TouchableHighlight,
  TouchableOpacity,
  Dimensions,
  Image
} from "react-native";
import QuestionTest from "../components/questionTest";
import QuestionMatch from "../components/questionDnd";
import QuestionSort from "../components/questionSort";
import QuestionGroup from "../components/questionGroup";
import GlobalHeader from "../components/globalHeader";

import MultipleDragDrop from "../components/multipleDragDrop";
const { width, height } = Dimensions.get("window");
import QuestionPhotoCard from "../components/questionPhotoCard";
import { styleVariables, shuffle, LIBRARY, NATURE, NATURE1 } from "../globals";
import { moderateScale } from "../components/ScaleElements";

const images = {
  [LIBRARY]: [
    require("../images/png/backgrounds/anthem.png"),
    require("../images/png/backgrounds/symbols.png"),
    require("../images/png/backgrounds/history.png")
  ],
  [NATURE]: [require("../images/png/backgrounds/natureRoom_1.png")],
  [NATURE1]: [require("../images/png/backgrounds/natureRoom_2.png")]
};
const backgroundImages = [
  { key: [3, 4, 5, 26, 27, 28, 24, 25, 36], val: 4 },
  { key: [14, 15, 11, 13], val: 1 },
  { key: [12], val: 0 },
  { key: [18, 22, 20, 23, 19, 33, 38], val: 2 },
  { key: [17, 16], val: 3 }
];

const RIGHT = "RIGHT";
const LEFT = "LEFT";

const LEFT_ARROW = "../images/png/components/arrow-left.png";
const RIGHT_ARROW = "../images/png/components/arrow-right.png";
const RIGHT_ARROW_END = "../images/png/components/arrow-right-save.png";
const CORRECT_FEEDBACK_MODAL = "../images/png/components/good-answer.png";
const INCORRECT_FEEDBACK_MODAL = "../images/png/components/bad-answer.png";
class QuestionDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      current: 0,
      modal: false,
      showFeedBackModal: false,
      leftArrow: LEFT_ARROW,
      backgroundOpacity: false
    };
    this.questions = [];
    this.currentCorrect = "null";
    this.nextQuestion = this.nextQuestion.bind(this);
    this.feedback = this.feedback.bind(this);
    this.counter = 0;
    this.MAP = new Map();
    backgroundImages.forEach(item => {
      item.key.forEach(key => {
        this.MAP.set(key, item.val);
      });
    });
  }
  static navigationOptions = ({ navigation }) => {
    return {
      header: (
        <GlobalHeader
          withBack
          clearTestResults
          noPoints
          title={"Prisijungimas"}
        />
      )
    };
  };
  componentDidMount() {
    let name = `${this.props.user.name} ${this.props.user.username} ${
      this.props.user.classString
    }`;
    this.props.navigation.setParams({ title: name });
    this.questions = this.renderQuestion();
  }

  componentWillReceiveProps() {
    if (this.props.question) {
      this.questions = this.renderQuestion();
    }
  }

  nextQuestion() {
    if (this.state.current < this.props.question.length - 1) {
      this.setState(prevState => {
        return { current: prevState.current + 1 };
      });
    } else {
      this.setState({ current: 0 }, () => {
        this.setState({ modal: true });
      });
    }
  }

  renderQuestion() {
    if (this.props.question) {
      const question = this.props.question.map((item, i) => {
        let opts = null;
        if (item.PollQuestionTypeId != 5) {
          opts = item.Answers.map(answ => {
            return {
              name: answ.Name,
              image: answ.PhotoUrl,
              correct: answ.IsCorrect,
              id: answ.id,
              order: answ.SortOrder
            };
          });
        }
        let key = `${item.name}${i}`;
        switch (item.PollQuestionTypeId) {
          case 1:
            return (
              <QuestionTest
                key={key}
                name={item.Name}
                image={item.PhotoUrl}
                id={item.id}
                options={opts}
                feedback={this.feedback}
                onUserClickCheckbox={this.saveCurrent}
                next={this.nextQuestion}
              />
            );
          case 2:
            return (
              <QuestionMatch
                key={key}
                name={item.Name}
                opts={item.Answers}
                id={item.id}
                next={this.nextQuestion}
                feedback={this.feedback}
                onUserDrag={this.saveCurrent}
                boxMarginFactor={2}
                boxSize={150 / (1 + item.Answers.length / 10)}
              />
            );
          case 3:
            return (
              <QuestionSort
                key={key}
                name={item.Name}
                options={opts}
                id={item.id}
                next={this.nextQuestion}
                feedback={this.feedback}
                //onChangeOrder={this.saveCurrent}
              />
            );
          case 4:
            //NEEGZISTUOJA TOKS KLAUSIMAS
            return (
              <QuestionGroup
                key={key}
                name={item.Name}
                options={opts}
                id={item.id}
                next={this.nextQuestion}
              />
            );
          case 5:
            return (
              <QuestionPhotoCard
                key={key}
                name={item.Name}
                options={opts}
                id={item.id}
                next={this.nextQuestion}
              />
            );
          case 6:
            let randomOrder = [];
            let requiredMatches = 0;
            for (let i = 0; i < item.Answers.length; i++) {
              requiredMatches += item.Answers[i].Details.length;
            }
            for (let j = 0; j < requiredMatches; j++) {
              randomOrder[j] = j;
            }
            randomOrder = shuffle(randomOrder);
            return (
              <MultipleDragDrop
                key={key}
                name={item.Name}
                opts={item.Answers}
                id={item.id}
                image={item.PhotoUrl}
                randomOrder={randomOrder}
                next={this.nextQuestion}
                feedback={this.feedback}
                onUserDrag={this.saveCurrent}
                boxMarginFactor={2}
                boxSize={150 / (1 + item.Answers.length / 10)}
              />
            );
        }
      });
      // this.setState({all: question}, () => {
      //   this.setState({loaded: true});
      //   this.setState({currentQuestion: this.state.all[this.state.current]});
      // });
      return question;
    }
  }

  feedback = res => {
    this.currentCorrect = res;
  };

  getCorrectOutOfN = () => {
    let correct = 0;
    let jsonMap = this.props.correct.map;
    let map;
    let i = 0;
    if (jsonMap !== "init") {
      map = new Map(JSON.parse(jsonMap));
      for (const value of map.values()) {
        value.result ? (correct += 1) : null;
        i++;
      }
      return (
        <View style={style.questionFeedbackContainer}>
          <View style={style.questionFeedbackTextBlock}>
            <Text style={style.questionFeedbackText}>Teisingai atsakei: </Text>
            <Text style={style.questionFeedbackText}>
              {correct} / {this.questions.length}
            </Text>
          </View>
          <View style={style.questionFeedbackTextBlock}>
            <Text style={style.questionFeedbackText}>Praleidai: </Text>
            <Text style={style.questionFeedbackText}>
              {this.questions.length - i}
            </Text>
          </View>
          <View style={style.questionFeedbackTextBlock}>
            <Text style={style.questionFeedbackText}>Surinkai: </Text>
            <Text style={style.questionFeedbackText}>{correct * 10} taškų</Text>
          </View>
        </View>
      );
    } else {
      return (
        <View style={[style.questionFeedbackTextBlock]}>
          <Text style={[style.questionFeedbackText,{ textAlign: 'center' }]}>Visas užduotis praleidai</Text>
        </View>
      );
    }
  };

  closeModal = () => {
    this.setState({ modal: false });
    this.props.resetAnswers();
  };

  renderCurrentQuestionCounter = () => {
    if (this.questions) {
      return (
        <Text
          adjustsFontSizeToFit
          style={{
            fontSize: styleVariables.fontSize,
            fontFamily: "CaveatBrush",
            color: "#8281e4"
          }}
        >
          {this.state.current + 1} / {this.questions.length}{" "}
        </Text>
      );
    }
  };

  renderTopLeftArrow = () => {
    let button = (
      <TouchableOpacity
        onPress={() => {
          this.currentCorrect = "null";
          this.setState(prev => {
            let next = (prev.current -= 1);
            return {
              current: next < 0 ? this.props.question.length - 1 : next
            };
          });
        }}
      >
        <Image source={require(LEFT_ARROW)} />
      </TouchableOpacity>
    );
    if (this.state.current !== 0) {
      return button;
    }
  };

  renderTopRightArrow = () => {
    let arrowImage = null;
    if (this.state.current + 1 === this.questions.length) {
      arrowImage = <Image source={require(RIGHT_ARROW_END)} />;
    } else {
      arrowImage = <Image source={require(RIGHT_ARROW)} />;
    }
    return (
      <TouchableOpacity
        onPress={() => {
          let showFeedBackModal = true;
          if (this.currentCorrect !== "null") {
            this.props.pushAnswer(this.currentCorrect);
            if (this.state.current + 1 === this.questions.length) {
              this.setState({ modal: true });
              this.props.submitResults(this.props.id, this.questions.length);
              showFeedBackModal = false;
            }
            if (showFeedBackModal) {
              this.setState({ showFeedBackModal: true }, () => {
                setTimeout(() => {
                  this.currentCorrect = "null";
                  this.setState(prev => {
                    let current = (prev.current += 1);
                    return { current, showFeedBackModal: false };
                  });
                }, 3000);
              });
            }
          } else if (this.currentCorrect === "null") {
            if (this.state.current + 1 === this.questions.length) {
              this.setState({ modal: true });
              this.props.submitResults(this.props.id, this.questions.length);
              showFeedBackModal = false;
            } else {
              this.currentCorrect = "null";
              this.setState(prev => {
                let next = (prev.current += 1);
                return {
                  current: next <= this.props.question.length - 1 ? next : 0
                };
              });
            }
          }
        }}
      >
        {arrowImage}
      </TouchableOpacity>
    );
  };
  getRandomInt = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  };

  getBackgroundImage = () => {
    let items = images[this.props.navigation.getParam("background", LIBRARY)];
    return items[this.getRandomInt(0, items.length - 1)];
  };
  renderResultsModal = () => {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.modal}
        onRequestClose={() => {}}
      >
        <View style={style.renderResultsModalContainer}>
          <Image
            source={images[this.MAP.get(this.props.id)]}
            style={{
              position: "absolute",
              width,
              height
            }}
          />
          <View style={style.renderFeedBackModalBlock}>
            <View
              style={{
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Text
                style={[
                  style.questionFeedbackText,
                  { fontSize: moderateScale(24), marginBottom: 20 }
                ]}
              >
                Rezultatai
              </Text>
              {this.getCorrectOutOfN()}
            </View>
            <View>
              <TouchableOpacity
                style={style.renderResultsModalButton}
                onPress={() => {
                  this.closeModal();
                  this.props.navigation.goBack();
                }}
              >
                <Text style={[style.questionFeedbackText, { marginBottom: 0 }]}>
                  Eiti į meniu
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  };

  renderFeedBackModal = () => {
    let modalImageToShow = null;
    if (this.currentCorrect.result) {
      // this.setState({backgroundOpacity: true})
      modalImageToShow = (
        <Image
          source={require(CORRECT_FEEDBACK_MODAL)}
          style={style.renderFeedBackModalImage}
        />
      );
    } else {
      // this.setState({backgroundOpacity: true})
      modalImageToShow = (
        <Image
          source={require(INCORRECT_FEEDBACK_MODAL)}
          style={style.renderFeedBackModalImage}
        />
      );
    }
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.showFeedBackModal}
        onRequestClose={() => {}}
      >
        <View style={style.renderFeedBackModalImageToShowBlock}>
          {modalImageToShow}
        </View>
      </Modal>
    );
  };

  renderCurrentQuestion = () => {
    if (this.questions && !this.state.showFeedBackModal) {
      return this.questions[this.state.current];
    }
  };

  render() {
    if (!this.props.question || this.props.question.length === 0) {
      return (
        <Card>
          <CardItem>
            <Body>
              <Text>Įvyko klaida</Text>
              <Text>Patikrinkite interneto ryšį</Text>
              <Text>arba blogai nustatytas pollCategoryId</Text>
            </Body>
          </CardItem>
        </Card>
      );
    } else {
      return (
        <View
          style={[
            style.renderMainBlock,
            {
              opacity: this.state.backgroundOpacity ? 0.5 : 1,
              backgroundColor: this.state.backgroundOpacity ? "black" : "none"
            }
          ]}
        >
          <Image
            source={this.getBackgroundImage()}
            style={{
              position: "absolute",
              width,
              height
            }}
          />
          <View style={style.renderBlock}>
            {this.renderTopLeftArrow()}
            {this.renderCurrentQuestionCounter()}
            {this.renderTopRightArrow()}
          </View>
          {this.renderCurrentQuestion()}
          {this.renderFeedBackModal()}
          {this.renderResultsModal()}
        </View>
      );
    }
  }
}

const style = StyleSheet.create({
  questionFeedbackText: {
    // fontSize: styleVariables.fontSize,
    fontSize: moderateScale(22),
    color: "#fff",
    fontFamily: "CaveatBrush",
    marginBottom: 15
  },
  questionFeedbackTextBlock: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  questionFeedbackContainer: {
    justifyContent: "center",
    flexDirection: "column"
  },
  renderFeedBackModalImage: {
    width: width / 2,
    resizeMode: "contain"
  },
  renderFeedBackModalImageToShowBlock: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  renderMainBlock: {
    flexDirection: "column",
    flexGrow: 1,
    justifyContent: "flex-start"
  },
  renderBlock: {
    justifyContent: "space-between",
    flexDirection: "row",
    alignContent: "space-between",
    flexShrink: 1
  },
  renderResultsModalContainer: {
    position: "absolute",
    alignSelf: "center",
    top: "20%",
    width: moderateScale(300),
    height: moderateScale(400),
    padding: moderateScale(50),
    backgroundColor: "#363a45",
    borderWidth: 10,
    borderColor: "#b1d1cb"
  },
  renderResultsModalButton: {
    backgroundColor: "#ffac19",
    borderRadius: 4,
    paddingHorizontal: 15,
    paddingVertical: 10,
    marginTop: moderateScale(50)
  },
  renderFeedBackModalBlock: {
    margin: "auto",
    borderRadius: 4,
    justifyContent: "space-evenly",
    alignItems: "center",
    padding: 20
  }
});

function mapStateToProps(state) {
  return {
    question: state.question.question,
    id: state.question.id,
    nodeid: state.question.nodeid,
    correct: state.testResults,
    user: state.user
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      submitResults,
      pushAnswer,
      resetAnswers,
      NavToMenu: () => ({ type: "NAV_MENU" })
    },
    dispatch
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(QuestionDetails);
