import React, { Component } from 'react'
import {StyleSheet, ImageBackground, Dimensions, Image} from 'react-native'
import {View, Text} from 'native-base'
import RoomObject from '../components/roomObject'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {fetchQuestion} from '../actions/index';
import { setPosition, translateX, translateY, shouldShow } from "./room";


class natureRoom extends Component {
  constructor(props) {
		super(props);
		this.state = {
		 shroomSolved: true,
		 nendreSolved: true,
    };
}
		renderFirstGrade = () => 
		<View>
			<ImageBackground
			source={require('../images/png/backgrounds/natureRoom_1.png')}
			style={{
				width: "100%",
				height:"100%",
			}}
			resizeMode="cover"
			>	<Text>ahsjkbjhasbdjhjhdbs</Text>
				<RoomObject object={shouldShow(this.props.finishedPolls, [4], "tree", "")} action={()=> {this.props.fetchQuestion(5)}} style={this.objectStyles.tree}/>
				<RoomObject object={shouldShow(this.props.finishedPolls, [4], "weed", "")} action={()=> {this.props.fetchQuestion(5)}} style={this.objectStyles.weed}/>
				{shouldShow(this.props.finishedPolls, [4], true, false) ? <RoomObject object={shouldShow(this.props.finishedPolls, [36], "trashSolved", "trash")} action={()=> {this.props.fetchQuestion(36)}} style={this.objectStyles.bin}/> : null}
				<RoomObject object="flower" action={()=> {this.props.fetchQuestion(4)}} style={this.objectStyles.flower}/>
			</ImageBackground>
		</View>;
	
	renderSecondGrade = () =>
		<View>
			<ImageBackground
			source={require('../images/png/backgrounds/natureRoom_1.png')}
			style={{
				width: "100%",
				height:"100%",
			}}
			resizeMode="cover"
			>
			<Text>ahsjkbjhasbdjhjhdbs</Text>
				<RoomObject object={shouldShow(this.props.finishedPolls, [10], "tree", "")} action={()=> {this.props.fetchQuestion(5)}} style={this.objectStyles.tree}/>
				<RoomObject object={shouldShow(this.props.finishedPolls, [27], "bush", "")} action={()=> {this.props.fetchQuestion(5)}} style={this.objectStyles.bush}/>
				<RoomObject object="flower" action={()=> {this.props.fetchQuestion(27)}} style={this.objectStyles.flower}/>
				<RoomObject object={shouldShow(this.props.finishedPolls, [35], "weed", "")} action={()=> {this.props.fetchQuestion(40)}} style={this.objectStyles.weed}/>
				<RoomObject object={shouldShow(this.props.finishedPolls, [27], "nendreSolved", "nendre")} action={()=> {this.props.fetchQuestion(35)}} style={this.objectStyles.nendre}/>
				<RoomObject object={shouldShow(this.props.finishedPolls, [35], "shroomSolved", "shroom")} action={()=> {this.props.fetchQuestion(35)}} style={this.objectStyles.shroom}/>
			</ImageBackground>
	  </View>;
	
  render() {
		switch(parseInt(this.props.user.class)) {
			case 1:
				return (
					this.renderFirstGrade()
					
					);
			case 3:
				return (this.renderSecondGrade());
			case 2:
				return (this.renderFirstGrade());
			case 4:
				return (this.renderSecondGrade());
			default:
				// code block
		}

	// return (
	//   <View>
	// 		<ImageBackground
	// 			source={require('../images/png/backgrounds/nature.png')}
	// 			style={{
	// 				width: "100%",
	// 				height:"100%",
	// 			}}
	// 			resizeMode="cover"
	// 		>
		
	// 	<RoomObject object="tree" action={()=> {this.props.fetchQuestion(4)}} style={this.objectStyles.tree}/>
	// 	<RoomObject object="bush" action={()=> {this.props.fetchQuestion(5)}} style={this.objectStyles.bush}/>
	// 	{/* <RoomObject object="stump" action={()=> {this.props.fetchQuestion(36)}} style={this.objectStyles.stump}/> */}
	// 	<RoomObject object="flower" action={()=> {this.props.fetchQuestion(27)}} style={this.objectStyles.flower}/>
	// 	<RoomObject object="weed" action={()=> {this.props.fetchQuestion(40)}} style={this.objectStyles.weed}/>
	// 	{/* Reikia pakeist */}
	// 	<RoomObject object={this.state.nendreSolved ? "nendreRupuze": "nendre"} action={()=> {this.props.fetchQuestion(28)}} style={this.objectStyles.nendre}/>
	// 	<RoomObject object={shouldShow(this.props.finishedPolls, 5, "shroomSolved", "shroom")} action={()=> {this.props.fetchQuestion(5)}} style={this.objectStyles.shroom}/>
	// 	{/* <RoomObject object="nendre" action={()=> {this.props.fetchQuestion(28)}} style={this.objectStyles.weed}/> */}

	// 	</ImageBackground>
	//   </View>
	// )
  }
}

const styles=StyleSheet.create({
	bgWrap:{
		position:'absolute',
		top:0,
		left:0,
		width:'100%',
		height:'100%'
	}
});

function mapStateToProps(state) {
  return {
		user: state.user,
		finishedPolls: state.user.complete_successfully
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchQuestion }, dispatch);
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(natureRoom);
