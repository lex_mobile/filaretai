import React, { Component } from "react";
import {
  View,
  Image,
  Text,
  Dimensions,
  TouchableHighlight,
  StyleSheet
} from "react-native";
import { connect } from "react-redux";
import Style from "../native-base-theme/variables/commonColor";
import { H1 } from "native-base";
import { NAV_MENU } from "../navigation";
import { bindActionCreators } from "redux";
import { getClasses } from "../actions/index";
import { styleVariables } from "../globals";
const { width } = Dimensions.get("window");

const style = new StyleSheet.create({
  button: {
    borderRadius: 30,
    backgroundColor: Style.brandDark,
    padding: 10,
    width: width / 3,
    marginHorizontal: 30,
    marginBottom:16,
  },
  buttonText: {
    fontSize: styleVariables.fontSize,
    color: "#fff",
    alignSelf: "center"
  },
  logo:{
    height:80,
  }
});
class SplashScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      header: () => null
    };
  };
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: ""
    };
  }
//TIKRINT KODEL DU KARTUS NAVIGUOJA
  render() {
    if ((this.props.user.fail)) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: "space-around",
            alignItems: "center",
            backgroundColor: Style.brandPrimary,
            padding: 16
          }}
        >
          {/* <H1 style={{ alignSelf: "center" }}>
            {this.props.user.fail === "Neprisijungęs"
              ? ""
              : this.props.user.fail}
          </H1> */}
          <Image
            source={require("../assets/img/splash.png")}
            // resizeMode="contain"
            style={{maxWidth:'100%'}}
            />
          <View>
            <TouchableHighlight style={style.button}
            onPress={()=> {this.props.getClasses()}}
            >
              <Text style={style.buttonText}>Registruotis</Text>
            </TouchableHighlight>
            
            <TouchableHighlight style={style.button}
            onPress={()=> {this.props.NavToLogin()}}
            >
              <Text style={style.buttonText}>Prisijungti</Text>
            </TouchableHighlight>
          </View>
          <Logos/>
        </View>
      );
    } else {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: "space-around",
            alignItems: "center",
            backgroundColor: Style.brandPrimary,
            paddingTop: 50
          }}
        >
          <H1 style={{fontFamily: "CaveatBrush", color:'#fff'}}>
            Labas {this.props.user.name} {this.props.user.username}{" "}
          </H1>
          
          <Image
            source={require("../assets/img/splash.png")}
          />
          <TouchableHighlight style={[style.button,{marginBottom:32}]}
            onPress={this.props.NavToMenu}
          >
            <Text style={style.buttonText}>Pradėti</Text>
          </TouchableHighlight>
          <Logos />
        </View>
      );
    }
  }
}

class Logos extends Component {
  render(){
    const importLogos = [
      require('../assets/img/zaliakalnio_logo.png'),
      require('../assets/img/filaretai_logo.png'),
      require('../assets/img/sirvintai_logo.png')
    ]
    return (
      <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
        {importLogos.map((el,i)=>{
          return (
            <Image style={style.logo} source={el} key={i} resizeMode="contain" />
          )
        })}
      </View>
    )
  }
}

function mapStateToProps(state) {
  return {
    user: state.user
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      NavToMenu: () => ({ type: NAV_MENU }),
      NavToLogin: () => ({ type: "NAV_LOGIN" }),
      getClasses
    },
    dispatch
  );
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SplashScreen);
