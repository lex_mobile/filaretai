import React, { Component } from 'react';
import { View, StyleSheet, Dimensions, Image, TouchableOpacity} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { NAV_MENU, NAV_TASKS, NAV_LIBRARYROOM, NAV_NATURE1} from '../navigation';
import RoomObject, { Blackboard } from '../components/roomObject';
import {translateX, setPosition, LIBRARY, NATURE, NATURE1, translateY} from '../globals'
import {
  NAV_NATUREROOM
} from "../navigation";
import { getTasks, logout } from '../actions/index';
import GlobalHeader from '../components/globalHeader';
const d = {
  floorHeigh:250
}

import firebase from 'react-native-firebase';
import { moderateScale } from '../components/ScaleElements';
let Analytics = firebase.analytics();

class MainRoom extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      header: <GlobalHeader withBack={false}>
              <TouchableOpacity onPress= {() => {
                navigation.getParam('logout')();
                }}>
              <Image source={require('../images/png/other/logout.png')} style={{width: 50, height: 50}} resizeMode={"contain"}/>
              </TouchableOpacity>
      </GlobalHeader>,
    };
 }
  constructor(props) {
    super(props);
    this.objectStyles={
          blackboard: [
            setPosition(null, null, d.floorHeigh + 300, '50%'),
            translateX(504)
          ],
          table: [
            setPosition(null, null, d.floorHeigh - 120, '50%'), 
            translateX(477)
          ],
          chair: setPosition(null, null, d.floorHeigh - 120, '50%'),
          //top, right, bottom, left
          stickyNote: [ setPosition(null, null, moderateScale(495), moderateScale(88)),
        
          
        ]
      };
      Analytics.setUserId(this.props.user.pk.toString());
      Analytics.setUserProperty('score', this.props.user.points.toString() || "none" );
      Analytics.setCurrentScreen("MainRoom");
  }
  componentDidMount(){
    let name = `${this.props.user.name} ${this.props.user.username} ${this.props.user.classString}`;
    this.props.navigation.setParams({ 'title': name, 'logout': this.props.logout });

    this.props.getTasks();
  }
  
  placeholderAction = () =>{
    console.log('he shoots and he scores');
  }

  render() {
      let bgPattern = [];
      for(let i = 0; i<8; i++) {
        bgPattern.push(
          <View key={i} style={i % 2 !== 0 ? { flex: 1, backgroundColor: '#44beb6' } : {flex:1} }>
          </View>
        )
      }
      return (
        <View style={{flex:1}}>
          <View style={styles.backgroundWrap}>
            <View style={styles.backgroundWall}>
              {bgPattern}
            </View>
            <View style={styles.backgroundFloor}/>
          </View>

          <Blackboard count={this.props.tasks.new_count ? this.props.tasks.new_count : 0 } action={this.props.NavToTasks} style={this.objectStyles.blackboard} />
          <RoomObject object="chair" id='10' style={this.objectStyles.chair}/>
          <RoomObject object="stickyNote" style={this.objectStyles.stickyNote} action={this.props.NavToScanner}/>
          <RoomObject object="table"style={this.objectStyles.table}>
       
            <View style={styles.tableTop}>
              <RoomObject object="globe" action={this.props.NavToMaps} style={{marginRight:32}} id='10'/>
              <RoomObject object="cactus" id='10' action={this.props.NavToNature}/>
              <RoomObject object="books" style={{marginLeft:'auto'}} id='10' action={this.props.NavToLibrary}/>            
            </View>
          </RoomObject>
          {parseInt(this.props.user.class) === 3 || parseInt(this.props.user.class) === 4 ? <RoomObject object="cupFlower" action={this.props.NavToNature1} style={{marginBottom: 10, position: "absolute", bottom: 120, right: 20 }} id='10'/> : null}
        </View>
      );
    }
}

const styles = StyleSheet.create({
  backgroundWrap:{
    position:'absolute',
    top:0,
    left:0,
    width:'100%',
    height:'100%'
  },
  backgroundWall:{
    flex:1,
    backgroundColor:'#01a99e',
    flexDirection:'row'
    
  },
  backgroundFloor:{
    height:d.floorHeigh,
    backgroundColor:'#e1eded'
  },
  tableTop:{
    flexDirection: 'row', 
    alignItems: 'flex-end', 
    paddingHorizontal: 16,
    marginBottom:-2
  },
  

});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getTasks,
      logout,
      NavToNature: () => ({ type: NAV_NATUREROOM, payload: {roomName: NATURE}}),
      NavToTasks: () => ({ type: NAV_TASKS }),
      NavToLibrary: () => ({ type: NAV_LIBRARYROOM, payload: {roomName: LIBRARY}}),
      NavToNature1: () => ({ type: NAV_NATURE1, payload: {roomName: NATURE1}}),
      NavToScanner: () => ({ type: "NAV_SCANNER"}),
      NavToMaps: () => ({ type: "NAV_MAPS"})
    },
    dispatch
  );
}

function mapStateToProps(state) {
  return {
    user: state.user,
    tasks: state.tasks
  };
}
export default  connect(mapStateToProps, mapDispatchToProps)(MainRoom);
