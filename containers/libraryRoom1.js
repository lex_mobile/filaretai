import React, { Component } from "react";
import { StyleSheet, ImageBackground } from "react-native";
import { View, Text } from "native-base";
import RoomObject from "../components/roomObject";
import { setPosition, translateX, shouldShow } from "../globals";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchQuestion } from "../actions/index";
import { moderateScale } from "../components/ScaleElements";
import roomObjectData from '../roomObjectsJSON.json';

class libraryRoom extends Component {
  constructor(props) {
    super(props);
    this.objectStyles = {
      ["cabinet"]: [setPosition(0, 0, 240, "50%"), translateX(475)],
      // ["shelfRight"]: setPosition(0, "0%", 300),
      // ["shelfLeft"]: setPosition(0, 0, 300, "0%"),
      ["wheel"]: setPosition(moderateScale(83), moderateScale(80), 0, 0),
      // update
      ["shield"]: setPosition(0, 120, 240, 0),
      ["herbas"]: setPosition(50, 0, 0, 150),
      ["flag"]: setPosition(355, 280, 0, 0),
      ["hook"]: setPosition(0, 0, moderateScale(220), moderateScale(100))
    };
  }

  renderBg = () => {
    let bgPattern = [];
    for (let i = 0; i < 8; i++) {
      bgPattern.push(
        <View
          key={i}
          style={
            i % 2 !== 0 ? { flex: 1, backgroundColor: "#d4cc9e" } : { flex: 1 }
          }
        />
      );
    }
    return bgPattern;
  };
  renderFirstGrade = () => {
		return (
      <View style={{ flex: 1 }}>
        <ImageBackground
          source={require("../images/png/backgrounds/libraryBackground.png")}
          style={{
            width: "100%",
            height: "100%"
          }}
          resizeMode="cover"
        >
          {/* OBJECTS */}
          <RoomObject
            object="herbas"
            action={() => {
              this.props.fetchQuestion(12);
            }}
            style={this.objectStyles.herbas}
          />
          <View style={{ flex: 1, paddingHorizontal: 20 }}>
            <RoomObject
              object={shouldShow(this.props.finishedPolls, [16], "flag", "")}
              action={() => {
                this.props.fetchQuestion(18);
              }}
              style={this.objectStyles.flag}
            />
          </View>

          <View style={{ flex: 1 }}>
            <RoomObject
              object={shouldShow(this.props.finishedPolls, [22], "pjautuvas", "")}
              action={() => {
                this.props.fetchQuestion(24);
              }}
              style={this.objectStyles.hook}
            />
          </View>
          <View style={{ flexDirection: "row" }}>
            <View style={{ flex: 2 }} />
          </View>
          <RoomObject
            object={shouldShow(this.props.finishedPolls, [18], "wheel", "")}
            action={() => {
              this.props.fetchQuestion(22);
            }}
            style={this.objectStyles.wheel}
          />
          <RoomObject
            object={shouldShow(this.props.finishedPolls, [12], "shield", "")}
            action={() => {
              this.props.fetchQuestion(16);
            }}
            style={this.objectStyles.shield}
          />
        </ImageBackground>
      </View>
    );
	}
  renderSecondGrade = () => {
    return (
      <View style={{ flex: 1 }}>
        <ImageBackground
          source={require("../images/png/backgrounds/libraryBackground.png")}
          style={{
            width: "100%",
            height: "100%"
          }}
          resizeMode="cover"
        >
          {/* OBJECTS */}
          <RoomObject
            object="herbas"
            action={() => {
              this.props.fetchQuestion(11);
            }}
            style={this.objectStyles.herbas}
          />
          <View style={{ flex: 1, paddingHorizontal: 20 }}>
            <RoomObject
              object={shouldShow(this.props.finishedPolls, [17], "flag", "")}
              action={() => {
                this.props.fetchQuestion(9);
              }}
              style={this.objectStyles.flag}
            />
          </View>

          <View style={{ flex: 1 }}>
            <RoomObject
              object={shouldShow(this.props.finishedPolls, [23], "pjautuvas", "")}
              action={() => {
                this.props.fetchQuestion(25);
              }}
              style={this.objectStyles.hook}
            />
          </View>
          <View style={{ flexDirection: "row" }}>
            <View style={{ flex: 2 }} />
          </View>
          <RoomObject
					  //pakeist shouldshow array
            object={shouldShow(this.props.finishedPolls, [11], "wheel", "")}
            action={() => {
              this.props.fetchQuestion(23);
            }}
            style={this.objectStyles.wheel}
          />
          <RoomObject
            object={shouldShow(this.props.finishedPolls, [11], "shield", "")}
            action={() => {
              this.props.fetchQuestion(17);
            }}
            style={this.objectStyles.shield}
          />
        </ImageBackground>
      </View>
    );
  }

  // generateRoomObject = (object, id, unlockedAfter, key) => {
  //   return <RoomObject
  //     key={key}
  //     object={shouldShow(this.props.finishedPolls, unlockedAfter, object, "")}
  //     action={()=> {this.props.fetchQuestion(id)}}
  //     style={this.objectStyles[object]}
  //   />;
  // }
generateRoomObject = (object, id, unlockedAfter, key) => {
    return <RoomObject
      key={key}
      object={shouldShow(this.props.finishedPolls, unlockedAfter, object, "")}
      action={()=> {this.props.fetchQuestion(id)}}
      style={this.objectStyles[object]}
    />;
}

  render() {
    let objects = roomObjectData.libraryRoom.map((item, key)=> {
      return this.generateRoomObject(item.object, item.id, item.unlockedAfter, key);
    });
    return (
    <View style={{ flex: 1 }}>
    <ImageBackground
      source={require(`../images/png/backgrounds/libraryBackground.png`)}
      style={{
        width: "100%",
        height: "100%"
      }}
      resizeMode="cover"
    >
    {objects}
    </ImageBackground>
    </View>
    );
    
    // switch (parseInt(this.props.user.class)) {
    //   case 1:
    //     return this.renderFirstGrade();
    //   case 3:
    //     return this.renderSecondGrade();
    //   case 2:
    //     return this.renderFirstGrade();
    //   case 4:
    //     return this.renderSecondGrade();
    // }
  }
}

const styles = StyleSheet.create({
  cabinet: {
    width: 490,
    borderWidth: 10,
    borderColor: "#63380c"
  },
  shelf: {
    borderWidth: 10,
    borderColor: "#63380c",
    backgroundColor: "#422819",
    height: 150,
    justifyContent: "flex-end",
    margin: -1
  },
  background: {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%"
  },
  backgroundWall: {
    flex: 1,
    backgroundColor: "#ccc794",
    flexDirection: "row",
    borderColor: "#382b1c",
    borderBottomWidth: 46
  },
  backgroundFloor: {
    height: 250,
    backgroundColor: "#bfba85"
  }
});

function mapStateToProps(state) {
  return {
		user: state.user,
		finishedPolls: state.user.complete_successfully
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchQuestion }, dispatch);
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(libraryRoom);
