import React, { Component } from "react";
import {
  View,
  Image,
  Text,
  Dimensions,
  StyleSheet,
  PixelRatio
} from "react-native";
import { Button } from "native-base";
import { connect } from "react-redux";
import GlobalHeader from '../components/globalHeader';
const { width, height } = Dimensions.get("window");
import {
  NAV_QUESTIONS,
  NAV_SETTINGS,
  NAV_TASKS
} from "../navigation";

const style = StyleSheet.create({
  menuText: {
    textAlign: "center",
    width: "100%",
    fontSize: PixelRatio.get() * 30,
  },
  menuButton: {
    width: "50%",
    marginLeft: "25%",
    marginBottom: 16,
    backgroundColor: "#ffac19",
    borderRadius: 30,
    height: "7%"
  },
  mainContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#000",
    flexDirection: "row",
    alignContent: "center"
  },
  buttonContainer: {
    flexDirection: "column",
    flex: 1,
    justifyContent: "center",
    alignItems: "center", 
    marginBottom: '15%'
  }
});
class mainMenu extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      header: <GlobalHeader withBack={false} />,
    };
  };

  componentWillMount() {
    let name = `${this.props.user.name} ${this.props.user.username} ${
      this.props.user.classString
    }`;
    this.props.navigation.setParams({ title: name });
  }
  constructor(props) {
    super(props);
    this.state = {
      menuItems: [
        { text: "Visos užduotys", screenProp: NAV_QUESTIONS },
        { text: "Kambarys", screenProp: NAV_TASKS },
        { text: "Nustatymai", screenProp: NAV_SETTINGS }
      ]
    };
  }

  renderList() {
    const menuBuild = this.state.menuItems.map((el, i) => {
      return (
        <Button
          style={style.menuButton}
          key={i.toString()}
          onPress={() => {
            this.props.NavTo(el.screenProp);
          }}
        >
          <Text adjustsFontSizeToFit style={style.menuText}>{el.text}</Text>
        </Button>
      );
    });
    return menuBuild;
  }

  render() {
    return (
      <View style={style.mainContainer}>
        <Image
          source={require("../images/png/backgrounds/menu.png")}
          style={{ position: "absolute", height, width }}
        />
        <View style={style.buttonContainer}>{this.renderList()}</View>
      </View>
    );
  }
}
const mapDispatchToProps = dispatch => ({
  NavTo: ScreenName => {
    if (ScreenName === NAV_QUESTIONS) {
      dispatch({ type: ScreenName });
      dispatch({ type: "RR" });
    } else {
      dispatch({ type: ScreenName });
    }
  }
});
function mapStateToProps(state) {
  return {
    user: state.user
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(mainMenu);
