import React, { Component } from "react";
import {
  View,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView
} from "react-native";
import {
  Container,
  List,
  ListItem,
  Text,
  Item,
  Input,
  Spinner,
  Icon,
  Button
} from "native-base";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { register, getClasses } from "../actions/index";
import { styleVariables } from "../globals";
import { Constants } from "expo";
import GlobalHeader from '../components/globalHeader';

const { width, height } = Dimensions.get("window");
const BACK = { Name: "Grįžti",  };
let RESTART = "Pakeisti";

const style = StyleSheet.create({
  headerContainer: {
    marginTop: Constants.statusBarHeight,
    justifyContent: "center",
    alignItems: "center",
    flexShrink: 1,
    backgroundColor: "#AFAEEE",
    borderBottomColor: "#D9D5DC",
    borderBottomWidth: 2
  },
  headerText: { fontSize: styleVariables.fontSize, padding: 15 },
  mainContainer: {
    width: "100%",
    height: "100%",
    padding: 10,
    justifyContent: "center",
    alignItems: "center",
    alignContent: "center",
    flex: 1
  },
  bgImage: { position: "absolute", height, width },
  registrationForm: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 50,
    flexGrow: 0.5,
    padding: 10,
    backgroundColor: "rgba(255, 255, 255, 0.8)",  
  },
  chooseSchoolContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    padding: 15,
    height: "30%",
    flex: 1
  },
  inputBlock: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    padding: 15
  },
  loginButton: {
    backgroundColor: "rgba(0,0,0,0.8)",
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center"
  },
  registerButton: {
    fontSize: styleVariables.fontSize - 4,
    color: "#fff",
    padding: 15
  }

});
class RegistrationScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: "Registracija",
      header: <GlobalHeader withBack noPoints title={"Registracija"}/>
    };
  };
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      username: "",
      password: "",
      classNumber: "",
      classLetter: "",
      schoolNumber: "",
      data: null,
      err1: false,
      err2: false,
      err3: false,
      err4: false,
      set: false
    };
  }

  componentDidMount() {
    this.setState({ data: this.props.classes.classes });
  }

  renderList() {
    if (this.state.data) {
      return (
        <List
          dataArray={this.state.data}
          button
          style={{ flex: 1 }}
          renderRow={item => {
            
              return (
                <ListItem
                  first
                  onPress={() => {
                    if (item == BACK || item == RESTART) {
                      this.setState({
                        schoolNumber: "",
                        classLetter: "",
                        classNumber: "",
                        data: this.props.classes.classes
                      });
                    } else if (this.state.schoolNumber === "") {
                      this.setState({
                        schoolNumber: { id: item.SchoolId, Name: item.Name },
                        data: [BACK, ...item.Classes]
                      });
                    } else if (this.state.classNumber === "" && item != BACK) {
                      this.setState(
                        {
                          classNumber: {
                            id: item.ClassId,
                            Name: item.Name
                          },
                          data: [BACK, ...item.Groups]
                        }
                      );
                    } else if (this.state.classLetter === "" && item != BACK) {
                      this.setState(
                        {
                          classLetter: {
                            id: item.ClassGroupId,
                            Name: item.Name
                          }
                        },
                        (prev) => {
                          RESTART = {
                            Name: `Pakeisti ${this.state.schoolNumber.Name} ${this.state.classNumber.Name}${
                              this.state.classLetter.Name
                            }`
                          };
                          this.setState({
                            data: [RESTART]
                          });
                        }
                      );
                    }
                  }}
                >
                  <Text>
                    {this.state.classLetter ? "" : this.state.classNumber.Name}
                    {item.Name}
                  </Text>
                </ListItem>
              );
          }}
        />
      );
    } else {
      return <Spinner />;
    }
  }

  render() {
    if (this.props.attempt === true) {
      return (
        <KeyboardAvoidingView
          style=
          {style.mainContainer}
          contentContainerStyle=
          {style.mainContainer}
          keyboardVerticalOffset=
          {-100}
          behavior='position' >
          <Image
            source={require("../images/png/backgrounds/menu.png")}
            style={style.bgImage}
          />
          <Container style={{width: width*0.5, height: height*0.5}}>
            <Spinner />
          </Container>
        </KeyboardAvoidingView>
      );
    } else {
      return (
        <KeyboardAvoidingView
          style={style.mainContainer}
          contentContainerStyle={style.mainContainer}
          keyboardVerticalOffset={-100}
          behavior="position"
        >
          <Image
            source={require("../images/png/backgrounds/menu.png")}
            style={style.bgImage}
          />
          <Container style={style.registrationForm}>
            {this.props.user.fail &&
            this.props.user.fail !== "Neprisijungęs" ? (
              <Text>{this.props.user.fail}</Text>
            ) : null}
            <View
              style={{
                flexDirection: "column",
                justifyContent: "space-evenly",
                flex: 1
              }}
            >
              <ScrollView>
                <View
                  style={style.chooseSchoolContainer}
                >
                  <Text style={{ alignSelf: "flex-start", paddingTop: 20 }}>
                    {" "}
                    Pasirinkite klasę ir mokyklą{" "}
                  </Text>

                  {this.state.err3 ? (
                    <Icon name="ios-warning" style={{ color: "red" }} />
                  ) : null}
                  {this.renderList()}
                </View>
                <View
                  style={style.inputBlock}
                >
                  <Item error={this.state.err1}>
                    <Input
                      placeholder="Įveskite vardą"
                      placeholderTextColor="black"
                      value={this.state.name}
                      onChangeText={name => this.setState({ name })}
                    />
                  </Item>
                </View>
                <View
                  style={style.inputBlock}
                >
                  <Item error={this.state.err2}>
                    <Input
                      placeholder="Įveskite pavardę"
                      placeholderTextColor="black"
                      value={this.state.username}
                      onChangeText={username => this.setState({ username })}
                    />
                  </Item>
                </View>
                <View
                  style={style.inputBlock}
                >
                  <Item error={this.state.err4}>
                    <Input
                      secureTextEntry
                      placeholder="Įveskite slaptažodį"
                      placeholderTextColor="black"
                      value={this.state.password}
                      onChangeText={password => this.setState({ password })}
                    />
                  </Item>
                </View>
              </ScrollView>
              <TouchableOpacity
                style={style.loginButton}
                onPress={() => {
                  if (
                    this.state.name &&
                    this.state.username &&
                    this.state.classNumber &&
                    this.state.classLetter &&
                    this.state.password &&
                    this.state.schoolNumber
                  ) {
                    this.props.register(
                      this.state.name.trim(),
                      this.state.username.trim(),
                      this.state.classLetter.id,
                      this.state.password,
                      ` ${this.state.schoolNumber.Name} ${this.state.classNumber.Name}${
                        this.state.classLetter.Name
                      }`
                    );
                  }
                  this.setState({
                    err1: this.state.name === "",
                    err2: this.state.username === "",
                    err3:
                      this.state.classLetter === "" ||
                      this.state.classNumber === "",
                    err4: this.state.password === ""
                  });
                }}
              >
                <Text
                  style={style.registerButton}
                >
                  Registracija
                </Text>
              </TouchableOpacity>
            </View>
          </Container>
        </KeyboardAvoidingView>
      );
    }
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ register, getClasses }, dispatch);
}
function mapStateToProps(state) {
  return {
    classes: state.classes,
    user: state.user,
    attempt: state.attempt,
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegistrationScreen);
