import React, { Component } from 'react'
import {StyleSheet, ImageBackground, Dimensions, Image} from 'react-native'
import {View, Text} from 'native-base'
import BG from '../images/jsx/backgrounds/nature'
import RoomObject from '../components/roomObject'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {fetchQuestion} from '../actions/index';
import { setPosition, translateX, translateY, shouldShow} from "./room";
import { moderateScale } from '../components/ScaleElements';


class Nature1 extends Component {
  constructor(props) {
		super(props);
		this.state = {
     kelmasSolved: true,
     siuksleSolved: true
    };
    //translateX sucentruoja img
    // top, right, bottom, left
  }

  render() {
	return (
	  <View>
			<ImageBackground
				source={require('../images/png/backgrounds/natureRoom_2.png')}
				style={{
					width: "100%",
					height:"100%",
				}}
				resizeMode="cover"
			>

      <RoomObject object={shouldShow(this.props.finishedPolls, [40],"kelmas") } action={()=> {this.props.fetchQuestion(40)}} style={this.objectStyles.kelmas}/> 
      <RoomObject object={shouldShow(this.props.finishedPolls, [38],"liepa", "")} action={()=> {this.props.fetchQuestion(28)}} style={this.objectStyles.liepa}/>
      <RoomObject object={"klevas"} action={()=> {this.props.fetchQuestion(5)}} style={this.objectStyles.klevas}/>
      <RoomObject object={shouldShow(this.props.finishedPolls, [40],"sermuksnis", "")} action={()=> {this.props.fetchQuestion(38)}} style={this.objectStyles.sermuksnis}/>
      <RoomObject object={shouldShow(this.props.finishedPolls, [36],"siuksliadeze", "siuksle")} action={()=> {this.props.fetchQuestion(36)}} style={this.objectStyles.siuksle}/>
      <RoomObject object={shouldShow(this.props.finishedPolls, [28],"egle", "")} action={()=> {this.props.fetchQuestion(5)}} style={this.objectStyles.egle}/>
      <RoomObject object={shouldShow(this.props.finishedPolls, [36],"vynuoge", "")} action={()=> {this.props.fetchQuestion(27)}} style={this.objectStyles.vynuoge}/>
		</ImageBackground>
	  </View>
	)
  }
}

const styles=StyleSheet.create({
	bgWrap:{
		position:'absolute',
		top:0,
		left:0,
		width:'100%',
		height:'100%'
  },
});

function mapStateToProps(state) {
  return {
    user: state.user,
    finishedPolls: state.user.complete_successfully
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchQuestion }, dispatch);
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Nature1);
