import React, { Component } from 'react';
import {
  List,
  ListItem,
  Text,
  Spinner,
  Content,
} from 'native-base';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchCategory, getQuestions } from '../actions/index';
import { NAV_POLL } from '../navigation';

class categoryList extends Component {
  renderList() {
    if (!this.props.categories) {
      return (<Spinner />);
    }
    let search = this.props.search || '';
    return this.props.categories.map((category) => {
      if (category.Name.toLowerCase().trim().includes(search.toLowerCase().trim()) || this.props.search.trim() === '') {
        if (this.props.questions === 'SELECT_QUESTION') {
          return (
            <ListItem
              key={category.id}
              onPressIn={() => {
                let nextAction = this.props.getQuestions(category);
                this.props.NavToPoll(nextAction);
                this.props.changePage(1);
                this.props.resetSearch();
              }}
            >
              <Text>{category.Name}</Text>
            </ListItem>
          );
        }
        return (
          <ListItem
            key={category.id}
            onPress={() => { this.props.fetchCategory({ item: category, questions: this.props.questions }); this.props.resetSearch(); }}
          >
            <Text>{category.Name}</Text>
          </ListItem>
        );
      }
    });
  }

  render() {
    return (
      <Content>
        <List>
          {this.renderList()}
        </List>
      </Content>
    );
  }
}

function mapStateToProps(state) {
  return {
    categories: state.categories,
    questions: state.questions,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchCategory, getQuestions, NavToPoll: (prop) => dispatch(prop)}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(categoryList);
