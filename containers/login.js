import React, { Component } from "react";
import {
  View,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView
} from "react-native";
import { Container, Text, Item, Input, Button, Spinner, Form} from "native-base";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { login, getClasses } from "../actions/index";
import { styleVariables } from "../globals";
import { Constants } from "expo";
import GlobalHeader from '../components/globalHeader';

const { width, height } = Dimensions.get("window");

const style = StyleSheet.create({
  headerContainer: {
    marginTop: Constants.statusBarHeight,
    justifyContent: "center",
    alignItems: "center",
    flexShrink: 1,
    backgroundColor: "#AFAEEE",
    borderBottomColor: "#D9D5DC",
    borderBottomWidth: 2
  },
  headerText: { fontSize: styleVariables.fontSize, padding: 15 },
  mainContainer: {
    width: "100%",
    height: "100%",
    padding: 10,
    justifyContent: "center",
    alignItems: "center",
    alignContent: "center",
    flex: 1
  },
  bgImage: { position: "absolute", height, width },
  registrationForm: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 50,
    flexGrow: 0.5,
    padding: 10,
    backgroundColor: "rgba(255, 255, 255, 0.8)",
    borderRadius: 30
  },
  loginButton: {
    backgroundColor: "rgba(0,0,0,0.8)",
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center"
  },
  formView: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    padding: 15
  },
  mainContainerView: {
    flexDirection: "column",
    justifyContent: "space-evenly",
    flex: 1
  }

});
class LoginScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: "Prisijungimas",
      header: <GlobalHeader withBack noPoints title={"Prisijungimas"}/>
    };
  };
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      username: "",
      password: "",
      classNumber: "",
      classLetter: "",
      data: null,
      err1: false,
      err2: false,
      err3: false,
      err4: false,
      set: false
    };
  }

  render() {
    if (this.props.attempt === true) {
      <KeyboardAvoidingView
        style={style.mainContainer}
        contentContainerStyle={style.mainContainer}
        keyboardVerticalOffset={-100}
        behavior="position"
      >
        <Image
          source={require("../images/png/backgrounds/menu.png")}
          style={style.bgImage}
        />
        <Container style={{ width: width * 0.5, height: height * 0.5 }}>
          <Spinner />
        </Container>
      </KeyboardAvoidingView>;
    } else {
      return (
        <KeyboardAvoidingView
          style={style.mainContainer}
          contentContainerStyle={style.mainContainer}
          keyboardVerticalOffset={-100}
          behavior="position"
        >
          <Image
            source={require("../images/png/backgrounds/menu.png")}
            style={style.bgImage}
          />
          <Container style={style.registrationForm}>
            {this.props.user.fail ? <Text>{this.props.user.fail}</Text> : null}
            <View
              style={style.mainContainerView}
            >
              <ScrollView>
              <Form>
                <View
                  style={style.formView}
                >
                  <Item error={this.state.err1}>
                    <Input
                      placeholder="Įveskite vardą"
                      placeholderTextColor="black"
                      blurOnSubmit={false}
                      value={this.state.name}
                      onChangeText={name => this.setState({ name })}
                    />
                  </Item>
                </View>
                <View
                  style={style.formView}
                >
                  <Item error={this.state.err2}>
                    <Input
                      placeholder="Įveskite pavardę"
                      placeholderTextColor="black"
                      value={this.state.username}
                      blurOnSubmit={false}
                      onChangeText={username => this.setState({ username })}
                    />
                  </Item>
                </View>
                <View
                  style={style.formView}
                >
                  <Item error={this.state.err4}>
                    <Input
                    secureTextEntry
                      placeholder="Įveskite slaptažodį"
                      placeholderTextColor="black"
                      blurOnSubmit={false}
                      value={this.state.password}
                      onChangeText={password => this.setState({ password })}
                    />
                  </Item>
                </View>
                </Form>
              </ScrollView>
              <TouchableOpacity
                style={style.loginButton}
                onPress={() => {
                  if (
                    this.state.name &&
                    this.state.username &&
                    this.state.password
                  ) {
                    this.props.login(
                      this.state.name.trim(),
                      this.state.username.trim(),
                      this.state.password
                    );
                  }
                  this.setState({
                    err1: this.state.name === "",
                    err2: this.state.username === "",
                    err4: this.state.password === ""
                  });
                }}
              >
                <Text
                  style={{
                    fontSize: styleVariables.fontSize - 5,
                    color: "#fff",
                    padding: 15
                  }}
                >
                  Prisijungti
                </Text>
              </TouchableOpacity>
            </View>
          </Container>
        </KeyboardAvoidingView>
      );
    }
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ login, getClasses }, dispatch);
}
function mapStateToProps(state) {
  return {
    classes: state.classes,
    user: state.user,
    attempt: state.attempt
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginScreen);
