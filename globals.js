import {StyleSheet, PixelRatio} from 'react-native';

export const styleVariables = {
    questionColor: 'rgba(255, 255, 255, 0.5)',
    checkBoxBgColor: 'rgba(245,166,35,1)',
    htmlViewHrefColor: '#FF3366',
    fontSize: PixelRatio.get() * 16,
};
export const LIBRARY = "libraryRoom";
export const NATURE = "natureRoom_1";
export const NATURE1 = "natureRoom_2";

export const htmlViewStyle = StyleSheet.create({
    a: {
      flex: 1,
      fontWeight: "300",
      // color: styleVariables.htmlViewHrefColor, // make links coloured pink
      fontFamily:'CaveatBrush',
      color: "#1f3a93"

    }, 
    p: {
      fontSize: PixelRatio.get() * 16,
      fontFamily:'CaveatBrush',
      // color:'#417505',
      color: "black",
      textAlign:'center',
      padding: 6
    },
    span: {
      fontFamily:'CaveatBrush',
    },
    h1: {
      fontFamily:'CaveatBrush',
    },
    h2: {
      fontFamily:'CaveatBrush',
    },
    h3: {
      fontFamily:'CaveatBrush',
    },
    h4: {
      fontFamily:'CaveatBrush',
    },
    strong: {
      fontFamily:'CaveatBrush',
    },
    i: {
      fontFamily:'CaveatBrush',
      // fontStyle: "italic"
    },
    em: {
      fontFamily:'CaveatBrush',
      // fontStyle: "italic"
    }
  });
export const translateX = (width) => {
  let halfWidth = width / 2;
  return { transform: [{ translateX: -halfWidth }] }
}
export const translateY = (height) => {
  let halfHeight = height / 2;
  return { transform: [{ translateY: -halfHeight }] }
}
export const setPosition = (top, right, bottom, left) => {
  let position = {
    position: 'absolute',
  };
  if (top) {
    position.top = top;
  }
  if (left) {
    position.left = left;
  }
  if (bottom) {
    position.bottom = bottom;
  }
  if (right) {
    position.right = right;
  }
  return position;
}
  /**
   * Shuffles array in place.
   * @param {Array} a items An array containing the items.
  */
  export function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
  return a;
}

export function shouldShow(array, ids, success, fail) {
  if(ids.length === 0) {
    return success;
  }
  let res = ids.every((item)=> array.some((initem) => initem.PollCategoryId === item))? success : fail;
  return res;
}